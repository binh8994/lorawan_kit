
#include "Lora_v1.h"
Stream *loraSerial  ;
Stream *dbgSerial ;

LoRA :: LoRA(Stream *uart_device) {
  loraSerial = uart_device;
}

uint8_t LoRA::cmd_uart_tx_frame_post(cmd_t* data, uint8_t data_len) {
  loraSerial->read();
  uint32_t times = millis();
  uint8_t frame_len = data_len + sizeof(data->cmd_id);
  tx_buffer[0] = 0xEF;          //start
  tx_buffer[1] = frame_len;     //Data length
  tx_buffer[2] = data->cmd_id;  // ID
  memcpy(&tx_buffer[3], data->cmd_data, data_len);
  tx_buffer[2 + frame_len] = cmd_uart_calcfcs(frame_len, &tx_buffer[2]);//check Sum

  loraSerial->write(tx_buffer, (frame_len + 3));



  while (1) {
    if ((millis() - times) > 4000) break;
    else if ((loraSerial->available()) >= ( frame_len + 3 ))break;
  }

}

uint8_t LoRA::cmd_uart_calcfcs(uint8_t len, uint8_t *data_ptr) {
  uint8_t xor_result = len;
  for (int i = 0; i < len; i++, data_ptr++) {
    xor_result = xor_result ^ *data_ptr;
  }
  return xor_result;
}

void LoRA::Enddevice_int()
{ delay(100);
  send_cmd.cmd_id = 0x00; //Enddevice
  send_buff[0] = 0x00;
  send_cmd.cmd_data = send_buff;
  cmd_uart_tx_frame_post(&send_cmd, 1);

  send_cmd.cmd_id = 0x0A; //Class
  send_buff[0] = 0x01;
  send_cmd.cmd_data = send_buff;
  cmd_uart_tx_frame_post(&send_cmd, 1);

  send_cmd.cmd_id = 0x0B; //Channel Mask
  send_buff[0] = 0x01;
  send_buff[1] = 0x00;
  send_cmd.cmd_data = send_buff;
  cmd_uart_tx_frame_post(&send_cmd, 2);
  send_buff[0] = 0; send_buff[1] = 0;

  send_cmd.cmd_id = 0x0C; //Datarate
  send_buff[0] = 0;
  send_cmd.cmd_data = send_buff;
  cmd_uart_tx_frame_post(&send_cmd, 1);

  send_cmd.cmd_id = 0x0D; //Rx2 Channel
  uint32_t hz = 433175111;
  send_cmd.cmd_data = hz;
  cmd_uart_tx_frame_post(&send_cmd, 5);
  delay(100);
  send_cmd.cmd_id = 0x0E; //Rx2 Datarate
  send_buff[0] = 0;
  send_cmd.cmd_data = send_buff;
  cmd_uart_tx_frame_post(&send_cmd, 1);
  delay(100);
  send_cmd.cmd_id = 0x0F; //Tx Power -no singer
  send_buff[0] = 20;
  send_cmd.cmd_data = send_buff;
  cmd_uart_tx_frame_post(&send_cmd, 1);

  send_cmd.cmd_id = 0x10; //ANT Gain
  send_buff[0] = 3;
  send_cmd.cmd_data = send_buff;
  cmd_uart_tx_frame_post(&send_cmd, 1);
  delay(500);
  send_buff[0] = 0x07; //NetID
  send_buff[1] = 0x00;
  send_buff[2] = 0x00;
  send_buff[3] = 0x00;
  send_cmd.cmd_id = 0x06;
  send_cmd.cmd_data = send_buff;
  cmd_uart_tx_frame_post(&send_cmd, 4);
}

void LoRA::ABP_int(uint8_t devAddr[], uint8_t  nwkSKey[], uint8_t appSKey[])
{
  delay(1000);
  for (int i = 0; i < 4; i++) {
    send_buff[i] = devAddr[i];
  }
  send_cmd.cmd_id = 0x07; //Device Address  //err
  send_cmd.cmd_data = send_buff;
  cmd_uart_tx_frame_post(&send_cmd, 4);

  delay(1000);
  for (int i = 0; i < 16; i++) {
    send_buff[i] = nwkSKey[i];
  }
  send_cmd.cmd_id = 0x08; //Network Session Key
  send_cmd.cmd_data = send_buff;
  cmd_uart_tx_frame_post(&send_cmd, 16);


  for (int i = 0; i < 16; i++) {
    send_buff[i] = appSKey[i];
  }
  delay(1000);
  send_cmd.cmd_id = 0x09; //App Session Key
  send_cmd.cmd_data = send_buff;
  cmd_uart_tx_frame_post(&send_cmd, 16);
  delay(1000);
  send_cmd.cmd_id = 0x01; //ABP
  send_buff[0] = 0x01;
  send_cmd.cmd_data = send_buff;
  cmd_uart_tx_frame_post(&send_cmd, 1);
}
void LoRA::OTAA_int(uint8_t devEui[], uint8_t  appEui[], uint8_t appKey[])
{
//  Serial.print("\n OTA");
  delay(100);
  for (int i = 0; i < 8; i++) {
    send_buff[i] = devEui[i];
  }
  send_cmd.cmd_id = 0x03; //devEui
  send_cmd.cmd_data = send_buff;
  cmd_uart_tx_frame_post(&send_cmd, 4);
  delay(1000);
 // Serial.print("\n devEui");
  for (int i = 0; i < 8; i++) {
    send_buff[i] = appEui[i];
  }
  send_cmd.cmd_id = 0x04; //appEui
  send_cmd.cmd_data = send_buff;
  cmd_uart_tx_frame_post(&send_cmd, 16);
  delay(1000);
  //Serial.print("\n appEUI");
  for (int i = 0; i < 16; i++) {
    send_buff[i] = appKey[i];
  }
  send_cmd.cmd_id = 0x05; //App Key
  send_cmd.cmd_data = send_buff;
  cmd_uart_tx_frame_post(&send_cmd, 16);
  delay(1000);
  send_cmd.cmd_id = 0x01; //OTA
  send_buff[0] = 0x00;
  send_cmd.cmd_data = send_buff;
  cmd_uart_tx_frame_post(&send_cmd, 1);

  send_cmd.cmd_id = 0x02; //OTA check
  send_buff[0] = 0x00;
  send_cmd.cmd_data = send_buff;
  cmd_uart_tx_frame_post(&send_cmd, 1);
}


void LoRA::sendData(uint8_t port, uint8_t datarate , uint8_t* payload) {

  /*startt || dataleg||ID  ||******************Data ******************|| checksum */
  /*startt || dataleg||ID  ||port  || data rate ||dataleg  || payload    || checksum */
  /* EF*****************1F*********************************************************/
  uint8_t ID = 0x1f;

  send_cmd.cmd_id = 0x1F; //
  send_buff[0] = port;//Port
  send_buff[1] = datarate;//Daatarate
  send_buff[2] = PAYLOAD_SIZE;//length

  memcpy(&send_buff[4], payload, send_buff[2]);
  send_cmd.cmd_data = send_buff;
  cmd_uart_tx_frame_post(&send_cmd, PAYLOAD_SIZE + 3);


}
void LoRA::ReadData(uint8_t* data) {

  /*startt || dataleg||ID  ||******************Data ********************************* || checksum */
  /*startt || dataleg||ID  ||port  || data rate ||RXslot||RSSI|nsr||dataleg  || payload || checksum */
  /* EF****************21*********************************************************/

  get_ID(0x21, data);

}
void LoRA::read_request() {
  uint8_t* buf;
  get_ID(0x21, buf );
  return buf;
}

String LoRA::get_ID(uint8_t ID, uint8_t* data, uint8_t length)
{ 
    delay(100);
 
  send_cmd.cmd_id = ID;
  send_buff[0] = 0x00;
  send_cmd.cmd_data = send_buff;
  cmd_uart_tx_frame_post(&send_cmd, 0);

      delay(100);
  uint8_t len = loraSerial->available();
  uint8_t *buffer = (uint8_t *) malloc( len * sizeof( uint8_t ));
  loraSerial->readBytes( buffer, len);
  memcpy(&data[0], &buffer[3], length);
  free (buffer);
  return data;
}


String LoRA::get_ID(uint8_t ID, uint8_t* data) {
  ;
}
uint8_t LoRA::get_EUI(uint8_t* buf)
{
  get_ID(0x11, buf ,8);
  return buf;
}
uint8_t LoRA::get_AppEUI(uint8_t* buf)
{
  get_ID(0X12, buf,8);
  return buf;
}
uint8_t LoRA::get_AppKey(uint8_t* buf)
{
  get_ID(0X13, buf,16);
  return buf;
}
uint8_t LoRA::get_NetID(uint8_t* buf)
{
  get_ID(0X14, buf,4);
  return buf;
}

uint32_t LoRA::get_DevAdd()
{
  //get_ID(0X15, buf,4);
  // return buf;

	uint32_ tmp = 0;

	send_cmd.cmd_id = 0x15;
	send_cmd.cmd_data = send_buff;
	cmd_uart_tx_frame_post(&send_cmd, 0);

	uint32_t last = millis();
	while (loraSerial->available() < 8) {
		if ((millis() - last) >= 2000) {
			return 0;
		}
	}

	uint8_t *buffer = (uint8_t *) malloc(8);
	loraSerial->readBytes( buffer, 8);

	if (cmd_uart_calcfcs(buffer[1], &buffer[2]) == buffer[7]) {
		memcpy(&tmp, &buffer[3], 4);
	}

	free (buffer);
	return tmp;

}
uint8_t LoRA::get_NetSKey(uint8_t* buf)
{
  get_ID(0X16, buf,16);
  return buf;
}
uint8_t LoRA::get_AppSKey(uint8_t* buf)
{
  get_ID(0X17, buf,16);
  return buf;
}
uint8_t LoRA::get_Class(uint8_t* buf)
{
  get_ID(0X18, buf,1);
  return buf;
}
uint8_t LoRA::get_Mask(uint8_t* buf)
{
  get_ID(0X19, buf,2);
  return buf;
}
uint8_t LoRA::get_Datarate(uint8_t* buf)
{
  get_ID(0X1A, buf,1);
  return buf;
}
uint8_t LoRA::get_RX2Chanel(uint8_t* buf)
{
  get_ID(0X1B, buf,4);
  return buf;
}
uint8_t LoRA::get_RX2Datarate(uint8_t* buf)
{
  get_ID(0X1C, buf,1);
  return buf;
}

uint8_t LoRA::get_TXPw(uint8_t* buf)
{
  get_ID(0X1D, buf,1);
  return buf;
}
uint8_t LoRA::get_AntennaGain(uint8_t* buf)
{
  get_ID(0X1E, buf,1);
  return buf;
}

/******************FORWARD********************/
void LoRA::Forward_Txconfig(uint32_t TX_Ferquency, uint8_t Datarate , uint8_t TxPowwer)
{
  /*startt || dataleg||ID  ||******************Data ******************|| checksum */
  /*startt || dataleg||ID  ||TX Frequency-4byte  || data rate 1 byte|| TX power 1byte  || checksum */
  /* EF****************0x23*********************************************************/
  send_cmd.cmd_id = 0x23; //
  memcpy(&send_buff[0], &TX_Ferquency, 4);
  memcpy(&send_buff[5], &Datarate, 1);
  memcpy(&send_buff[6], &TxPowwer, 1);
  send_cmd.cmd_data = send_buff;
  cmd_uart_tx_frame_post(&send_cmd, 6);

}
void LoRA::get_TxForward(uint8_t buf)
{
  get_ID(0x2A, &buf);
  return &buf;
}
void LoRA::Forward_Rxconfig(uint32_t TX_Ferquency, uint8_t Datarate )
{
  send_cmd.cmd_id = 0x25; //
  memcpy(&send_buff[0], &TX_Ferquency, 4);
  memcpy(&send_buff[5], &Datarate, 1);
  send_cmd.cmd_data = send_buff;
  cmd_uart_tx_frame_post(&send_cmd, 5);

}
void  LoRA::get_RxForward(uint8_t* buf)
{
  get_ID(0x26, buf);
  return buf;
}
void  LoRA::Forward_send(uint8_t* Payload)
{
  send_cmd.cmd_id = 0x27; //
  memcpy(&send_buff[0], Payload, sizeof(Payload));

  send_cmd.cmd_data = send_buff;
  cmd_uart_tx_frame_post(&send_cmd, sizeof(Payload));
}
void  LoRA::Forward_read(uint8_t* buf)
{
  get_ID(0x28, buf);
  return buf;
}









