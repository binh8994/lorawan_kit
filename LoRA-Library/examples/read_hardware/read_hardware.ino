
#include "Lora_v1.h"

#define SOFT_TX 3
#define SOFT_RX 2
#include <SoftwareSerial.h>
#define JOIN_MODE OTAA    //  OTAA or ABP

#if JOIN_MODE == ABP
const byte devAdd[4] =
{ 0x26, 0x04, 0x1D, 0xC7 };
// USE YOUR OWN KEYS!
const byte appSKey[16] =
{ 0x08, 0x60, 0x05, 0x03, 0x40, 0xDB, 0x05, 0x00, 0xE4, 0x02, 0xC0, 0x4F, 0x0E, 0x70, 0xAD, 0x0B };
// USE YOUR OWN KEYS!
const byte nwkKey[16] =
{ 0x0D, 0xB0, 0x3B, 0x08, 0x40, 0x50, 0x06, 0x70, 0xFD, 0x00, 0xF0, 0x04, 0x0D, 0xB0, 0xA9, 0x0A };
#endif

#if JOIN_MODE == OTA
const byte devEui[8] =
{ 0x00, 0x83, 0x58, 0x7F, 0x78, 0xB4, 0x1E, 0x57 };
const byte appEui[8] =
{ 0x70, 0xB3, 0xD5, 0x7E, 0xD0, 0x00, 0xF3, 0x8D };
const byte appKey[16] =
{ 0xE6, 0x6D, 0x38, 0x84, 0x1E, 0xF1, 0x4E, 0xA5, 0x87, 0x9D, 0xA3, 0xAA, 0x83, 0xC2, 0x61, 0x59 };
#endif


SoftwareSerial mySerial(SOFT_RX, SOFT_TX);
LoRA mylora(&mySerial);

uint8_t  Address[8];
uint8_t  NetSKey[16];
uint8_t  AntennaGain[4];

void setup() {
  Serial.begin(115200);
  mySerial.begin(115200);
  Serial.println("setup");

  mylora.Enddevice_int();
  mylora.ABP_int(devAdd, nwkKey, appSKey);
  // mylora.OTAA_int(devEui, appEui, appKey);
  while (!Serial) {
    Serial.println("!Serial");
  }
}
void loop() {


  mylora.get_DevAdd(Address);
  Serial.print("\nDeviceAdress:");
  for (int i = 0; i < 4; i++) {
    Serial.print( Address[i], HEX);
  }

  mylora.get_NetSKey(NetSKey);
  Serial.print("\nNetSkey:");
  for (int i = 0; i < 16; i++) {
    Serial.print( NetSKey[i], HEX);
  }

  mylora.get_AntennaGain(AntennaGain);
  Serial.print("\nAntennaGainl:");
  for (int i = 0; i < 4; i++) {
    Serial.print( AntennaGain[i], HEX);
  }



  delay(1000);
}



