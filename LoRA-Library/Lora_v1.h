#ifndef Lora_v1.h
#include "Arduino.h"

#define PAYLOAD_SIZE        50

typedef struct {

	uint8_t cmd_id;
	uint8_t* cmd_data;
} cmd_t;

class LoRA {

public :
	LoRA(Stream *uart_device);
	/*
	  A simplified constructor taking only a Stream ({Software/Hardware}Serial) object.
	  The serial port should already be initialised when initialising this library.
	*/


	uint8_t cmd_uart_tx_frame_post(cmd_t* data, uint8_t data_len);
	uint8_t cmd_uart_rx_frame_post(cmd_t* data, uint8_t data_len);
	uint8_t cmd_uart_calcfcs(uint8_t len, uint8_t *data_ptr);

	/*************Get Endevice***************/
	String  get_ID(uint8_t ID, uint8_t* data,uint8_t length);
  String  get_ID(uint8_t ID, uint8_t* data);
  
	uint8_t  get_EUI(uint8_t* buf);
	uint8_t  get_AppEUI(uint8_t* buf);
	uint8_t  get_AppKey(uint8_t* buf);
	uint8_t  get_NetID(uint8_t* buf);
	uint8_t  get_DevAdd(uint8_t* buf);
	uint8_t  get_NetSKey(uint8_t* buf);
	uint8_t  get_AppSKey(uint8_t* buf);
	uint8_t  get_Class(uint8_t* buf);
	uint8_t  get_Mask(uint8_t* buf);
	uint8_t  get_Datarate(uint8_t* buf);
	uint8_t  get_RX2Chanel(uint8_t* buf);
	uint8_t  get_RX2Datarate(uint8_t* buf);
	uint8_t  get_TXPw(uint8_t* buf);
	uint8_t  get_AntennaGain(uint8_t* buf);

	/******************Enddevice********************/
	void Enddevice_int();
	void sendData(uint8_t port, uint8_t datarate, uint8_t* data);
	void ReadData(uint8_t* data) ;
	void read_request();

	/******************FORWARK********************/
	void Forward_Txconfig(uint32_t TX_Ferquency, uint8_t Datarate , uint8_t TxPowwer);
	void get_TxForward(uint8_t buf);
	void Forward_Rxconfig(uint32_t TX_Ferquency, uint8_t Datarate );
	void get_RxForward(uint8_t* buf);
	void Forward_send(uint8_t* Payload);
	void Forward_read(uint8_t* buf);


	void ABP_int(uint8_t devAddr[], uint8_t  nwkSKey[], uint8_t appSKey[]);
	void OTAA_int(uint8_t devEui[], uint8_t  appEui[], uint8_t appKey[]);

	void write_lora(uint8_t data, uint8_t leg);

private:

	uint8_t tx_buffer[255];
	uint8_t send_buff[255];
	//  uint8_t buffer[16] = {};
	cmd_t send_cmd;

	uint8_t buf[32];
	uint8_t ret;
};
#endif
/*
  EUI
  appEUI
  [DBG] AntennaGain: 3

  [DBG] nwk_id: x00000007
  #
  evui#
  ppUIcmd unknown


  err

*/


