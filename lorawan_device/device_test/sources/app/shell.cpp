/**
 ******************************************************************************
 * @author: ThanNT
 * @date:   13/08/2016
 ******************************************************************************
**/

#include <stdint.h>
#include <stdlib.h>

#include "ak.h"
#include "task.h"
#include "timer.h"
#include "message.h"

#include "cmd_line.h"
#include "utils.h"
#include "xprintf.h"
#include "view_render.h"

#include "sys_ctrl.h"
#include "sys_io.h"
#include "sys_dbg.h"
#include "sys_irq.h"
#include "sys_svc.h"
#include "sys_arduino.h"

#include "app.h"
#include "app_if.h"
#include "app_dbg.h"
#include "app_data.h"
#include "app_flash.h"
#include "app_eeprom.h"
#include "app_non_clear_ram.h"

#include "task_shell.h"
#include "task_list.h"
#include "task_list_if.h"
#include "task_if.h"
#include "task_life.h"

#include "rtc.h"
#include "led.h"
#include "eeprom.h"
#include "Adafruit_ssd1306syp.h"
#include "EmonLib.h"
#include "DS1302.h"
#include "flash.h"
#include "hs1101.h"
#include "exor.h"

#include "WString.h"
#include "Wire.h"
#include "task_sensor.h"

/*****************************************************************************/
/*  local declare
 */
/*****************************************************************************/
#define STR_LIST_MAX_SIZE		50
#define STR_BUFFER_SIZE			500

#if 0
static char cmd_buffer[STR_BUFFER_SIZE];
static char* str_list[STR_LIST_MAX_SIZE];
static uint8_t str_list_len;

static uint8_t str_parser(char* str);
static char* str_parser_get_attr(uint8_t);
#endif

/*****************************************************************************/
/*  command function declare
 */
/*****************************************************************************/
static int32_t shell_reset(uint8_t* argv);
static int32_t shell_ver(uint8_t* argv);
static int32_t shell_help(uint8_t* argv);
static int32_t shell_reboot(uint8_t* argv);
static int32_t shell_dbg(uint8_t* argv);
static int32_t shell_lora(uint8_t* argv);
static int32_t shell_sensor(uint8_t* argv);

/*****************************************************************************/
/*  command table
 */
/*****************************************************************************/
cmd_line_t lgn_cmd_table[] = {

	/*************************************************************************/
	/* system command */
	/*************************************************************************/
	{(const int8_t*)"reset",	shell_reset,		(const int8_t*)"reset terminal"},
	{(const int8_t*)"ver",		shell_ver,			(const int8_t*)"version info"},
	{(const int8_t*)"help",		shell_help,			(const int8_t*)"help info"},
	{(const int8_t*)"reboot",	shell_reboot,		(const int8_t*)"reboot"},
	{(const int8_t*)"lora",		shell_lora,			(const int8_t*)"lora setting"},
	{(const int8_t*)"sensor",	shell_sensor,		(const int8_t*)"sensor setting"},

	/*************************************************************************/
	/* debug command */
	/*************************************************************************/
	{(const int8_t*)"dbg",		shell_dbg,			(const int8_t*)"dbg"},

	/* End Of Table */
	{(const int8_t*)0,(pf_cmd_func)0,(const int8_t*)0}
};

#if 0
uint8_t str_parser(char* str) {
	strcpy(cmd_buffer, str);
	str_list_len = 0;

	uint8_t i = 0;
	uint8_t str_list_index = 0;
	uint8_t flag_insert_str = 1;

	while (cmd_buffer[i] != 0 && cmd_buffer[i] != '\n' && cmd_buffer[i] != '\r') {
		if (cmd_buffer[i] == ' ') {
			cmd_buffer[i] = 0;
			flag_insert_str = 1;
		}
		else if (flag_insert_str) {
			str_list[str_list_index++] = &cmd_buffer[i];
			flag_insert_str = 0;
		}
		i++;
	}

	cmd_buffer[i] = 0;

	str_list_len = str_list_index;
	return str_list_len;
}

char* str_parser_get_attr(uint8_t index) {
	if (index < str_list_len) {
		return str_list[index];
	}
	return NULL;
}
#endif

/*****************************************************************************/
/*  command function definaion
 */
/*****************************************************************************/
int32_t shell_reset(uint8_t* argv) {
	(void)argv;
	xprintf("\033[2J\r");
	return 0;
}

int32_t shell_ver(uint8_t* argv) {
	(void)argv;

	firmware_header_t firmware_header;
	sys_ctrl_get_firmware_info(&firmware_header);

	LOGIN_PRINT("Kernel version: %s\n", AK_VERSION);
	LOGIN_PRINT("App version: %s\n", app_version);
	LOGIN_PRINT("Firmware checksum: %04x\n", firmware_header.checksum);
	LOGIN_PRINT("Firmware length: %d\n", firmware_header.bin_len);

	LOGIN_PRINT("\nSystem information:\n");
	LOGIN_PRINT("\tFLASH used:\t%d bytes\n", system_info.flash_used);
	LOGIN_PRINT("\tSRAM used:\t%d bytes\n", system_info.ram_used);
	LOGIN_PRINT("\t\tdata init size:\t\t%d bytes\n", system_info.data_init_size);
	LOGIN_PRINT("\t\tdata non_init size:\t%d bytes\n", system_info.data_non_init_size);
	LOGIN_PRINT("\t\tstack avail:\t\t%d bytes\n", system_info.stack_avail);
	LOGIN_PRINT("\t\theap avail:\t\t%d bytes\n", system_info.heap_avail);
	LOGIN_PRINT("\t\tother:\t\t\t%d bytes\n", system_info.ram_other);
	LOGIN_PRINT("\n");
	LOGIN_PRINT("\tcpu clock:\t%d Hz\n", system_info.cpu_clock);
	LOGIN_PRINT("\ttime tick:\t%d ms\n", system_info.tick);
	LOGIN_PRINT("\tconsole:\t%d bps\n", system_info.console_baudrate);
	LOGIN_PRINT("\n\n");
	return 0;
}

int32_t shell_help(uint8_t* argv) {
	uint32_t idx = 0;
	switch (*(argv + 4)) {
	default:
		LOGIN_PRINT("\nCOMMANDS INFORMATION:\n\n");
		while(lgn_cmd_table[idx].cmd != (const int8_t*)0) {
			LOGIN_PRINT("%s\n\t%s\n\n", lgn_cmd_table[idx].cmd, lgn_cmd_table[idx].info);
			idx++;
		}
		break;
	}
	return 0;
}

int32_t shell_reboot(uint8_t* argv) {
	(void)argv;
	sys_ctrl_reset();
	return 0;
}

int32_t shell_dbg(uint8_t* argv) {
	(void)(argv);
	device_setting_t setting;

	switch (*(argv + 4)) {
	case '0': {
		eeprom_read(DEVICE_SETTING_ADDR, (uint8_t*)&setting, sizeof(device_setting_t));
		setting.enable_sleep = 0;
		eeprom_write(DEVICE_SETTING_ADDR, (uint8_t*)&setting, sizeof(device_setting_t));
		LOGIN_PRINT("disable sleep mode\n");
	}
		break;

	case '1': {
		eeprom_read(DEVICE_SETTING_ADDR, (uint8_t*)&setting, sizeof(device_setting_t));
		setting.enable_sleep = 1;
		eeprom_write(DEVICE_SETTING_ADDR, (uint8_t*)&setting, sizeof(device_setting_t));
		LOGIN_PRINT("enable sleep mode\n");
		goto_sleep_mode(setting.periodic_timer * 60);
	}
		break;

	default:
		LOGIN_PRINT("dbg <..>\n");
		LOGIN_PRINT(" 0\t dis sleep\n");
		LOGIN_PRINT(" 1\tena sleep\n");
		break;
	}

	return 0;
}

int32_t shell_lora(uint8_t* argv) {

	switch (*(argv + 5)) {
	case 'r': {
		LOGIN_PRINT("erasing setups\n");
		eeprom_erase(DEVICE_SETTING_ADDR, sizeof(device_setting_t));
		eeprom_erase(LORAWAN_SETTING_ADDR, sizeof(lorawan_setting_t));
		LOGIN_PRINT("done\n");
	}
		break;

	case 'o': {
		lorawan_setting_t lsetting;
		uint8_t app_Eui[8] = { 0x70, 0xB3, 0xD5, 0x7E, 0xD0, 0x00, 0xBF, 0x50 };
		uint8_t app_key[16] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16};

		memset(&lsetting, 0, sizeof(lorawan_setting_t));
		lsetting.nwk_joined = LORAWAN_NWK_FREE;
		get_device_Eui(lsetting.device_Eui);
		memcpy(lsetting.app_Eui, app_Eui, 8);
		memcpy(lsetting.app_key, app_key, 16);

		LOGIN_PRINT("writing setups\n");
		eeprom_write(LORAWAN_SETTING_ADDR, (uint8_t*)&lsetting, sizeof(lorawan_setting_t));
		LOGIN_PRINT("done\n");

	}
		break;

	case 'a': {
		lorawan_setting_t lsetting;
		uint32_t net_id = 0;
		uint32_t dev_addr = 0x2604187E;
		//uint8_t app_skey[16] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16};
		//uint8_t nwk_skey[16] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16};
		uint8_t app_skey[16] = { 0x36, 0x41, 0x94, 0x4C, 0x93, 0x1A, 0xAA, 0x83, 0x68, 0x7F, 0x0E, 0x26, 0xD1, 0xA6, 0x35, 0x3C };
		uint8_t nwk_skey[16] = { 0x43, 0x64, 0x36, 0xDE, 0x7E, 0x07, 0xCF, 0x54, 0x0F, 0x9D, 0x38, 0x5D, 0x71, 0x03, 0xD2, 0x82 };

		eeprom_read(LORAWAN_SETTING_ADDR, (uint8_t*)&lsetting, sizeof(lorawan_setting_t));

		lsetting.nwk_joined = LORAWAN_NWK_JOINED;
		lsetting.nwk_id = net_id;
		lsetting.dev_addr = dev_addr;
		memcpy(lsetting.app_skey, app_skey, 16);
		memcpy(lsetting.nwk_skey, nwk_skey, 16);

		LOGIN_PRINT("writing setups\n");
		eeprom_write(LORAWAN_SETTING_ADDR, (uint8_t*)&lsetting, sizeof(lorawan_setting_t));
		LOGIN_PRINT("done\n");
	}
		break;

	case 't': {
		int periodic = atoi((const char *)(argv + 7));
		device_setting_t dsetting;

		eeprom_read(DEVICE_SETTING_ADDR, (uint8_t*)&dsetting, sizeof(device_setting_t));
		dsetting.periodic_timer = (uint32_t)abs(periodic);

		LOGIN_PRINT("set timer: %d (min)\n", dsetting.periodic_timer);
		eeprom_write(DEVICE_SETTING_ADDR, (uint8_t*)&dsetting, sizeof(device_setting_t));
		LOGIN_PRINT("done\n");
	}
		break;

	case 's': {
		task_post_pure_msg(AC_TASK_SENSOR_ID, SENSOR_DBG);
	}
		break;

	default:
		LOGIN_PRINT("lora <..>\n");
		LOGIN_PRINT(" r\terase\n");
		LOGIN_PRINT(" o\tinit OTA\n");
		LOGIN_PRINT(" a\tinit APB\n");
		LOGIN_PRINT(" t <time(min)>\tset periodic timer\n");
		LOGIN_PRINT(" s send last msg\n");
		break;
	}

	return 0;
}


int32_t shell_sensor(uint8_t* argv) {

	switch (*(argv + 7)) {
	case 'o': {
		ph_sensor_pwr(true); do_sensor_pwr(true);
		orp_sensor_pwr(true); ec_sensor_pwr(true);
		tur_sensor_pwr(true);
		LOGIN_PRINT("sensor pwr on\n");
	}
		break;

	case 'f': {
		ph_sensor_pwr(false); do_sensor_pwr(false);
		orp_sensor_pwr(false); ec_sensor_pwr(false);
		tur_sensor_pwr(false);
		LOGIN_PRINT("sensor pwr off\n");
	}
		break;

	case 'i': {
		LOGIN_PRINT("lock to I2C\n");

		Wire.begin();
		ph_sensor_pwr(true); do_sensor_pwr(true);
		orp_sensor_pwr(true); ec_sensor_pwr(true);
		tur_sensor_pwr(true);
		delay(2000);

		/* PH sensor */
		Wire.beginTransmission(PH_ADDR);
		Wire.write((uint8_t*)"plock,1", strlen("plock,1"));
		uint8_t ret = Wire.endTransmission();
		APP_PRINT("PH res: %d\n", ret);

		/* DO sensor */
		Wire.beginTransmission(DO_ADDR);
		Wire.write((uint8_t*)"plock,1", strlen("plock,1"));
		ret = Wire.endTransmission();
		APP_PRINT("DO res: %d\n", ret);

		/* EC sensor */
		Wire.beginTransmission(EC_ADDR);
		Wire.write((uint8_t*)"plock,1", strlen("plock,1"));
		ret = Wire.endTransmission();
		APP_PRINT("EC res: %d\n", ret);

		/* ORP sensor */
		Wire.beginTransmission(ORP_ADDR);
		Wire.write((uint8_t*)"plock,1", strlen("plock,1"));
		ret = Wire.endTransmission();
		APP_PRINT("ORP res: %d\n", ret);

		//ph_sensor_pwr(false); do_sensor_pwr(false);
		//orp_sensor_pwr(false); ec_sensor_pwr(false);
		//tur_sensor_pwr(false);
	}
		break;

	case '0': {
		LOGIN_PRINT("led off\n");

		Wire.begin();
		ph_sensor_pwr(true); do_sensor_pwr(true);
		orp_sensor_pwr(true); ec_sensor_pwr(true);
		tur_sensor_pwr(true);
		delay(2000);

		/* PH sensor */
		Wire.beginTransmission(PH_ADDR);
		Wire.write((uint8_t*)"l,0", strlen("l,0"));
		uint8_t ret = Wire.endTransmission();
		APP_PRINT("PH res: %d\n", ret);

		/* DO sensor */
		Wire.beginTransmission(DO_ADDR);
		Wire.write((uint8_t*)"l,0", strlen("l,0"));
		ret = Wire.endTransmission();
		APP_PRINT("DO res: %d\n", ret);

		/* EC sensor */
		Wire.beginTransmission(EC_ADDR);
		Wire.write((uint8_t*)"l,0", strlen("l,0"));
		ret = Wire.endTransmission();
		APP_PRINT("EC res: %d\n", ret);

		/* ORP sensor */
		Wire.beginTransmission(ORP_ADDR);
		Wire.write((uint8_t*)"l,0", strlen("l,0"));
		ret = Wire.endTransmission();
		APP_PRINT("ORP res: %d\n", ret);

		//ph_sensor_pwr(false); do_sensor_pwr(false);
		//orp_sensor_pwr(false); ec_sensor_pwr(false);
		//tur_sensor_pwr(false);
	}
		break;

	case '1': {
		LOGIN_PRINT("led on\n");

		Wire.begin();
		ph_sensor_pwr(true); do_sensor_pwr(true);
		orp_sensor_pwr(true); ec_sensor_pwr(true);
		tur_sensor_pwr(true);
		delay(2000);

		/* PH sensor */
		Wire.beginTransmission(PH_ADDR);
		Wire.write((uint8_t*)"l,1", strlen("l,1"));
		uint8_t ret = Wire.endTransmission();
		APP_PRINT("PH res: %d\n", ret);

		/* DO sensor */
		Wire.beginTransmission(DO_ADDR);
		Wire.write((uint8_t*)"l,1", strlen("l,1"));
		ret = Wire.endTransmission();
		APP_PRINT("DO res: %d\n", ret);

		/* EC sensor */
		Wire.beginTransmission(EC_ADDR);
		Wire.write((uint8_t*)"l,1", strlen("l,1"));
		ret = Wire.endTransmission();
		APP_PRINT("EC res: %d\n", ret);

		/* ORP sensor */
		Wire.beginTransmission(ORP_ADDR);
		Wire.write((uint8_t*)"l,1", strlen("l,1"));
		ret = Wire.endTransmission();
		APP_PRINT("ORP res: %d\n", ret);

		//ph_sensor_pwr(false); do_sensor_pwr(false);
		//orp_sensor_pwr(false); ec_sensor_pwr(false);
		//tur_sensor_pwr(false);
	}
		break;

	case 'E': {

		Wire.begin();
		ec_sensor_pwr(true);
		delay(2000);

		String ret = Atlas_sensor_cmd(EC_ADDR, (uint8_t*)(argv + 9), strlen((char*)(argv + 9)));
		APP_PRINT("EC res: %s\n", ret.c_str());

		//ec_sensor_pwr(false);
	}
		break;

	case 'O': {

		Wire.begin();
		orp_sensor_pwr(true);
		delay(2000);

		String ret = Atlas_sensor_cmd(ORP_ADDR, (uint8_t*)(argv + 9), strlen((char*)(argv + 9)));
		APP_PRINT("ORP res: %s\n", ret.c_str());

		//orp_sensor_pwr(false);
	}
		break;

	case 'D': {

		Wire.begin();
		do_sensor_pwr(true);
		delay(2000);

		String ret = Atlas_sensor_cmd(DO_ADDR, (uint8_t*)(argv + 9), strlen((char*)(argv + 9)));
		APP_PRINT("DO res: %s\n", ret.c_str());

		//do_sensor_pwr(false);
	}
		break;

	case 'P': {

		Wire.begin();
		ph_sensor_pwr(true);
		delay(2000);

		String ret = Atlas_sensor_cmd(PH_ADDR, (uint8_t*)(argv + 9), strlen((char*)(argv + 9)));
		APP_PRINT("PH res: %s\n", ret.c_str());

		//ec_sensor_pwr(false);
	}
		break;

	default:
		LOGIN_PRINT("sensor <..>\n");
		LOGIN_PRINT(" o\tpwr on\n");
		LOGIN_PRINT(" f\tpwr off\n");
		LOGIN_PRINT(" i\tlock i2c\n");
		LOGIN_PRINT(" 0\tled off\n");
		LOGIN_PRINT(" 1\tled on\n");
		LOGIN_PRINT(" E\tsend to EC\n");
		LOGIN_PRINT(" O\tsend to ORP\n");
		LOGIN_PRINT(" D\tsend to DO\n");
		LOGIN_PRINT(" P\tsend to PH\n");


		break;
	}

	return 0;
}

