#include <string.h>

#include "fsm.h"
#include "port.h"
#include "timer.h"
#include "message.h"

#include "sys_dbg.h"
#include "sys_irq.h"
#include "sys_io.h"

#include "Arduino.h"

#include "app.h"
#include "app_dbg.h"
#include "lorawan.h"
#include "led.h"
#include "eeprom.h"

#include "task_list.h"
#include "task_setting.h"

static lorawan_msg_t recv_msg;
static shimizu_setting_t* setting_recv;
static device_setting_t setting;

void task_setting(ak_msg_t* msg) {
	switch (msg->sig) {
	case SETTING_RECV_MSG: {
		APP_DBG_SIG("SETTING_RECV_MSG\n");

		memcpy(&recv_msg, get_data_common_msg(msg), get_data_len_common_msg(msg));
		APP_DBG_HEX(&recv_msg, get_data_len_common_msg(msg));

		if (recv_msg.header.device_type != LORAWAN_DEVICE_TYPE_SHIMIZU)
			break;
		if (recv_msg.header.function_type != LORAWAN_FUNCTION_TYPE_PERIODIC_TIMER_SETTING)
			break;
		if (recv_msg.header.data_type != LORAWAN_DATA_TYPE_UINT32)
			break;

		setting_recv = (shimizu_setting_t*)recv_msg.data;
		setting_recv->periodic_timer = (uint32_t)max(setting_recv->periodic_timer, 1000);
		setting.periodic_timer = setting_recv->periodic_timer;

		//APP_PRINT("setting timer: %d (ms)\n", setting.periodic_timer);
		//eeprom_write(DEVICE_SETTING_ADDR, (uint8_t*)&setting, sizeof(device_setting_t));

		/* prepair for response */
		//timer_set(AC_TASK_SENSOR_ID, SENSOR_PWR_ON, 100, TIMER_ONE_SHOT);

	};
		break;
	default:
		break;
	}
}
