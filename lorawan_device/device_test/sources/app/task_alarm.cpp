#include <string.h>

#include "../ak/fsm.h"
#include "../ak/port.h"
#include "../ak/timer.h"
#include "../ak/message.h"

#include "../sys/sys_dbg.h"
#include "../sys/sys_irq.h"
#include "../sys/sys_io.h"

#include "app.h"
#include "app_dbg.h"
#include "lorawan.h"
#include "led.h"

#include "task_list.h"
#include "task_alarm.h"

led_t led_status;

static lorawan_msg_t recv_msg;

void task_alarm(ak_msg_t* msg) {
	switch (msg->sig) {
	case ALARM_RECV_MSG: {
		APP_DBG_SIG("ALARM_RECV_MSG\n");

		memcpy(&recv_msg, get_data_common_msg(msg), get_data_len_common_msg(msg));
		APP_DBG_HEX(&recv_msg, get_data_len_common_msg(msg));

		if (recv_msg.header.device_type != LORAWAN_DEVICE_TYPE_SHIMIZU)
			break;
		if (recv_msg.header.function_type != LORAWAN_FUNCTION_TYPE_CONTROL_A_CONTROL)
			break;
		if (recv_msg.header.data_type != LORAWAN_DATA_TYPE_UINT1)
			break;

		shimizu_control_a_t* ctrl_data = (shimizu_control_a_t*)recv_msg.data;
		if (ctrl_data->output == 0x01) {
			if (ctrl_data->timeout) {
				//APP_PRINT("setting ALARM on\n");
				//
				//led_on(&led_status);
				//speaker_triger(true);
				//
				//timer_set(AC_TASK_SENSOR_ID, SENSOR_PWR_ON, 200, TIMER_ONE_SHOT);
				//
				//timer_set(AC_TASK_ALARM_ID, ALARM_OFF, ctrl_data->timeout * 1000, TIMER_ONE_SHOT);
			}
		}
	}
		break;

	case ALARM_ON: {
		APP_DBG_SIG("ALARM ON\n");
	}
		break;

	case ALARM_OFF: {
		APP_DBG_SIG("ALARM OFF\n");

		led_off(&led_status);
		speaker_triger(false);
	}
		break;

	default:
		break;
	}
}
