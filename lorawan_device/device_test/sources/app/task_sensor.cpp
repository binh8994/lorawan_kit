#include "fsm.h"
#include "port.h"
#include "timer.h"
#include "message.h"

#include "sys_dbg.h"
#include "sys_irq.h"
#include "sys_io.h"

#include "thermistor.h"
#include "eeprom.h"

#include "app.h"
#include "app_non_clear_ram.h"
#include "app_eeprom.h"
#include "app_dbg.h"
#include "task_list.h"
#include "task_sensor.h"

#include "Wire.h"
#include "WString.h"
#include "lorawan.h"
#include "lora_ping_pong.h"
#include "task_life.h"

#include "sx1276.h"

/* ATLAS SENSOR DEFINE */
#define PH_DELAY				(900 + 100)
#define DO_DELAY				(600 + 100)
#define ORP_DELAY				(900 + 100)
#define EC_DELAY				(600 + 100)

#define RESPONSE_SUCCESS		(1)
#define RESPONSE_FAILED			(2)
#define RESPONSE_PENDING		(254)
#define RESPONSE_NO_DATA		(255)

THERMISTOR ther(THER_ADC_CHAN, 10000, 3590, 10000);

static device_setting_t device_setting;
static lorawan_msg_t report_message;

static String read_string;
static int32_t data_x100;
static char *pEC, *pTDS, *pSAL;
static char sensorstring_array[20];
static shimizu_sensor_a_t* shimizu_sensor;

static String read_Atlas_PH(void);
static String read_Atlas_DO(void);
static String read_Atlas_ORP(void);
static String read_Atlas_EC_TDS_SAL(void);

void task_sensor(ak_msg_t* msg) {
	switch (msg->sig) {
	case SENSOR_INIT: {
		APP_DBG_SIG("SENSOR_INIT\n");

		ther.begin();
		/*prepair msg for send*/
		report_message.header.device_type = LORAWAN_DEVICE_TYPE_SHIMIZU;
		report_message.header.function_type = LORAWAN_FUNCTION_TYPE_SENSOR_A_REPORT;
		report_message.header.data_type = LORAWAN_DATA_TYPE_ARRAY;
		report_message.header.data_length = sizeof(shimizu_sensor_a_t);
		shimizu_sensor = (shimizu_sensor_a_t*)report_message.data;

		timer_set(AC_TASK_SENSOR_ID, SENSOR_PWR_ON, 100, TIMER_ONE_SHOT);
	}
		break;

	case SENSOR_PWR_ON: {
		APP_DBG_SIG("SENSOR_PWR_ON\n");

		Wire.begin();
		ph_sensor_pwr(true);
		do_sensor_pwr(true);
		orp_sensor_pwr(true);
		ec_sensor_pwr(true);

		timer_set(AC_TASK_SENSOR_ID, SENSOR_READ, 2000, TIMER_ONE_SHOT);
	};
		break;

	case SENSOR_READ: {
		APP_DBG_SIG("SENSOR_READ\n");

		/* check batery level */
		shimizu_sensor->bat = get_batery_level();
		APP_PRINT("BAT: %d\n", shimizu_sensor->bat);
		if (shimizu_sensor->bat == 0) {
			eeprom_read(DEVICE_SETTING_ADDR, (uint8_t*)&device_setting, sizeof(device_setting_t));
			APP_PRINT("Periodic timer: %d (min)\n", device_setting.periodic_timer);
			if (device_setting.enable_sleep == 1) {

				/*release i2c pin*/
				i2c_pin_release();
				ph_sensor_pwr(false);
				do_sensor_pwr(false);
				orp_sensor_pwr(false);
				ec_sensor_pwr(false);

				SX1276SetSleep();

				goto_sleep_mode(device_setting.periodic_timer * 60);
				break;
			}
			else {
				break;
			}
		}

		read_string = read_Atlas_PH();
		APP_PRINT("PH: %s\n", read_string.c_str());
		data_x100 = (int32_t)(read_string.toFloat() * 100);
		shimizu_sensor->ph_inter = (int16_t)(data_x100 / 100) & 0xFFFF;
		shimizu_sensor->ph_decac = (int16_t)data_x100 % 100 & 0xFFFF;
		APP_DBG("PH: %d.%d\n", shimizu_sensor->ph_inter, shimizu_sensor->ph_decac);

		read_string = read_Atlas_DO();
		APP_PRINT("DO: %s\n", read_string.c_str());
		data_x100 = (int32_t)(read_string.toFloat() * 100);
		shimizu_sensor->do_inter = (int16_t)(data_x100 / 100) & 0xFFFF;
		shimizu_sensor->do_decac = (int16_t)data_x100 % 100 & 0xFFFF;
		APP_DBG("DO: %d.%d\n", shimizu_sensor->do_inter, shimizu_sensor->do_decac);

		read_string = read_Atlas_ORP();
		APP_PRINT("ORP: %s\n", read_string.c_str());
		data_x100 = (int32_t)(read_string.toFloat() * 100);
		shimizu_sensor->orp_inter = (int16_t)(data_x100 / 100) & 0xFFFF;
		shimizu_sensor->orp_decac = (int16_t)data_x100 % 100 & 0xFFFF;
		APP_DBG("ORP: %d.%d\n", shimizu_sensor->orp_inter, shimizu_sensor->orp_decac);

		read_string = read_Atlas_EC_TDS_SAL();
		APP_PRINT("EC_TDS_SAL_SG: %s\n", read_string.c_str());
		read_string.toCharArray(sensorstring_array, 30);
		pEC = strtok(sensorstring_array, ",");
		pTDS = strtok(NULL, ",");
		pSAL = strtok(NULL, ",");

		data_x100 = (int32_t)(atof(pEC) * 100);
		shimizu_sensor->ec_inter = (int32_t)(data_x100 / 100);
		shimizu_sensor->ec_decac = (int32_t)data_x100 % 100;
		APP_DBG("EC: %d.%d\n", shimizu_sensor->ec_inter, shimizu_sensor->ec_decac);

		data_x100 = (int32_t)(atof(pTDS) * 100);
		shimizu_sensor->tds_inter = (int32_t)(data_x100 / 100);
		shimizu_sensor->tds_decac = (int32_t)data_x100 % 100;
		APP_DBG("TDS: %d.%d\n", shimizu_sensor->tds_inter, shimizu_sensor->tds_decac);

		data_x100 = (int32_t)(atof(pSAL) * 100);
		shimizu_sensor->sal_inter = (int32_t)(data_x100 / 100);
		shimizu_sensor->sal_decac = (int32_t)data_x100 % 100;
		APP_DBG("SAL: %d.%d\n", shimizu_sensor->sal_inter, shimizu_sensor->sal_decac);

		//shimizu_sensor->tur = adc_tur_io_read(TUR_ADC_CHAN);
		APP_PRINT("TURB: %d\n", shimizu_sensor->tur);

		data_x100 = ther.read()*10;
		shimizu_sensor->temp_inter = (int16_t)(data_x100 / 100) & 0xFFFF;
		shimizu_sensor->temp_decac = (int16_t)data_x100 % 100 & 0xFFFF;
		APP_PRINT("TEMP: %d\n", shimizu_sensor->temp_inter, shimizu_sensor->temp_decac);

		/*release i2c pin*/
		i2c_pin_release();
		ph_sensor_pwr(false);
		do_sensor_pwr(false);
		orp_sensor_pwr(false);
		ec_sensor_pwr(false);

		/*send to gateway*/
		//lorawan_send(&report_message, sizeof(lorawan_header_msg_t) + report_message.header.data_length);
		task_post_pure_msg(AC_TASK_SENSOR_ID, SENSOR_DBG);
	}
		break;

	case SENSOR_DBG: {
		APP_DBG_SIG("SENSOR_DBG\n");
		lorawan_send(&report_message, sizeof(lorawan_header_msg_t) + report_message.header.data_length);
	}
		break;

	default:
		break;
	}
}

String read_Atlas_PH(void) {
	uint8_t response_code = RESPONSE_NO_DATA;
	uint8_t num_read = 0;
	uint8_t read_byte;
	String result;

	//ENTRY_CRITICAL();
	//num_read = Wire.requestFrom(PH_ADDR, 10, 1);
	num_read = Wire.requestWaitFrom(PH_ADDR, 10, 'r', 1, 1, PH_DELAY);
	//EXIT_CRITICAL();
	//APP_DBG("num_read: %d\n", num_read);

	if (num_read) {
		response_code = Wire.read();
		APP_PRINT("response_code: %d\n", response_code);

		if (response_code == RESPONSE_SUCCESS){
			for (uint8_t i = 0; i < (num_read - 1); i++) {
				read_byte = Wire.read();
				//APP_DBG("%c", read_byte);

				if (read_byte == 0) {
					Wire.endTransmission();

					if (result.length()) {
						return result;
					}
					else {
						break;
					}
				}

				result += (char)(read_byte);
			}
		}
	}

	return String("");
}

String read_Atlas_ORP(void) {
	uint8_t response_code = RESPONSE_NO_DATA;
	uint8_t num_read = 0;
	uint8_t read_byte;
	String result;

	//ENTRY_CRITICAL();
	//num_read = Wire.requestFrom(ORP_ADDR, 20, 1);
	num_read = Wire.requestWaitFrom(ORP_ADDR, 10, 'r', 1, 1, ORP_DELAY);
	//EXIT_CRITICAL();
	//APP_DBG("num_read: %d\n", num_read);

	if (num_read) {
		response_code = Wire.read();
		APP_PRINT("response_code: %d\n", response_code);

		if (response_code == RESPONSE_SUCCESS){
			for (uint8_t i = 0; i < (num_read - 1); i++) {
				read_byte = Wire.read();
				//APP_DBG("%c", read_byte);

				if (read_byte == 0) {
					Wire.endTransmission();

					if (result.length()) {
						return result;
					}
					else {
						break;
					}
				}

				result += (char)(read_byte);
			}
		}
	}

	return String("");
}

String read_Atlas_DO(void) {
	uint8_t response_code = RESPONSE_NO_DATA;
	uint8_t num_read = 0;
	uint8_t read_byte;
	String result;

	//ENTRY_CRITICAL();
	//num_read = Wire.requestFrom(DO_ADDR, 10, 1);
	num_read = Wire.requestWaitFrom(DO_ADDR, 10, 'r', 1, 1, DO_DELAY);
	//EXIT_CRITICAL();
	//APP_DBG("num_read: %d\n", num_read);

	if (num_read) {
		response_code = Wire.read();
		APP_PRINT("response_code: %d\n", response_code);

		if (response_code == RESPONSE_SUCCESS){
			for (uint8_t i = 0; i < (num_read - 1); i++) {
				read_byte = Wire.read();
				//APP_DBG("%c", read_byte);

				if (read_byte == 0) {
					Wire.endTransmission();

					if (result.length()) {
						return result;
					}
					else {
						break;
					}
				}

				result += (char)(read_byte);
			}
		}
	}

	return String("");
}

String read_Atlas_EC_TDS_SAL(void) {
	uint8_t response_code = RESPONSE_NO_DATA;
	uint8_t num_read = 0;
	uint8_t read_byte;
	String result;

	//ENTRY_CRITICAL();
	//num_read = Wire.requestFrom(EC_ADDR, 20, 1);
	num_read = Wire.requestWaitFrom(EC_ADDR, 20, 'r', 1, 1, EC_DELAY);
	//EXIT_CRITICAL();
	//APP_DBG("num_read: %d\n", num_read);

	if (num_read) {
		response_code = Wire.read();
		APP_PRINT("response_code: %d\n", response_code);

		if (response_code == RESPONSE_SUCCESS){
			for (uint8_t i = 0; i < (num_read - 1); i++) {
				read_byte = Wire.read();
				//xprintf("%c", read_byte);

				if (read_byte == 0) {
					Wire.endTransmission();

					if (result.length()) {
						return result;
					}
					else {
						break;
					}
				}

				result += (char)(read_byte);
			}
		}
	}

	return String(",,,");
}

String Atlas_sensor_cmd(uint8_t addr, uint8_t* cmd, uint32_t len) {
	uint8_t response_code = RESPONSE_NO_DATA;
	uint8_t num_read = 0;
	uint8_t read_byte;
	String result;

	Wire.beginTransmission(addr);
	Wire.write((uint8_t*)cmd, len);
	ENTRY_CRITICAL();
	Wire.endTransmission();
	delay(2000);
	num_read = Wire.readFrom(addr, 20, 1);
	EXIT_CRITICAL();
	//APP_DBG("num_read: %d\n", num_read);

	if (num_read) {
		response_code = Wire.read();
		APP_PRINT("response_code: %d\n", response_code);

		if (response_code == RESPONSE_SUCCESS){
			for (uint8_t i = 0; i < (num_read - 1); i++) {
				read_byte = Wire.read();
				//xprintf("%c", read_byte);

				if (read_byte == 0) {
					Wire.endTransmission();

					if (result.length()) {
						return result;
					}
					else {
						break;
					}
				}

				result += (char)(read_byte);
			}
		}
	}

	return String("");
}
