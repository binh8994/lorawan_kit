#ifndef __APP_DBG_H__
#define __APP_DBG_H__

#include "xprintf.h"

#if defined(APP_DBG_EN)
#define APP_DBG(fmt, ...)       xprintf("[DBG] " fmt, ##__VA_ARGS__)
#define APP_DBG_HEX(buffer, length)	do { for(int i = 0; i < length; i++) { xprintf("%02x ", *((char*)buffer + i));}; xprintf("\n"); } while (0);
#define APP_DBG_MSG(buffer, length)	do { for(int i = 0; i < length; i++) { xprintf("%c ", *((char*)buffer + i));}; xprintf("\n"); } while (0);
#else
#define APP_DBG(fmt, ...)
#define APP_DBG_HEX(buffer, length)
#define APP_DBG_MSG(buffer, length)
#endif

#if defined(APP_PRINT_EN)
#define APP_PRINT(fmt, ...)       xprintf("[PRINT] " fmt, ##__VA_ARGS__)
#define APP_PRINT_HEX(buffer, length)	do { for(int i = 0; i < length; i++) { xprintf("%02x ", *((char*)buffer + i));}; xprintf("\n"); } while (0);
#define APP_PRINT_MSG(buffer, length)	do { for(int i = 0; i < length; i++) { xprintf("%c ", *((char*)buffer + i));}; xprintf("\n"); } while (0);
#else
#define APP_PRINT(fmt, ...)
#define APP_PRINT_HEX(buffer, length)
#define APP_PRINT_MSG(buffer, length)
#endif

#if defined(LOGIN_PRINT_EN)
#define LOGIN_PRINT(fmt, ...)       xdymcprintf(fmt, ##__VA_ARGS__)
#else
#define LOGIN_PRINT(fmt, ...)
#endif

#if defined(APP_DBG_SIG_EN)
#define APP_DBG_SIG(fmt, ...)       xprintf("-SIG-> " fmt, ##__VA_ARGS__)
#else
#define APP_DBG_SIG(fmt, ...)
#endif

#endif //__APP_DBG_H__
