#include <string.h>

#include "../ak/fsm.h"
#include "../ak/port.h"
#include "../ak/timer.h"
#include "../ak/message.h"

#include "../sys/sys_ctrl.h"
#include "../sys/sys_dbg.h"
#include "../sys/sys_io.h"
#include "../common/xprintf.h"

#include "app.h"
#include "app_dbg.h"
#include "task_list.h"
#include "app_bsp.h"
#include "app_non_clear_ram.h"

#include "loramac/radio/sx1276/sx1276_cfg.h"
#include "loramac/radio/sx1276/sx1276.h"

#include "loramac/mac/LoRaMac-definitions.h"
#include "loramac/mac/LoRaMac.h"
#include "loramac/mac/LoRaMacCrypto.h"

#include "loramac/region/Region.h"

#include "loramac/crypto/aes.h"
#include "loramac/crypto/cmac.h"

#include "lorawan.h"
#include "app_eeprom.h"
#include "eeprom.h"

static lorawan_setting_t setting;

static uint16_t ChanMask = 0x0001;
static Rx2ChannelParams_t Rx2Chan = {433175000, DR_0};

static LoRaMacPrimitives_t LoRaMacPrimitives;
static LoRaMacCallback_t LoRaMacCallbacks;
static MibRequestConfirm_t mibReq;

static void McpsConfirm( McpsConfirm_t *mcpsConfirm );
static void McpsIndication( McpsIndication_t *mcpsIndication );
static void MlmeConfirm( MlmeConfirm_t *mlmeConfirm );

void lorawan_init() {
	APP_DBG("lorawan_init()\n");

	LoRaMacPrimitives.MacMcpsConfirm = McpsConfirm;
	LoRaMacPrimitives.MacMcpsIndication = McpsIndication;
	LoRaMacPrimitives.MacMlmeConfirm = MlmeConfirm;
	LoRaMacCallbacks.GetBatteryLevel = 0;
	LoRaMacInitialization( &LoRaMacPrimitives, &LoRaMacCallbacks, LORAMAC_REGION_EU433 );

	mibReq.Type = MIB_ADR;
	mibReq.Param.AdrEnable = false;
	LoRaMacMibSetRequestConfirm( &mibReq );

	mibReq.Type = MIB_REPEATER_SUPPORT;
	mibReq.Param.EnableRepeaterSupport = false;
	LoRaMacMibSetRequestConfirm( &mibReq );

	mibReq.Type = MIB_PUBLIC_NETWORK;
	mibReq.Param.EnablePublicNetwork = true;
	LoRaMacMibSetRequestConfirm( &mibReq );

	mibReq.Type = MIB_DEVICE_CLASS;
	mibReq.Param.Class = CLASS_A;
	LoRaMacMibSetRequestConfirm( &mibReq );

	mibReq.Type = MIB_CHANNELS_DEFAULT_MASK;
	mibReq.Param.ChannelsDefaultMask = &ChanMask;
	LoRaMacMibSetRequestConfirm( &mibReq );

	mibReq.Type = MIB_CHANNELS_MASK;
	mibReq.Param.ChannelsMask = &ChanMask;
	LoRaMacMibSetRequestConfirm( &mibReq );

	mibReq.Type = MIB_CHANNELS_DATARATE;
	mibReq.Param.ChannelsDatarate = DR_0;
	LoRaMacMibSetRequestConfirm( &mibReq );

	mibReq.Type = MIB_CHANNELS_DEFAULT_DATARATE;
	mibReq.Param.ChannelsDefaultDatarate = DR_0;
	LoRaMacMibSetRequestConfirm( &mibReq );

	mibReq.Type = MIB_RX2_DEFAULT_CHANNEL;
	mibReq.Param.Rx2DefaultChannel = Rx2Chan;
	LoRaMacMibSetRequestConfirm( &mibReq );

	mibReq.Type = MIB_RX2_CHANNEL;
	mibReq.Param.Rx2Channel = Rx2Chan;
	LoRaMacMibSetRequestConfirm( &mibReq );

	mibReq.Type = MIB_CHANNELS_DEFAULT_TX_POWER;
	mibReq.Param.ChannelsDefaultTxPower = TX_POWER_0;
	LoRaMacMibSetRequestConfirm( &mibReq );

	mibReq.Type = MIB_CHANNELS_TX_POWER;
	mibReq.Param.ChannelsTxPower = TX_POWER_0;
	LoRaMacMibSetRequestConfirm( &mibReq );

	mibReq.Type = MIB_ANTENNA_GAIN;
	mibReq.Param.AntennaGain = 0.0;
	LoRaMacMibSetRequestConfirm( &mibReq );

}

lorawan_status_t lorawan_join() {
	APP_DBG("lorawan_join()\n");

	eeprom_read(LORAWAN_SETTING_ADDR, (uint8_t*)&setting, sizeof(lorawan_setting_t));

	if (setting.nwk_joined == LORAWAN_NWK_JOINED) {

		//APP_PRINT("NetID:0x%08X\n", setting.nwk_id);
		APP_PRINT("DevAddr:0x%08X\n", setting.dev_addr);
		//APP_PRINT("NwkSKey: ");
		//APP_PRINT_HEX(setting.nwk_skey, 16);
		//APP_PRINT("AppSKey: ");
		//APP_PRINT_HEX(setting.app_skey, 16);

		mibReq.Type = MIB_NET_ID;
		mibReq.Param.NetID = setting.nwk_id;
		LoRaMacMibSetRequestConfirm( &mibReq );

		mibReq.Type = MIB_DEV_ADDR;
		mibReq.Param.DevAddr = setting.dev_addr;
		LoRaMacMibSetRequestConfirm( &mibReq );

		mibReq.Type = MIB_NWK_SKEY;
		mibReq.Param.NwkSKey = (uint8_t*)setting.nwk_skey;
		LoRaMacMibSetRequestConfirm( &mibReq );

		mibReq.Type = MIB_APP_SKEY;
		mibReq.Param.AppSKey = (uint8_t*)setting.app_skey;
		LoRaMacMibSetRequestConfirm( &mibReq );

		mibReq.Type = MIB_NETWORK_JOINED;
		mibReq.Param.IsNetworkJoined = true;
		LoRaMacMibSetRequestConfirm( &mibReq );

		return LORAWAN_JOIN_FROM_SETTING;
	}
	else if (setting.nwk_joined == LORAWAN_NWK_FREE){

		APP_PRINT("DevEui: ");
		APP_PRINT_HEX(setting.device_Eui, 8);
		//APP_PRINT("AppEui: ");
		//APP_PRINT_HEX(setting.app_Eui, 8);
		//APP_PRINT("AppKey: ");
		//APP_PRINT_HEX(setting.app_key, 16);

		MlmeReq_t mlmeReq;
		mlmeReq.Type = MLME_JOIN;
		mlmeReq.Req.Join.DevEui = setting.device_Eui;
		mlmeReq.Req.Join.AppEui = setting.app_Eui;
		mlmeReq.Req.Join.AppKey = setting.app_key;
		mlmeReq.Req.Join.NbTrials = 3;

		LoRaMacMlmeRequest( &mlmeReq );

		return LORAWAN_REQUEST_OK;
	}

	APP_DBG("Setting network error\n");
	return LORAWAN_JOIN_ERROR;
}

lorawan_status_t lorawan_send(void* fBuffer, uint8_t length) {
	APP_DBG("lorawan_send()\n");

	MibRequestConfirm_t mibReq;
	LoRaMacStatus_t status;

	mibReq.Type = MIB_NETWORK_JOINED;
	status = LoRaMacMibGetRequestConfirm( &mibReq );

	if( status == LORAMAC_STATUS_OK ) {
		if( mibReq.Param.IsNetworkJoined == true ) {

			McpsReq_t mcpsReq;
			LoRaMacTxInfo_t txInfo;
			lorawan_status_t return_status = LORAWAN_UNKNOWN_ERROR;

			if( LoRaMacQueryTxPossible( length, &txInfo ) != LORAMAC_STATUS_OK )
			{
				mcpsReq.Type = MCPS_UNCONFIRMED;
				mcpsReq.Req.Unconfirmed.fPort = 0;
				mcpsReq.Req.Unconfirmed.fBuffer = 0;
				mcpsReq.Req.Unconfirmed.fBufferSize = 0;
				mcpsReq.Req.Unconfirmed.Datarate = LORAWAN_DEFAULT_DATARATE;

				return_status = LORAWAN_LENGTH_ERROR;
			}
			else {
				mcpsReq.Type = MCPS_UNCONFIRMED;
				mcpsReq.Req.Unconfirmed.fPort = LORAWAN_PORT_REPORT;
				mcpsReq.Req.Unconfirmed.fBuffer = fBuffer;
				mcpsReq.Req.Unconfirmed.fBufferSize = length;
				mcpsReq.Req.Unconfirmed.Datarate = LORAWAN_DEFAULT_DATARATE;

				return_status = LORAWAN_REQUEST_OK;
			}

			if(LoRaMacMcpsRequest( &mcpsReq ) != LORAMAC_STATUS_OK) {
				return_status = LORAWAN_UNKNOWN_ERROR;
			}
			return return_status;
		}
		else {
			APP_DBG("Join nwk before\n");
			return LORAWAN_JOIN_ERROR;
		}
	}

	return LORAWAN_UNKNOWN_ERROR;
}

static void McpsConfirm( McpsConfirm_t *mcpsConfirm ) {
	if( mcpsConfirm->Status == LORAMAC_EVENT_INFO_STATUS_OK ) {
		switch( mcpsConfirm->McpsRequest ) {
		case MCPS_UNCONFIRMED: {
			APP_PRINT("McpsConf-UNCONF\n");

			device_setting_t setting;
			eeprom_read(DEVICE_SETTING_ADDR, (uint8_t*)&setting, sizeof(device_setting_t));
			APP_PRINT("Periodic timer: %d (min)\n", setting.periodic_timer);
			if (setting.enable_sleep == 1) {
				goto_sleep_mode(setting.periodic_timer * 60);
			}

			break;
		}
		case MCPS_CONFIRMED: {
			APP_PRINT("McpsConf-CONF\n");
			break;
		}
		case MCPS_PROPRIETARY: {
			APP_PRINT("McpsConf-PROP\n");
			break;
		}
		default:
			break;
		}

	}
}

static void McpsIndication( McpsIndication_t *mcpsIndication ) {
	if( mcpsIndication->Status != LORAMAC_EVENT_INFO_STATUS_OK ) {
		return;
	}

	switch( mcpsIndication->McpsIndication ) {
	case MCPS_UNCONFIRMED: {
		APP_PRINT("McpsInd-UNCONF\n");
		break;
	}
	case MCPS_CONFIRMED: {
		APP_PRINT("McpsInd-CONF\n");
		break;
	}
	case MCPS_PROPRIETARY: {
		APP_PRINT("McpsInd-PRO\n");
		break;
	}
	case MCPS_MULTICAST: {
		APP_PRINT("McpsIndn:MULT\n");
		break;
	}
	default:
		break;
	}

	if( mcpsIndication->RxData == true ) {
		APP_PRINT("McpsInd-RxData:true\n");

		switch (mcpsIndication->Port) {
		case LORAWAN_PORT_CONTROL:
			task_post_common_msg(AC_TASK_ALARM_ID, ALARM_RECV_MSG, mcpsIndication->Buffer, mcpsIndication->BufferSize);
			break;
		case LORAWAN_PORT_SETTING:
			task_post_common_msg(AC_TASK_SETTING_ID, SETTING_RECV_MSG, mcpsIndication->Buffer, mcpsIndication->BufferSize);
		default:
			break;
		}

	}
}

static void MlmeConfirm( MlmeConfirm_t *mlmeConfirm ) {
	switch( mlmeConfirm->MlmeRequest ) {
	case MLME_JOIN: {
		if( mlmeConfirm->Status == LORAMAC_EVENT_INFO_STATUS_OK ) {
			APP_PRINT("MlmeConf-JOIN_SUCC\n");

			MibRequestConfirm_t mibReq;
			mibReq.Type = MIB_NET_ID;
			if( LoRaMacMibGetRequestConfirm( &mibReq ) == LORAMAC_STATUS_OK) {
				setting.nwk_id = mibReq.Param.NetID;
				APP_PRINT("NetID:0x%08X\n", setting.nwk_id);
			}
			mibReq.Type = MIB_DEV_ADDR;
			if( LoRaMacMibGetRequestConfirm( &mibReq ) == LORAMAC_STATUS_OK) {
				setting.dev_addr = mibReq.Param.DevAddr;
				APP_PRINT("DevAddr:0x%08X\n", setting.dev_addr);
			}
			mibReq.Type = MIB_NWK_SKEY;
			if( LoRaMacMibGetRequestConfirm( &mibReq ) == LORAMAC_STATUS_OK) {
				memcpy(setting.nwk_skey, mibReq.Param.NwkSKey, 16);
				APP_PRINT("NwkSKey: ");
				APP_PRINT_HEX(setting.nwk_skey, 16);

			}
			mibReq.Type = MIB_APP_SKEY;
			if( LoRaMacMibGetRequestConfirm( &mibReq ) == LORAMAC_STATUS_OK) {
				memcpy(setting.app_skey, mibReq.Param.AppSKey, 16);
				APP_PRINT("AppSKey: ");
				APP_PRINT_HEX(setting.app_skey, 16);
			}
			setting.nwk_joined = LORAWAN_NWK_JOINED;
			eeprom_write(LORAWAN_SETTING_ADDR, (uint8_t*)&setting, sizeof(lorawan_setting_t));

			/* start report sensor perodic */
			timer_set(AC_TASK_SENSOR_ID, SENSOR_INIT, 100, TIMER_ONE_SHOT);

		}
		else {
			APP_PRINT("MlmeConf-JOIN_FAIL\n");
			lorawan_join();
		}
		break;
	}
	case MLME_LINK_CHECK: {
		if( mlmeConfirm->Status == LORAMAC_EVENT_INFO_STATUS_OK ) {
			APP_PRINT("MlmeConf-LINK\n");
		}
		break;
	}
	default:
		break;
	}

}
