#ifndef __LORA_PING_PONG_H__
#define __LORA_PING_PONG_H__

#define SINGLE_CHANNEL_GW_FREQ						433175000
#define SINGLE_CHANNEL_GW_BANDWIDTH					0
// 0: 125 kHz
// 1: 250 kHz
// 2: 500 kHz
// 3: Reserved
#define SINGLE_CHANNEL_GW_SF						12//12
// [SF7..SF12]
#define SINGLE_CHANNEL_GW_DR						0//DR_0
#define SINGLE_CHANNEL_GW_CD						1
// 1: 4/5
// 2: 4/6
// 3: 4/7
// 4: 4/8
#define SINGLE_CHANNEL_GW_TX_POWER					20//14	// dBm
#define SINGLE_CHANNEL_GW_PREAMBLE_LENGTH			8
#define SINGLE_CHANNEL_GW_SYMBOL_TIMEOUT			5
#define SINGLE_CHANNEL_GW_FIX_LENGTH_PAY			false
#define SINGLE_CHANNEL_GW_IQ_TX						false
#define SINGLE_CHANNEL_GW_IQ_RX						false
#define SINGLE_CHANNEL_GW_TX_TIMEOUT				3000
#define SINGLE_CHANNEL_GW_RX_TIMEOUT				0

extern void lora_ping_pong_init();
extern void lora_ping_pong_send();

#endif // __LORA_PING_PONG_H__
