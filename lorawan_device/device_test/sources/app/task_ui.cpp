#include <string.h>
#include "fsm.h"
#include "port.h"
#include "message.h"

#include "sys_ctrl.h"
#include "sys_io.h"

#include "button.h"
#include "app.h"
#include "app_dbg.h"
#include "eeprom.h"

#include "task_list.h"
#include "task_ui.h"
#include "lorawan.h"
#include "lora_ping_pong.h"
#include "app_bsp.h"

static lorawan_setting_t lsetting;

void task_ui(ak_msg_t* msg) {
	switch (msg->sig) {
	case AC_BUTTON_ONE_CLICK:
		break;

	case AC_BUTTON_LONG_PRESS: {
		APP_DBG_SIG("AC_BUTTON_LONG_PRESS\n");

		uint8_t app_Eui[8] = {1, 2, 3, 4, 5, 6, 7, 8};
		uint8_t app_key[16] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16};

		memset(&lsetting, 0, sizeof(lorawan_setting_t));
		lsetting.nwk_joined = LORAWAN_NWK_FREE;
		get_device_Eui(lsetting.device_Eui);
		memcpy(lsetting.app_Eui, app_Eui, 8);
		memcpy(lsetting.app_key, app_key, 16);

		eeprom_write(LORAWAN_SETTING_ADDR, (uint8_t*)&lsetting, sizeof(lorawan_setting_t));
		APP_PRINT("leave network.\n");
		sys_ctrl_reset();
	}
		break;

	case AC_PINGPONG_SENT_TEST:
		APP_DBG_SIG("AC_PINGPONG_SENT_TEST\n");
		lora_ping_pong_send();
		break;

	case AC_LORAWAN_SENT_TEST:
		break;

	default:
		break;
	}
}
