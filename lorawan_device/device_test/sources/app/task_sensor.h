#ifndef __TASK_SENSOR_H__
#define __TASK_SENSOR_H__

#include "WString.h"

/* ATLAS SENSOR DEFINE */
#define PH_ADDR					(99)//0x63
#define DO_ADDR					(97)//0x61
#define ORP_ADDR				(98)//0x62
#define EC_ADDR					(100)//0x64

extern String Atlas_sensor_cmd(uint8_t addr, uint8_t* cmd, uint32_t len);

#endif // __TASK_SENSOR_H__
