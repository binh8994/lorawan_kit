#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "base64.h"

#include "app.h"
#include "app_dbg.h"
#include "app_config.h"

string config_folder;
string config_file_path;

app_config::app_config() {

}

void app_config::initializer(char * file_name) {
	struct stat st;

	string config_folder = static_cast<string>(APP_ROOT_PATH_DISK) + static_cast<string>("/config");

	/* create app root path */
	if (stat(config_folder.data(), &st) == -1) {
		mkdir(config_folder.data(), S_IRWXU | S_IRWXG | S_IRWXO);
	}

	string config_file_path = config_folder + static_cast<string>((const char*)file_name);

	strcpy(m_config_path, config_file_path.data());
}

void app_config::set_config_path_file(char* path) {
	strcpy(m_config_path, (const char*)path);
}

void app_config::get_config_path_file(char* path) {
	strcpy(path, (const char*)m_config_path);
}

int app_config::parser_config_file(void* cfg) {
	app_config_parameter_t* pcfg = (app_config_parameter_t*)cfg;
	struct stat file_info;
	int configure_file_obj = -1;
	int buffer_len;
	char * buffer;

	configure_file_obj = open(m_config_path, O_RDONLY);

	if (configure_file_obj < 0) {
		return -1;
	}

	fstat(configure_file_obj, &file_info);

	buffer_len = file_info.st_size + 1;
	buffer = (char*)malloc(buffer_len);

	if (buffer == NULL) {
		return -1;
	}

	memset(buffer, 0, buffer_len);

	/*get data*/
	ssize_t ret = pread(configure_file_obj, buffer, file_info.st_size, 0);
	(void) ret;

	close(configure_file_obj);

	/*parse data*/
	json j = json::parse(buffer);

	/* azure gateway */
	memset(pcfg->azure_gateway.host_name			, 0, CONFIGURE_PARAMETER_BUFFER_SIZE);
	memset(pcfg->azure_gateway.device_id			, 0, CONFIGURE_PARAMETER_BUFFER_SIZE);
	memset(pcfg->azure_gateway.access_share_key	, 0, CONFIGURE_PARAMETER_BUFFER_SIZE);
	memset(pcfg->azure_gateway.connection_string	, 0, CONFIGURE_PARAMETER_BUFFER_SIZE);

	strcpy(pcfg->azure_gateway.host_name,		j["azure_gateway"]["host_name"].get<string>().data());
	strcpy(pcfg->azure_gateway.device_id,		j["azure_gateway"]["device_id"].get<string>().data());
	strcpy(pcfg->azure_gateway.access_share_key,	j["azure_gateway"]["access_share_key"].get<string>().data());
	strcpy(pcfg->azure_gateway.connection_string,j["azure_gateway"]["connection_string"].get<string>().data());

	free(buffer);

	return 0;
}

int app_config::write_config_data(void* cfg) {
	app_config_parameter_t* pcfg = (app_config_parameter_t*)cfg;
	json js_config = {
		{
			"azure_gateway", {
				{"host_name", pcfg->azure_gateway.host_name},
				{"device_id", pcfg->azure_gateway.device_id},
				{"access_share_key", pcfg->azure_gateway.access_share_key},
				{"connection_string", pcfg->azure_gateway.connection_string},
			}
		}
	};

	string config = js_config.dump();
	const char* buffer = config.data();

	FILE* js_config_obj = fopen(m_config_path, "w");

	if (js_config_obj == NULL) {
		return -1;
	}

	fwrite(buffer, 1, config.length(), js_config_obj);

	fclose(js_config_obj);

	return 0;
}
