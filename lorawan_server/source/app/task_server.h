#ifndef __TASK_SERVER_H__
#define __TASK_SERVER_H__

#include "message.h"
#include "port.h"

#define SERVER_PHY_MAXPAYLOAD		255

typedef struct {
	uint32_t network_id;
	uint8_t application_key[16];
} __AK_PACKETED serv_init_param_t;

typedef struct {
	uint32_t tx_frequency;
	uint8_t tx_datarater;
	uint8_t tx_power;
} __AK_PACKETED serv_tx_config_t;

typedef struct {
	uint32_t rx_frequency;
	uint8_t rx_datarater;
} __AK_PACKETED serv_rx_config_t;

typedef struct {
	uint8_t devive_Eui[8];
	uint8_t application_Eui[8];
	uint32_t timeout;
} __AK_PACKETED serv_join_permit_t;

typedef struct {
	uint8_t devive_Eui[8];
	uint8_t application_Eui[8];
	uint32_t device_address;
} __AK_PACKETED serv_join_acept_t;

typedef struct {
	struct {
		uint32_t device_address;
		bool receive_ack;
		uint8_t port;
		uint16_t data_len;
	} __AK_PACKETED header;
	uint8_t data[SERVER_PHY_MAXPAYLOAD];
} __AK_PACKETED serv_unconfirmed_msg_t;

typedef struct {
	struct {
		uint32_t device_address;
		bool receive_ack;
		uint8_t retries;
		uint8_t port;
		uint16_t data_len;
	} __AK_PACKETED header;
	uint8_t data[SERVER_PHY_MAXPAYLOAD];
} __AK_PACKETED serv_confirmed_msg_t;

extern q_msg_t gw_task_server_mailbox;
extern void* gw_task_server_entry(void*);

#endif // __TASK_SERVER_H__
