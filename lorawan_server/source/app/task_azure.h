#ifndef __TASK_AZURE_H__
#define __TASK_AZURE_H__

#include "message.h"
#include "port.h"

#define MAXIMUM_DATA_LENGTH				47

typedef enum {
	LORAWAN_REQUEST_OK,
	LORAWAN_LENGTH_ERROR,
	LORAWAN_JOIN_ERROR,
	LORAWAN_UNKNOWN_ERROR,
	LORAWAN_JOIN_FROM_SETTING,
} lorawan_status_t;

enum {
	LORAWAN_PORT_REPORT = 50,
	LORAWAN_PORT_CONTROL = 150,
	LORAWAN_PORT_SETTING = 200,
	LORAWAN_PORT_GETTING = 210,
};

enum {
	LORAWAN_DEVICE_TYPE_DEFAULT,
	LORAWAN_DEVICE_TYPE_SHIMIZU,
};

enum {
	LORAWAN_FUNCTION_TYPE_SENSOR_A_REPORT,
	LORAWAN_FUNCTION_TYPE_CONTROL_A_REPORT,
	LORAWAN_FUNCTION_TYPE_CONTROL_B_REPORT,
	LORAWAN_FUNCTION_TYPE_CONTROL_A_CONTROL,
	LORAWAN_FUNCTION_TYPE_CONTROL_B_CONTROL,
	LORAWAN_FUNCTION_TYPE_PERIODIC_TIMER_SETTING,
	LORAWAN_FUNCTION_TYPE_CONTROL_B_GETTING,
	LORAWAN_FUNCTION_TYPE_SENSOR_B_REPORT,
};

enum {
	LORAWAN_DATA_TYPE_UINT1,
	LORAWAN_DATA_TYPE_UINT8,
	LORAWAN_DATA_TYPE_UINT16,
	LORAWAN_DATA_TYPE_UINT32,
	LORAWAN_DATA_TYPE_FLOAT,
	LORAWAN_DATA_TYPE_DOUBLE,
	LORAWAN_DATA_TYPE_STRING,
	LORAWAN_DATA_TYPE_ARRAY,
};

typedef struct {
	uint8_t device_type;
	uint8_t function_type;
	uint8_t data_type;
	uint8_t data_length;
} __AK_PACKETED lorawan_header_msg_t;

typedef struct {
	lorawan_header_msg_t header;
	uint8_t data[MAXIMUM_DATA_LENGTH];
} __AK_PACKETED lorawan_msg_t;

typedef struct {
	int16_t ph_inter;
	int16_t ph_decac;
	int16_t do_inter;
	int16_t do_decac;
	int16_t orp_inter;
	int16_t orp_decac;
	int32_t ec_inter;
	int32_t ec_decac;
	int32_t tds_inter;
	int32_t tds_decac;
	int32_t sal_inter;
	int32_t sal_decac;
	int16_t tur;
	int16_t temp_inter;
	int16_t temp_decac;
	int8_t bat;
} __AK_PACKETED shimizu_sensor_a_t;

typedef struct {
	int16_t hum_inter;
	int16_t hum_decac;
	uint16_t lux;
	int16_t temp_inter;
	int16_t temp_decac;
	int8_t bat;
} __AK_PACKETED shimizu_sensor_b_t;

typedef struct {
	uint8_t output;
	uint8_t timeout;
} __AK_PACKETED shimizu_control_a_t;

typedef struct {
	uint8_t mask;
	uint8_t output;
	uint8_t timeout;
} __AK_PACKETED shimizu_control_b_t;

typedef struct {
	uint32_t periodic_timer;//(ms)
} __AK_PACKETED shimizu_setting_t;

extern q_msg_t gw_task_azure_mailbox;
extern void* gw_task_azure_entry(void*);

#endif //__TASK_AZURE_H__
