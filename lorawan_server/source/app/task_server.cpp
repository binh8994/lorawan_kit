#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/signal.h>
#include <fcntl.h>
#include <termios.h>
#include <errno.h>

#include "ak.h"
#include "timer.h"

#include "app.h"
#include "app_if.h"
#include "app_dbg.h"
#include "app_data.h"

#include "task_list.h"
#include "task_server.h"

#define CMD_UART_FRAME_TO_INTERVAL		500
#define CMD_UART_SOP_CHAR				0xEF
#define CMD_UART_DEVPATH				"/dev/ttyUSB0"
#define CMD_UART_DATA_SIZE				254

static struct cmd_uart_frame_t {
	uint8_t		frame_sop;
	uint32_t	len;
	uint8_t		data_index;
	uint8_t		data[CMD_UART_DATA_SIZE];
	uint8_t		frame_fcs;
} cmd_uart_frame;

#define CMD_RETURN_OK					(1)
#define CMD_RETURN_ERR					(0)

enum {
	MODE_ENDDEVICE,
	MODE_FORWARD,
	MODE_SERVER
};

enum {
	CMD_SET_MODE,

	CMD_ENDD_SET_ACTIVE_MODE,
	CMD_ENDD_OTA_STATUS,
	CMD_ENDD_SET_DEV_EUI ,
	CMD_ENDD_SET_APP_EUI,
	CMD_ENDD_SET_APP_KEY,
	CMD_ENDD_SET_NET_ID,
	CMD_ENDD_SET_DEV_ADDR,
	CMD_ENDD_SET_NKW_SKEY,
	CMD_ENDD_SET_APP_SKEY,
	CMD_ENDD_SET_CLASS,
	CMD_ENDD_SET_CHAN_MASK,
	CMD_ENDD_SET_DATARATE,
	CMD_ENDD_SET_RX2_CHAN,
	CMD_ENDD_SET_RX2_DR,
	CMD_ENDD_SET_TX_PWR,
	CMD_ENDD_SET_ANT_GAIN,
	CMD_ENDD_GET_DEV_EUI,
	CMD_ENDD_GET_APP_EUI,
	CMD_ENDD_GET_APP_KEY,
	CMD_ENDD_GET_NET_ID,
	CMD_ENDD_GET_DEV_ADDR,
	CMD_ENDD_GET_NKW_SKEY,
	CMD_ENDD_GET_APP_SKEY,
	CMD_ENDD_GET_CLASS,
	CMD_ENDD_GET_CHAN_MASK,
	CMD_ENDD_GET_DATARATE,
	CMD_ENDD_GET_RX2_CHAN,
	CMD_ENDD_GET_RX2_DR,
	CMD_ENDD_GET_TX_PWR,
	CMD_ENDD_GET_ANT_GAIN,
	CMD_ENDD_SEND_UNCONFIRM_MSG,
	CMD_ENDD_SEND_CONFIRM_MSG,
	CMD_ENDD_RECV_CONFIRM_MSG,
	CMD_ENDD_RECV_UNCONFIRM_MSG,

	CMD_FW_SET_TX_CONFIG,
	CMD_FW_GET_TX_CONFIG,
	CMD_FW_SET_RX_CONFIG,
	CMD_FW_GET_RX_CONFIG,
	CMD_FW_SEND,
	CMD_FW_RECV,

	CMD_SERV_SET_TX_CONFIG,
	CMD_SERV_GET_TX_CONFIG,
	CMD_SERV_SET_RX_CONFIG,
	CMD_SERV_GET_RX_CONFIG,
	CMD_SERV_SET_NET_PARAM,
	CMD_SERV_GET_NET_PARAM,
	CMD_SERV_SET_JOINT_PERMIT,
	CMD_SERV_GET_JOINT_PERMIT,
	CMD_SERV_RESP_JOINT_PERMIT,
	CMD_SERV_REMOVE_DEVICE,
	CMD_SERV_REMOVE_DEVICE_ALL,
	CMD_SERV_SEND_UNCONFIRM_MSG,
	CMD_SERV_SEND_CONFIRM_MSG,
	CMD_SERV_RECV_UNCONFIRM_MSG,
	CMD_SERV_RECV_CONFIRM_MSG,
	CMD_SERV_RECV_CONFIRM,
};

typedef struct {
	uint8_t des_task_id;
	uint8_t des_sig;
} cmd_uart_tbl_t;

static cmd_uart_tbl_t Cmd_uart_tbl[] = \
{
	{GW_TASK_SERVER_ID, SERVER_SET_TX_CONFIG		},	//CMD_SET_MODE,
	//
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	//
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	//
	{GW_TASK_SERVER_ID, SERVER_SET_RX_CONFIG		},	//CMD_SERV_SET_TX_CONFIG,
	{0, 0},												//CMD_SERV_GET_TX_CONFIG,
	{GW_TASK_SERVER_ID, SERVER_SET_NET_PARAM		},	//CMD_SERV_SET_RX_CONFIG,
	{0, 0},												//CMD_SERV_GET_RX_CONFIG,
	{0, 0},												//CMD_SERV_SET_NET_PARAM,
	{0, 0},												//CMD_SERV_GET_NET_PARAM,
	{0, 0},												//CMD_SERV_SET_JOINT_PERMIT,
	{0, 0},												//CMD_SERV_GET_JOINT_PERMIT,
	{GW_TASK_SERVER_ID, SERVER_RESP_JOIN			},	//CMD_SERV_RESP_JOINT_PERMIT,
	{0, 0},												//CMD_SERV_REMOVE_DEVICE,
	{0, 0},												//CMD_SERV_REMOVE_DEVICE_ALL,
	{0, 0},												//CMD_SERV_SEND_UNCONFIRM_MSG,
	{0, 0},												//CMD_SERV_SEND_CONFIRM_MSG,
	{GW_TASK_SERVER_ID, SERVER_RECV_UNCONFIRM_MSG	},	//CMD_SERV_RECV_UNCONFIRM_MSG,
	{GW_TASK_SERVER_ID, SERVER_RECV_CONFIRM_MSG		},	//CMD_SERV_RECV_CONFIRM_MSG,
	{0, 0},												//CMD_SERV_RECV_CONFIRM,
};

typedef struct {
	uint8_t cmd_id;
	uint8_t* cmd_data;
} cmd_t;

#define UART_TX_BUFFER				256
#define GW_FREQ						433175000
#define GW_BANDWIDTH				0
// 0: 125 kHz
// 1: 250 kHz
// 2: 500 kHz
#define GW_DR						0
#define GW_TX_POWER					20
#define DEFAULT_KEY					{ 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10 }

static serv_tx_config_t Serv_tx_config  = { GW_FREQ, GW_DR, GW_TX_POWER};
static serv_rx_config_t Serv_rx_config = { GW_FREQ, GW_DR};
static serv_init_param_t Serv_init_param = {0, DEFAULT_KEY};

static int cmd_uart_fd;

static int cmd_uart_opentty(const char* devpath);
static uint8_t cmd_uart_calcfcs(uint8_t len, uint8_t *data_ptr);

static pthread_t cmd_uart_rx_thread;
static void* cmd_uart_rx_thread_handler(void*);

#define SOP_STATE						0x00
#define LEN_STATE						0x01
#define DATA_STATE						0x02
#define FCS_STATE						0x03
static uint8_t rx_frame_state = SOP_STATE;

static void rx_frame_parser(uint8_t* data, uint8_t len);
int cmd_uart_tx_frame_post(cmd_t* data, uint8_t data_len);

static uint8_t tx_buffer[UART_TX_BUFFER];

q_msg_t gw_task_server_mailbox;

void* gw_task_server_entry(void*) {
	ak_msg_t* msg = AK_MSG_NULL;
	wait_all_tasks_started();

	APP_DBG("[STARTED] gw_task_server_entry\n");

	if (cmd_uart_opentty(CMD_UART_DEVPATH) < 0) {
		APP_DBG("Cannot open %s !\n", CMD_UART_DEVPATH);
	}
	else {
		APP_DBG("Opened %s success !\n", CMD_UART_DEVPATH);\
		pthread_create(&cmd_uart_rx_thread, NULL, cmd_uart_rx_thread_handler, NULL);
	}

	while (1) {
		/* get messge */
		msg = ak_msg_rev(GW_TASK_SERVER_ID);

		switch (msg->header->sig) {
		case SERVER_UART_TO: {
			APP_DBG_SIG("SERVER_UART_TO\n");
			rx_frame_state = SOP_STATE;
		}
			break;

		case SERVER_INIT: {
			APP_DBG_SIG("SERVER_INIT\n");

			uint8_t mode = MODE_SERVER;
			cmd_t send_serv_init;
			send_serv_init.cmd_id =  CMD_SET_MODE;
			send_serv_init.cmd_data = &mode;
			cmd_uart_tx_frame_post(&send_serv_init, sizeof(mode));
		}
			break;

		case SERVER_SET_TX_CONFIG: {
			APP_DBG_SIG("SERVER_SET_TX_CONFIG\n");

			cmd_t send_tx_cfg;
			send_tx_cfg.cmd_id =  CMD_SERV_SET_TX_CONFIG;
			send_tx_cfg.cmd_data = (uint8_t*)&Serv_tx_config;
			cmd_uart_tx_frame_post(&send_tx_cfg, sizeof(serv_tx_config_t));
		}
			break;

		case SERVER_SET_RX_CONFIG: {
			APP_DBG_SIG("SERVER_SET_RX_CONFIG\n");

			cmd_t send_rx_cfg;
			send_rx_cfg.cmd_id =  CMD_SERV_SET_RX_CONFIG;
			send_rx_cfg.cmd_data = (uint8_t*)&Serv_rx_config;
			cmd_uart_tx_frame_post(&send_rx_cfg, sizeof(serv_rx_config_t));

		}
			break;

		case SERVER_SET_NET_PARAM: {
			APP_DBG_SIG("SERVER_SET_NET_PARAM\n");

			cmd_t send_rx_cfg;
			send_rx_cfg.cmd_id =  CMD_SERV_SET_NET_PARAM;
			send_rx_cfg.cmd_data = (uint8_t*)&Serv_init_param;
			cmd_uart_tx_frame_post(&send_rx_cfg, sizeof(serv_init_param_t));

		}
			break;

		case SERVER_PERMIT_JOIN: {
			APP_DBG_SIG("SERVER_PERMIT\n");

			serv_join_permit_t* send_join_permit = (serv_join_permit_t*)msg->header->payload;
			cmd_t send_permit;
			send_permit.cmd_id =  CMD_SERV_SET_JOINT_PERMIT;
			send_permit.cmd_data = (uint8_t*)send_join_permit;
			cmd_uart_tx_frame_post(&send_permit, sizeof(serv_join_permit_t));

			APP_DBG("device_Eui:");	APP_DBG_HEX(send_join_permit->devive_Eui, 8);
			APP_DBG("application_Eui:");	APP_DBG_HEX(send_join_permit->application_Eui, 8);
		}
			break;

		case SERVER_RESP_JOIN: {
			APP_DBG_SIG("SERVER_RESP_JOIN\n");
			serv_join_acept_t* serv_join_acept;
			if (get_data_len_dynamic_msg(msg) == sizeof(serv_join_acept_t)) {
				serv_join_acept = (serv_join_acept_t*)msg->header->payload;
				APP_DBG("device_Eui:");	APP_DBG_HEX(serv_join_acept->devive_Eui, 8);
				APP_DBG("device_address: x%08X\n", serv_join_acept->device_address);
			}
		}
			break;

		case SERVER_SEND_UNCONFIRM_MSG: {
			APP_DBG_SIG("SERVER_SEND_UNCONFIRM_MSG\n");

			serv_unconfirmed_msg_t* send_unconfirmed_msg = (serv_unconfirmed_msg_t*)msg->header->payload;
			cmd_t send_msg;
			send_msg.cmd_id =  CMD_SERV_SEND_UNCONFIRM_MSG;
			send_msg.cmd_data = (uint8_t*)send_unconfirmed_msg;
			cmd_uart_tx_frame_post(&send_msg, get_data_len_dynamic_msg(msg));
		}
			break;

		case SERVER_SEND_CONFIRM_MSG: {
			APP_DBG_SIG("SERVER_SEND_CONFIRM_MSG\n");

			serv_confirmed_msg_t* send_confirmed_msg = (serv_confirmed_msg_t*)msg->header->payload;
			cmd_t send_msg;
			send_msg.cmd_id =  CMD_SERV_SEND_CONFIRM_MSG;
			send_msg.cmd_data = (uint8_t*)send_confirmed_msg;
			cmd_uart_tx_frame_post(&send_msg, get_data_len_dynamic_msg(msg));

		}
			break;

		case SERVER_REMOVE_DEVICE: {
			APP_DBG_SIG("SERVER_REMOVE_DEVICE\n");

			uint32_t* serv_device_address = (uint32_t*)msg->header->payload;
			cmd_t send_remove;
			send_remove.cmd_id = CMD_SERV_REMOVE_DEVICE;
			send_remove.cmd_data = (uint8_t*)serv_device_address;
			cmd_uart_tx_frame_post(&send_remove, sizeof(uint32_t));
		}
			break;

		case SERVER_REMOVE_DEVICE_ALL: {
			APP_DBG_SIG("SERVER_REMOVE_DEVICE_ALL\n");

			cmd_t send_remove;
			send_remove.cmd_id = CMD_SERV_REMOVE_DEVICE_ALL;
			send_remove.cmd_data = NULL;
			cmd_uart_tx_frame_post(&send_remove, 0);
		}
			break;

		case SERVER_RECV_UNCONFIRM_MSG: {
			APP_DBG_SIG("SERVER_RECV_UNCONFIRM_MSG\n");
			serv_unconfirmed_msg_t* serv_unconfirmed_msg;

			if (get_data_len_common_msg(msg) >= sizeof(serv_unconfirmed_msg->header)) {
				serv_unconfirmed_msg = (serv_unconfirmed_msg_t*)msg->header->payload;
				APP_DBG("device_address: x%08X\n", serv_unconfirmed_msg->header.device_address);
				APP_DBG("port: %d\n", serv_unconfirmed_msg->header.port);
				APP_DBG("data_len: x%08X\n", serv_unconfirmed_msg->header.data_len);
				APP_DBG_HEX(serv_unconfirmed_msg->data, serv_unconfirmed_msg->header.data_len);
			}
		}
			break;

		case SERVER_RECV_CONFIRM_MSG: {
			APP_DBG_SIG("SERVER_RECV_CONFIRM_MSG\n");
			serv_confirmed_msg_t* serv_confirmed_msg;

			if (get_data_len_common_msg(msg) >= sizeof(serv_confirmed_msg->header)) {
				serv_confirmed_msg = (serv_confirmed_msg_t*)msg->header->payload;
				APP_DBG("device_address: x%08X\n", serv_confirmed_msg->header.device_address);
				APP_DBG("port: %d\n", serv_confirmed_msg->header.port);
				APP_DBG("data_len: x%08X\n", serv_confirmed_msg->header.data_len);
				APP_DBG_HEX(serv_confirmed_msg->data, serv_confirmed_msg->header.data_len);
			}
		}
			break;
		default:
			break;
		}

		/* free message */
		ak_msg_free(msg);
	}

	return (void*)0;
}

void* cmd_uart_rx_thread_handler(void*) {
	APP_DBG("cmd_uart_rx_thread_handler entry successed!\n");
	uint8_t rx_buffer[CMD_UART_DATA_SIZE];
	uint32_t rx_read_len;

	task_post_pure_msg(GW_TASK_SERVER_ID, SERVER_INIT);

	while(1) {
		rx_read_len = read(cmd_uart_fd, rx_buffer, CMD_UART_DATA_SIZE);
		if (rx_read_len > 0) {
			rx_frame_parser(rx_buffer, rx_read_len);
		}
		usleep(1);
	}

	return (void*)0;
}

int cmd_uart_opentty(const char* devpath) {
	struct termios options;
	APP_DBG("[cmd_uart_opentty] devpath: %s\n", devpath);

	cmd_uart_fd = open(devpath, O_RDWR | O_NOCTTY | O_NDELAY);
	if (cmd_uart_fd < 0) {
		return cmd_uart_fd;
	}
	else {
		fcntl(cmd_uart_fd, F_SETFL, 0);

		/* get current status */
		tcgetattr(cmd_uart_fd, &options);

		cfsetispeed(&options, B115200);
		cfsetospeed(&options, B115200);

		/* No parity (8N1) */
		options.c_cflag &= ~PARENB;
		options.c_cflag &= ~CSTOPB;
		options.c_cflag &= ~CSIZE;
		options.c_cflag |= CS8;

		options.c_cflag |= (CLOCAL | CREAD);
		options.c_cflag     &=  ~CRTSCTS;

		cfmakeraw(&options);

		tcflush(cmd_uart_fd, TCIFLUSH);
		if (tcsetattr (cmd_uart_fd, TCSANOW, &options) != 0) {
			SYS_DBG("error in tcsetattr()\n");
		}
	}
	return 0;
}

void rx_frame_parser(uint8_t* data, uint8_t len) {
	uint8_t ch;
	int rx_remain;

	while(len) {

		ch = *data++;
		len--;

		switch (rx_frame_state) {
		case SOP_STATE: {
			if (CMD_UART_SOP_CHAR == ch) {
				rx_frame_state = LEN_STATE;
				timer_set(GW_TASK_SERVER_ID, SERVER_UART_TO, CMD_UART_FRAME_TO_INTERVAL, TIMER_ONE_SHOT);
			}
		}
			break;

		case LEN_STATE: {
			if (ch > CMD_UART_DATA_SIZE) {
				timer_remove_attr(GW_TASK_SERVER_ID, SERVER_UART_TO);

				rx_frame_state = SOP_STATE;
				return;
			}
			else {
				cmd_uart_frame.len = ch;
				cmd_uart_frame.data_index = 0;
				rx_frame_state = DATA_STATE;

				timer_set(GW_TASK_SERVER_ID, SERVER_UART_TO, CMD_UART_FRAME_TO_INTERVAL, TIMER_ONE_SHOT);
			}
		}
			break;

		case DATA_STATE: {
			cmd_uart_frame.data[cmd_uart_frame.data_index++] = ch;

			rx_remain = cmd_uart_frame.len - cmd_uart_frame.data_index;

			if (len >= rx_remain) {
				memcpy((uint8_t*)(cmd_uart_frame.data + cmd_uart_frame.data_index), data, rx_remain);
				cmd_uart_frame.data_index += rx_remain;
				len -= rx_remain;
				data += rx_remain;
			}
			else {
				memcpy((uint8_t*)(cmd_uart_frame.data + cmd_uart_frame.data_index), data, len);
				cmd_uart_frame.data_index += len;
				len = 0;
			}

			if (cmd_uart_frame.data_index == cmd_uart_frame.len) {
				rx_frame_state = FCS_STATE;
			}
		}
			break;

		case FCS_STATE: {
			timer_remove_attr(GW_TASK_SERVER_ID, SERVER_UART_TO);

			rx_frame_state = SOP_STATE;

			cmd_uart_frame.frame_fcs = ch;

			if (cmd_uart_frame.frame_fcs \
					== cmd_uart_calcfcs(cmd_uart_frame.len, cmd_uart_frame.data)) {

				uint8_t* cmd_id = &cmd_uart_frame.data[0];
				uint8_t* dest_task_id = &Cmd_uart_tbl[*cmd_id].des_task_id;
				uint8_t* dest_sig = &Cmd_uart_tbl[*cmd_id].des_sig;
				//APP_DBG("cmd_id:%d\n", *cmd_id);
				//APP_DBG("dest_task_id:%d\n", *dest_task_id);
				//APP_DBG("dest_sig:%d\n", *dest_sig);
				switch (*dest_task_id) {
				case GW_TASK_SERVER_ID: {
					if (*dest_sig) {
						task_post_dynamic_msg(*dest_task_id, *dest_sig, &cmd_uart_frame.data[1], cmd_uart_frame.len - sizeof(uint8_t));
					}
				}
					break;

				default: {
					//cmd_t Send_cmd;
					//uint8_t Return_code = CMD_RETURN_ERR;
					//Send_cmd.cmd_id = *cmd_id;
					//Send_cmd.cmd_data = &Return_code;
					//cmd_uart_tx_frame_post((cmd_t*)&Send_cmd, sizeof(Return_code));
				}
					break;
				}
			}
			else {
				/* TODO: handle checksum incorrect */
			}
		}
			break;

		default:
			break;
		}
	}
}

int cmd_uart_tx_frame_post(cmd_t* data, uint8_t data_len) {
	//printf("cmd_uart_tx_frame_post\n");
	uint8_t frame_len = data_len + sizeof(data->cmd_id);
	tx_buffer[0] = CMD_UART_SOP_CHAR;
	tx_buffer[1] = frame_len;
	tx_buffer[2] = data->cmd_id;
	memcpy(&tx_buffer[3], data->cmd_data, data_len);
	tx_buffer[2 + frame_len] = cmd_uart_calcfcs(frame_len, &tx_buffer[2]);

	return write(cmd_uart_fd, tx_buffer, (frame_len + 3));
}

uint8_t cmd_uart_calcfcs(uint8_t len, uint8_t *data_ptr) {
	uint8_t xor_result = len;
	for (int i = 0; i < len; i++, data_ptr++) {
		xor_result = xor_result ^ *data_ptr;
	}
	return xor_result;
}
