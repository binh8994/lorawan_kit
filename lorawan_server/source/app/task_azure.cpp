#include <unistd.h>
#include <stdint.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>

#include "app.h"
#include "app_if.h"
#include "app_data.h"
#include "app_dbg.h"
#include "json.hpp"

#include "task_list.h"
#include "task_list_if.h"
#include "if_rf24.h"

#include "task_azure.h"


#define DEBUG_REV_MSG			1

using namespace std;
using json = nlohmann::json;

q_msg_t gw_task_azure_mailbox;

#define GW_REV_BUF_SIZE				4096
#define GW_BACKLOG					1

static int server_fd;
static pthread_t gw_recv_thread_id;
static void* gw_rev_thread_entry(void*);

const char* py_socket = "/tmp/py_socket";
const char* gw_socket = "/tmp/gw_socket";

static int send_message_to_py_app(json* send_json);
static int exec_prog(const char **argv);

void* gw_task_azure_entry(void*) {
	wait_all_tasks_started();

	APP_DBG("[STARTED] gw_task_azure_entry\n");

	struct sockaddr_un addr;

	const char *kill_python_argv[6] = {"/usr/bin/killall",\
									   "python",\
									   NULL};

	const char *run_python_argv[6] = {"/root/python_app/iothub_client_demo.py",\
									  "-p", "MQTT",\
									  "-c", gateway_configure_parameter.azure_gateway.connection_string,\
									  NULL};

	//APP_DBG("gateway_configure_parameter.azure_gateway.connection_string: %s\n", gateway_configure_parameter.azure_gateway.connection_string);

	/* kill python app previous */
	exec_prog(kill_python_argv);
	usleep(500000);
	/* run python app */
	exec_prog(run_python_argv);
	usleep(500000);

	/* create gateway socket */
	server_fd = socket(AF_UNIX, SOCK_STREAM, 0);
	if (server_fd == -1) {
		APP_DBG("[ERR] socket\n");
	}
	/* remove exist path */
	if (remove(gw_socket) == -1 && errno != ENOENT) {
		APP_DBG("[ERR]remove-%s", gw_socket);
	}
	/* bind socket */
	memset(&addr, 0, sizeof(struct sockaddr_un));
	addr.sun_family = AF_UNIX;
	strncpy(addr.sun_path, gw_socket, sizeof(addr.sun_path) - 1);
	if (bind(server_fd, (struct sockaddr *) &addr, sizeof(struct sockaddr_un)) == -1) {
		APP_DBG("[ERR] bind\n");
	}
	/* start listen socket */
	if (listen(server_fd, GW_BACKLOG) == -1) {
		APP_DBG("[ERR]listen\n");
	}

	ak_msg_t* msg;

	//pthread_create(&gw_recv_thread_id, NULL, gw_rev_thread_entry, NULL);

	while (1) {
		/* get messge */
		msg = ak_msg_rev(GW_TASK_AZURE_ID);
		switch (msg->header->sig) {
		case AZURE_SEND: {
			APP_DBG_SIG("PY_SOCKET_SEND_TEST\n");

			//uint8_t* mes = (uint8_t*)msg->header->payload;
			//uint32_t len = msg->header->len;
			//send_message_to_py_app((const char*)mes, len);

		}
			break;

		default:
			break;
		}

		/* free message */
		ak_msg_free(msg);
	}
	return (void*)0;
}

void* gw_rev_thread_entry(void*) {
	APP_DBG("gw_rev_thread_entry\n");
	int client_fd;
	ssize_t num_read;
	char recv_buffer[GW_REV_BUF_SIZE];

	while (1) {
		/* accept client connection */
		client_fd = accept(server_fd, NULL, NULL);
		if (client_fd == -1) {
			APP_DBG("[ERR] accept\n");
		}

		while ((num_read = read(client_fd, recv_buffer, GW_REV_BUF_SIZE)) > 0) {
			recv_buffer[num_read] = '\0';
#if 0
			//APP_DBG("received: %d\n", (int)nums_read);
			//string rev_str = string(rev_buf);
			//rev_str.erase(std::remove(rev_str.begin(), rev_str.end(), '\t'), rev_str.end());
			//rev_str.erase(std::remove(rev_str.begin(), rev_str.end(), ' '), rev_str.end());
			//rev_str.erase(std::remove(rev_str.begin(), rev_str.end(), '\n'), rev_str.end());
#endif
			APP_DBG("%s\n",recv_buffer);
			json recv_json;
			json send_json;
			try {
				recv_json = json::parse(recv_buffer);

				if (recv_json.find("type_msg") != recv_json.end()) {
					string type_msg =  recv_json["type_msg"].get<std::string>();
					/* prepair for response */
					send_json["type_msg"] = type_msg;
					send_json["gateway_id"] = string(gateway_configure_parameter.azure_gateway.device_id);
					//APP_DBG("type_msg: %s\n", type_msg.c_str());

					if (type_msg == string("join_enable")) {
						int values[8];
						serv_join_permit_t join_permit_msg;
						string dev_Eui, app_Eui;

						/* check timeout valid */
						if (recv_json.find("timeout") != recv_json.end()) {
							join_permit_msg.timeout = (uint32_t)recv_json["timeout"].get<int>();

							/* check dev_Eui valid */
							if (recv_json.find("dev_eui") != recv_json.end()) {
								dev_Eui = recv_json["dev_eui"].get<std::string>();

								/* check app_Eui valid */
								if (recv_json.find("app_eui") != recv_json.end()) {
									app_Eui = recv_json["app_eui"].get<std::string>();

									transform(dev_Eui.begin(), dev_Eui.end(), dev_Eui.begin(), ::tolower);
									if ( 8 == sscanf( dev_Eui.c_str(),
													  "%02x%02x%02x%02x%02x%02x%02x%02x%*c",
													  &values[0], &values[1], &values[2], &values[3], &values[4], &values[5], &values[6], &values[7] ) ) {
										/* convert to uint8_t */
										for( int i = 0; i < 8; ++i )	join_permit_msg.devive_Eui[i] = (uint8_t) values[i];
										APP_DBG_HEX(join_permit_msg.devive_Eui, 8);
									}
									else {
										send_json["status"] = string("error:dev_eui");
										send_message_to_py_app(&send_json);

										break;
									}

									transform(app_Eui.begin(), app_Eui.end(), app_Eui.begin(), ::tolower);
									if ( 8 == sscanf( app_Eui.c_str(),
													  "%02x%02x%02x%02x%02x%02x%02x%02x%*c",
													  &values[0], &values[1], &values[2], &values[3], &values[4], &values[5], &values[6], &values[7] ) ) {
										/* convert to uint8_t */
										for( int i = 0; i < 8; ++i )	join_permit_msg.application_Eui[i] = (uint8_t) values[i];
										APP_DBG_HEX(join_permit_msg.application_Eui, 8);
									}
									else {
										send_json["status"] = string("error:=app_eui");
										send_message_to_py_app(&send_json);

										break;
									}

									task_post_dynamic_msg(GW_TASK_AZURE_ID, GW_TASK_SERVER_ID, SERVER_PERMIT_JOIN, (uint8_t*)&join_permit_msg, sizeof(serv_join_permit_t));

									send_json["status"] = string("success");
									send_message_to_py_app(&send_json);
								}
								else {
									send_json["status"] = string("error:=app_eui");
									send_message_to_py_app(&send_json);
								}
							}
							else {
								send_json["status"] = string("error:dev_eui");
								send_message_to_py_app(&send_json);
							}
						}
						else {
							send_json["status"] = string("error:timeout");
							send_message_to_py_app(&send_json);
						}
					}
					else if (type_msg == string("delete_device")) {
						if (recv_json.find("dev_addr") != recv_json.end()) {
							unsigned long device_address = recv_json["dev_addr"].get<long>();
							send_json["dev_addr"] = device_address;

							task_post_dynamic_msg(GW_TASK_AZURE_ID, GW_TASK_SERVER_ID, SERVER_REMOVE_DEVICE, \
												  (uint8_t*)&device_address, sizeof(device_address));
							//APP_DBG("Remove device\n");

							send_json["status"] = string("success");
							send_message_to_py_app(&send_json);
						}
						else {
							send_json["status"] = string("error:dev_addr");
							send_message_to_py_app(&send_json);
						}
					}
					else if (type_msg == string("device_control")) {
						if (recv_json.find("dev_addr") != recv_json.end()) {
							json control_values;
							uint8_t relay_value = 0;
							uint8_t relay_mask = 0;
							unsigned long device_address = 0;
							bool is_sent = false;

							device_address = recv_json["dev_addr"].get<long>();
							send_json["dev_addr"] = device_address;

							if (recv_json.find("control_values") != recv_json.end()) {
								control_values = recv_json["control_values"];
								if (control_values.is_array() && control_values.size() > 0) {

									for (uint index = 0; index < control_values.size(); index++) {

										if (control_values[index].find("output") != control_values[index].end()) {
											string output_name = control_values[index]["output"].get<std::string>();
											if (output_name.compare("relay") == 0) {

												if (control_values[index].find("index") != control_values[index].end()) {
													if (control_values[index].find("value") != control_values[index].end()) {

														int relay_index = control_values[index]["index"].get<int>();
														if (relay_index == 0) {
															relay_mask = 0xFF;
															relay_value = control_values[index]["value"].get<int>();

															serv_confirmed_msg_t confirmed_msg;
															confirmed_msg.header.device_address = (uint32_t)device_address;
															confirmed_msg.header.retries = 5;
															confirmed_msg.header.port = LORAWAN_PORT_CONTROL;
															confirmed_msg.header.data_len = sizeof(lorawan_header_msg_t) + sizeof(shimizu_control_b_t);

															lorawan_msg_t* send_msg = (lorawan_msg_t*)confirmed_msg.data;
															send_msg->header.device_type = LORAWAN_DEVICE_TYPE_SHIMIZU;
															send_msg->header.function_type = LORAWAN_FUNCTION_TYPE_CONTROL_B_CONTROL;
															send_msg->header.data_type = LORAWAN_DATA_TYPE_UINT8;
															send_msg->header.data_length = sizeof(shimizu_control_b_t);

															shimizu_control_b_t* ctrl_data = (shimizu_control_b_t*)send_msg->data;
															ctrl_data->mask = (uint8_t)relay_mask;
															ctrl_data->output = (uint8_t)relay_value;

															APP_DBG("ctrl_data->mask: %d\n", ctrl_data->mask);
															APP_DBG("ctrl_data->output: %d\n", ctrl_data->output);

															task_post_dynamic_msg(GW_TASK_AZURE_ID, GW_TASK_SERVER_ID, SERVER_SEND_CONFIRM_MSG, \
																				  (uint8_t*)&confirmed_msg, sizeof(confirmed_msg.header) + confirmed_msg.header.data_len);

															is_sent = true;

															send_json["status"] = string("success");
															send_message_to_py_app(&send_json);

															break;
														}
														else {
															relay_index = relay_index -1;
															relay_mask = relay_mask | (1 << relay_index);

															if (control_values[index]["value"].get<int>()) {
																relay_value = relay_value | (1 << relay_index);
															}
															else {
																relay_value = relay_value & (~(1 << relay_index));
															}
														}
													}
													else {
														send_json["status"] = string("error:value");
														send_message_to_py_app(&send_json);
													}
												}
												else {
													send_json["status"] = string("error:index");
													send_message_to_py_app(&send_json);
												}
											}
										}
										else {
											send_json["status"] = string("error:output");
											send_message_to_py_app(&send_json);
										}
									}

									if (is_sent == false) {
										serv_confirmed_msg_t confirmed_msg;
										confirmed_msg.header.device_address = (uint32_t)device_address;
										confirmed_msg.header.retries = 5;
										confirmed_msg.header.port = LORAWAN_PORT_CONTROL;
										confirmed_msg.header.data_len = sizeof(lorawan_header_msg_t) + sizeof(shimizu_control_b_t);

										lorawan_msg_t* send_msg = (lorawan_msg_t*)confirmed_msg.data;
										send_msg->header.device_type = LORAWAN_DEVICE_TYPE_SHIMIZU;
										send_msg->header.function_type = LORAWAN_FUNCTION_TYPE_CONTROL_B_CONTROL;
										send_msg->header.data_type = LORAWAN_DATA_TYPE_UINT8;
										send_msg->header.data_length = sizeof(shimizu_control_b_t);

										shimizu_control_b_t* ctrl_data = (shimizu_control_b_t*)send_msg->data;
										ctrl_data->mask = (uint8_t)relay_mask;
										ctrl_data->output = (uint8_t)relay_value;

										APP_DBG("ctrl_data->mask: %d\n", ctrl_data->mask);
										APP_DBG("ctrl_data->output: %d\n", ctrl_data->output);

										task_post_dynamic_msg(GW_TASK_AZURE_ID, GW_TASK_SERVER_ID, SERVER_SEND_CONFIRM_MSG, \
															  (uint8_t*)&confirmed_msg, sizeof(confirmed_msg.header) + confirmed_msg.header.data_len);


										send_json["status"] = string("success");
										send_message_to_py_app(&send_json);
									}
								}
								else {
									send_json["status"] = string("error:control_values");
									send_message_to_py_app(&send_json);
								}
							}
							else {
								send_json["status"] = string("error:control_values");
								send_message_to_py_app(&send_json);
							}
						}
						else {
							send_json["status"] = string("error:dev_addr");
							send_message_to_py_app(&send_json);
						}
					}
					else if (type_msg == string("device_setting")) {
						//TODO
					}
					else if (type_msg == string("gateway_status")) {
						send_json["status"] = string("online");


						send_message_to_py_app(&send_json);
					}
					else if (type_msg == string("device_getting")) {
						if (recv_json.find("dev_addr") != recv_json.end()) {
							unsigned long device_address = recv_json["dev_addr"].get<long>();

							send_json["dev_addr"] = device_address;

							serv_confirmed_msg_t confirmed_msg;
							confirmed_msg.header.device_address = (uint32_t)device_address;
							confirmed_msg.header.retries = 5;
							confirmed_msg.header.port = LORAWAN_PORT_GETTING;
							confirmed_msg.header.data_len = sizeof(lorawan_header_msg_t) + 0;

							lorawan_msg_t* send_msg = (lorawan_msg_t*)confirmed_msg.data;
							send_msg->header.device_type = LORAWAN_DEVICE_TYPE_SHIMIZU;
							send_msg->header.function_type = LORAWAN_FUNCTION_TYPE_CONTROL_B_GETTING;
							send_msg->header.data_length = 0;

							task_post_dynamic_msg(GW_TASK_AZURE_ID, GW_TASK_SERVER_ID, SERVER_SEND_CONFIRM_MSG, \
												  (uint8_t*)&confirmed_msg, sizeof(confirmed_msg.header) + confirmed_msg.header.data_len);

							send_json["status"] = string("success");
							send_message_to_py_app(&send_json);


						}
						else {
							send_json["status"] = string("error:dev_addr");
							send_message_to_py_app(&send_json);
						}
					}
					else { //Default
						send_json["status"] = string("error:type_msg");
						send_message_to_py_app(&send_json);
					}
				}
				else {
					send_json["status"] = string("error:json");
					send_message_to_py_app(&send_json);
				}

			}
			catch ( exception e){
				(void)e;
				json err_send;
				err_send["gateway_id"] = string(gateway_configure_parameter.azure_gateway.device_id);
				err_send["status"] = string("error:parser");
				send_message_to_py_app(&send_json);

				break;
			}
		}

		if (num_read == -1) {
			APP_DBG("[ERR] read\n");
		}

		if (close(client_fd) == -1) {
			APP_DBG("[ERR] close\n");
		}
	}
}


int send_message_to_py_app(json* send_json) {
	struct sockaddr_un addr;
	int sfd;
	string dump = send_json->dump();
	uint length = dump.length();
	const char* buff = dump.c_str();

	APP_DBG_MSG(buff, length);

	sfd = socket(AF_UNIX, SOCK_STREAM, 0);
	if (sfd < 0) {
		APP_DBG("[ERR] send_to_py_app socket\n");
		return -1;
	}

	memset(&addr, 0, sizeof(struct sockaddr_un));
	addr.sun_family = AF_UNIX;

	strncpy(addr.sun_path, py_socket, sizeof(addr.sun_path) - 1);
	if (connect(sfd, (struct sockaddr *) &addr, sizeof(struct sockaddr_un)) == -1) {
		APP_DBG("[ERR] send_to_py_app connect\n");
		return -1;
	}

	if (write(sfd, buff, length) != (ssize_t)length) {
		APP_DBG("[ERR] send_to_py_app write\n");
		return -1;
	}

	close(sfd);

	return 1;
}

static int exec_prog(const char **argv) {
	pid_t my_pid;
	int status, timeout /* unused ifdef WAIT_FOR_COMPLETION */;

	if (0 == (my_pid = fork())) {
		if (-1 == execve(argv[0], (char **)argv , NULL)) {
			perror("child process execve failed [%m]");
			return -1;
		}
	}

#ifdef WAIT_FOR_COMPLETION
	timeout = 1000;

	while (0 == waitpid(my_pid , &status , WNOHANG)) {
		if ( --timeout < 0 ) {
			perror("timeout");
			return -1;
		}
		sleep(1);
	}

	printf("%s WEXITSTATUS %d WIFEXITED %d [status %d]\n",
		   argv[0], WEXITSTATUS(status), WIFEXITED(status), status);

	if (1 != WIFEXITED(status) || 0 != WEXITSTATUS(status)) {
		perror("%s failed, halt system");
		return -1;
	}
#else
	(void) status;
	(void) timeout;
#endif
	return 0;
}
