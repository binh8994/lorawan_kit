#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/socket.h>
#include <sys/stat.h>

#include <time.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <fcntl.h>

#include "ak.h"
#include "timer.h"

#include "cmd_line.h"

#include "app.h"
#include "app_if.h"
#include "app_dbg.h"
#include "app_config.h"
#include "shell.h"

#include "link.h"
#include "link_sig.h"

#include "task_list.h"
#include "task_list_if.h"

static int32_t shell_ver(uint8_t* argv);
static int32_t shell_help(uint8_t* argv);
static int32_t shell_dbg(uint8_t* argv);
static int32_t shell_cfg(uint8_t* argv);

cmd_line_t lgn_cmd_table[] = {
	{(const int8_t*)"ver",		shell_ver,		(const int8_t*)"get kernel version"},
	{(const int8_t*)"help",		shell_help,		(const int8_t*)"help command info"},
	{(const int8_t*)"dbg",		shell_dbg,		(const int8_t*)"debug"},
	{(const int8_t*)"cfg",		shell_cfg,		(const int8_t*)"gateway cfg"},

	/* End Of Table */
	{(const int8_t*)0,(pf_cmd_func)0,(const int8_t*)0}
};

int32_t shell_ver(uint8_t* argv) {
	(void)argv;
	APP_PRINT("version: %s\n", AK_VERSION);
	return 0;
}

int32_t shell_help(uint8_t* argv) {
	uint32_t idx = 0;
	switch (*(argv + 4)) {
	default:
		APP_PRINT("\nCOMMANDS INFORMATION:\n\n");
		while(lgn_cmd_table[idx].cmd != (const int8_t*)0) {
			APP_PRINT("%s\n\t%s\n\n", lgn_cmd_table[idx].cmd, lgn_cmd_table[idx].info);
			idx++;
		}
		break;
	}
	return 0;
}

int32_t shell_dbg(uint8_t* argv) {
	switch (*(argv + 4)) {
	case 's': {
		uint64_t device_address = 0;
		char* ch = strtok((char*)(argv + 6), " ");
		std::istringstream(ch) >> std::hex >> device_address;
		json send_json;
		json ph_json, do_json, orp_json, ec_json, tds_json, sal_json, tur_json, temp_json, bat_json;
		time_t tt = time(nullptr);
		struct tm* ptm = localtime(&tt);
		char buf[30];
		strftime(buf, 30, "%Y-%m-%d %H:%M:%S", ptm);

		send_json["type_msg"] = string("device_report");
		send_json["gateway_id"] = string(gateway_configure_parameter.azure_gateway.device_id);
		send_json["DevAddr"] = device_address;
		send_json["RecordedTime"] = string(buf);

		ph_json["s"] = string("PH");
		ph_json["v"] = 0;
		do_json["s"] = string("DO");
		do_json["v"] = 0;
		orp_json["s"] = string("ORP");
		orp_json["v"] = 0;
		ec_json["s"] = string("EC");
		ec_json["v"] = 0;
		tds_json["s"] = string("TDS");
		tds_json["v"] = 0;
		sal_json["s"] = string("SAL");
		sal_json["v"] = 0;
		tur_json["s"] = string("TURB");
		tur_json["v"] = 0;
		temp_json["s"] = string("TEMP");
		temp_json["v"] = 0;
		bat_json["s"] = string("BAT");
		bat_json["v"] = 100;

		send_json["SensorValues"] = { ph_json, do_json, orp_json, ec_json, tds_json, sal_json, tur_json, temp_json, bat_json};
		//string dump = send_json.dump();
		//task_post_dynamic_msg(GW_TASK_CONSOLE_ID, GW_TASK_PYTHON_SOCKET_ID, PY_SOCKET_SEND_TEST, (uint8_t*)dump.data(), dump.length());

		APP_PRINT("Send to py socket\n");
	}
		break;

	case 'i': {
		task_post_pure_msg(GW_TASK_SERVER_ID, SERVER_INIT);
	}
		break;

	case 'j': {
		char* devEui = (char*)(argv + 6);
		char* appEui = (char*)(argv + 23);
		int values[8];
		serv_join_permit_t join_permit_msg;

		if ( 8 == sscanf( devEui,
						  "%02x%02x%02x%02x%02x%02x%02x%02x%*c",
						  &values[0], &values[1], &values[2], &values[3], &values[4], &values[5], &values[6], &values[7] ) ) {
			/* convert to uint8_t */
			for( int i = 0; i < 8; ++i )	join_permit_msg.devive_Eui[i] = (uint8_t) values[i];
			APP_PRINT("DevEui:");
			APP_DBG_HEX(join_permit_msg.devive_Eui, 8);
		}
		else {
			APP_PRINT("DevEui error\n");
			break;
		}

		if ( 8 == sscanf( appEui,
						  "%02x%02x%02x%02x%02x%02x%02x%02x%*c",
						  &values[0], &values[1], &values[2], &values[3], &values[4], &values[5], &values[6], &values[7] ) ) {
			/* convert to uint8_t */
			for( int i = 0; i < 8; ++i )	join_permit_msg.application_Eui[i] = (uint8_t) values[i];
			APP_PRINT("AppEui:");
			APP_DBG_HEX(join_permit_msg.application_Eui, 8);
		}
		else {
			APP_PRINT("AppEui error\n");
			break;
		}

		join_permit_msg.timeout = 60;
		task_post_dynamic_msg(GW_TASK_CONSOLE_ID, GW_TASK_SERVER_ID, SERVER_PERMIT_JOIN, (uint8_t*)&join_permit_msg, sizeof(serv_join_permit_t));

		APP_PRINT("Send join enable\n");
	}
		break;

	case 'u': {
		uint64_t device_address = 0;
		std::istringstream((char*)(argv + 6)) >> std::hex >> device_address;

		serv_unconfirmed_msg_t unconfirmed_msg;
		unconfirmed_msg.header.device_address = (uint32_t)device_address;
		unconfirmed_msg.header.port = LORAWAN_PORT_CONTROL;
		unconfirmed_msg.header.data_len = sizeof(lorawan_header_msg_t) + sizeof(shimizu_control_b_t);

		lorawan_msg_t* send_msg = (lorawan_msg_t*)unconfirmed_msg.data;
		send_msg->header.device_type = LORAWAN_DEVICE_TYPE_SHIMIZU;
		send_msg->header.function_type = LORAWAN_FUNCTION_TYPE_CONTROL_B_CONTROL;
		send_msg->header.data_type = LORAWAN_DATA_TYPE_UINT8;
		send_msg->header.data_length = sizeof(shimizu_control_b_t);

		shimizu_control_b_t* ctrl_data = (shimizu_control_b_t*)send_msg->data;
		ctrl_data->mask = (uint8_t)0XFF;
		ctrl_data->output = (uint8_t)0xFF;

		APP_DBG("ctrl_data->mask: %d\n", ctrl_data->mask);
		APP_DBG("ctrl_data->output: %d\n", ctrl_data->output);

		task_post_dynamic_msg(GW_TASK_CONSOLE_ID, GW_TASK_SERVER_ID, SERVER_SEND_UNCONFIRM_MSG, \
							  (uint8_t*)&unconfirmed_msg, sizeof(unconfirmed_msg.header) + unconfirmed_msg.header.data_len);

		APP_PRINT("Send unconfirm msg: %ul\n", (uint32_t)device_address);
	}
		break;

	case 'c': {
		uint64_t device_address = 0;
		std::istringstream((char*)(argv + 6)) >> std::hex >> device_address;

		serv_confirmed_msg_t confirmed_msg;
		confirmed_msg.header.device_address = (uint32_t)device_address;
		confirmed_msg.header.retries = 5;
		confirmed_msg.header.port = LORAWAN_PORT_CONTROL;
		confirmed_msg.header.data_len = sizeof(lorawan_header_msg_t) + sizeof(shimizu_control_b_t);

		lorawan_msg_t* send_msg = (lorawan_msg_t*)confirmed_msg.data;
		send_msg->header.device_type = LORAWAN_DEVICE_TYPE_SHIMIZU;
		send_msg->header.function_type = LORAWAN_FUNCTION_TYPE_CONTROL_B_CONTROL;
		send_msg->header.data_type = LORAWAN_DATA_TYPE_UINT8;
		send_msg->header.data_length = sizeof(shimizu_control_b_t);

		shimizu_control_b_t* ctrl_data = (shimizu_control_b_t*)send_msg->data;
		ctrl_data->mask = (uint8_t)0xFF;
		ctrl_data->output = (uint8_t)0xFF;

		APP_DBG("ctrl_data->mask: %d\n", ctrl_data->mask);
		APP_DBG("ctrl_data->output: %d\n", ctrl_data->output);

		task_post_dynamic_msg(GW_TASK_CONSOLE_ID, GW_TASK_SERVER_ID, SERVER_SEND_CONFIRM_MSG, \
							  (uint8_t*)&confirmed_msg, sizeof(confirmed_msg.header) + confirmed_msg.header.data_len);

		APP_PRINT("Send confirm msg: %ul\n", (uint32_t)device_address);

		break;
	}

	case 'r': {
		uint64_t device_address = 0;
		std::istringstream((char*)(argv + 6)) >> std::hex >> device_address;

		task_post_dynamic_msg(GW_TASK_CONSOLE_ID, GW_TASK_SERVER_ID, SERVER_REMOVE_DEVICE, \
							  (uint8_t*)&device_address, sizeof(device_address));
		APP_PRINT("Delete device: %ul\n", (uint32_t)device_address);
	}
		break;

	case 'a': {
		task_post_pure_msg(GW_TASK_SERVER_ID, SERVER_REMOVE_DEVICE_ALL);
		APP_PRINT("Delete device all\n");
	}
		break;

	default:
		APP_PRINT("dbg <..>\n");
		APP_PRINT(" s\tsend to py sock\n");
		APP_PRINT(" i\tinit server mode\n");
		APP_PRINT(" j\t<DevEui:16> <AppEui:16> join enable\n");
		APP_PRINT(" u\t<DevAddr:8> send unconfirm msg\n");
		APP_PRINT(" c\t<DevAddr:8> send confirm msg\n");
		APP_PRINT(" r\t<DevAddr:8> delete device\n");
		APP_PRINT(" a\tdelete device all\n");
		break;
	}
	return 0;
}


int32_t shell_cfg(uint8_t* argv) {
	switch (*(argv + 4)) {
	case 'c': {
		char* input_string = (char*)(argv + 6);
		if (*(argv + 6) != 0) {
			input_string[strlen(input_string) - 1] = 0;

			string conn_string(input_string);
			int h_index = conn_string.find("HostName=") + strlen("HostName=");
			int d_index = conn_string.find("DeviceId=") + strlen("DeviceId=");
			int s_index = conn_string.find("SharedAccessKey=") + strlen("SharedAccessKey=");
			int h_len = d_index - 1 - h_index - strlen("DeviceId=");
			int d_len = s_index - 1 - d_index - strlen("SharedAccessKey=");

			string sHost_name			= conn_string.substr(h_index, h_len);
			string sDevice_id			= conn_string.substr(d_index, d_len);
			string sAccess_share_key	= conn_string.substr(s_index);

			app_config_parameter_t config;
			/* azure gateway */
			strcpy(config.azure_gateway.host_name,			sHost_name.data());
			strcpy(config.azure_gateway.device_id,			sDevice_id.data());
			strcpy(config.azure_gateway.access_share_key,	sAccess_share_key.data());
			strcpy(config.azure_gateway.connection_string,	conn_string.data());

			gateway_configure.write_config_data(&config);
			gateway_configure.parser_config_file(&config);

			APP_PRINT("azure_gateway.host_name:%s\n"				,config.azure_gateway.host_name);
			APP_PRINT("azure_gateway.device_id:%s\n"				,config.azure_gateway.device_id);
			APP_PRINT("azure_gateway.access_share_key:%s\n"			,config.azure_gateway.access_share_key);
			APP_PRINT("azure_gateway.connection_string:%s\n"		,config.azure_gateway.connection_string);
		}
		else {
			APP_PRINT("Connection string not found.");
		}

	}
		break;

	default:
		APP_PRINT("cfg <..>\n");
		APP_PRINT(" c <conn_string>\tset azure connection string\n");
		break;
	}

	return 0;
}
