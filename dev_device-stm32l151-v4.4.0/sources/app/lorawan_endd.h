#ifndef __LORAWAN_ENDD_H__
#define __LORAWAN_ENDD_H__

#include "app_eeprom.h"
#include "LoRaMac.h"

#define ENDD_BUFFER_SIZE		(254)

typedef struct {
	struct {
		uint8_t port;
	} __AK_PACKETED header;
	uint8_t buffer[ENDD_BUFFER_SIZE];
} endd_send_unconfirm_t;

typedef struct {
	struct {
		uint8_t port;
		uint8_t num_retries;
	} __AK_PACKETED header;
	uint8_t buffer[ENDD_BUFFER_SIZE];
} endd_send_confirm_t;

typedef struct {
	struct {
		uint8_t port;
		uint8_t rx_slot;
		int16_t rssi;
		uint8_t snr;
	} __AK_PACKETED header;
	uint8_t buffer[ENDD_BUFFER_SIZE];
} endd_recv_common_t;

typedef enum {
	LORAWAN_ERROR,
	LORAWAN_REQUEST_OK,
	LORAWAN_LENGTH_ERROR,
	LORAWAN_JOIN_ERROR,
	LORAWAN_JOIN_FROM_SETTING,
} endd_status_t;

extern endd_setting_t Endd_setting;
extern MibRequestConfirm_t MibReq;

extern void endd_init();
extern bool endd_set_active_mode(uint8_t mode);

extern bool endd_set_chanmask(const uint16_t* chanmask);
extern uint16_t* endd_get_chanmask();

extern bool endd_set_rx2_freq(const uint32_t* freq);
extern uint32_t* endd_get_rx2_freq();

extern bool endd_set_rx2_dr(const uint8_t* dr);
extern uint8_t* endd_get_rx2_dr();

extern bool endd_send_confirm_msg(uint8_t port, uint8_t retries, uint8_t* payload, uint8_t payload_len);
extern bool endd_send_unconfirm_msg(uint8_t port, uint8_t* payload, uint8_t payload_len);

#endif // __LORAWAN_ENDD_H__
