#include <string.h>

#include "app_eeprom.h"
#include "app_dbg.h"
#include "sys_ctrl.h"
#include "sys_dbg.h"

#include "eeprom.h"
#include "radio.h"
#include "LoRaMac.h"
#include "LoRaMacCrypto.h"

#include "task_list.h"
#include "lorawan_serv.h"

#define GW_FREQ						433175000
#define GW_BANDWIDTH				0
// 0: 125 kHz
// 1: 250 kHz
// 2: 500 kHz
// 3: Reserved
#define GW_SF						12
// [SF7..SF12]
#define GW_DR						0	//DR_0
#define GW_CD						1
// 1: 4/5
// 2: 4/6
// 3: 4/7
// 4: 4/8
#define GW_TX_POWER					20	//dBm
#define GW_PREAMBLE_LENGTH			8
#define GW_SYMBOL_TIMEOUT			5
#define GW_FIX_LENGTH_PAY			false
#define GW_IQ_TX					true
#define GW_IQ_RX					false
#define GW_TX_TIMEOUT				3000
#define GW_RX_TIMEOUT				0

#define DEFAULT_KEY					{ 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10 }

#define STATIC_ADDR					0xFFFFFFFF
#define MAX_DEVICE					30
typedef struct {
	uint8_t device_Eui[8];
	uint8_t application_Eui[8];
	uint8_t appskey[16];
	uint8_t nwkskey[16];
	uint16_t device_nonce;
	uint32_t device_address;
	dlsettings_t dlsettting;
	delay_t delay;

	uint32_t down_link_cnt;
	uint32_t up_link_cnt;
	bool device_ack_req;
} __AK_PACKETED device_t;

struct device_list_t {
	uint8_t nb_device;
	device_t device[MAX_DEVICE];
} __AK_PACKETED list;

static serv_tx_config_t Serv_tx_config  = { GW_FREQ, GW_DR, GW_TX_POWER};
static serv_rx_config_t Serv_rx_config = { GW_FREQ, GW_DR};
static uint8_t SF[] = {12, 11, 10, 9, 8, 7};
static serv_init_param_t Serv_init_param = {0, DEFAULT_KEY};

static RadioEvents_t Lora_events;
static uint8_t Output_buffer[SERVER_PHY_MAXPAYLOAD];
static uint8_t Join_buffer[64];
static serv_unconfirmed_msg_t Unconfirmed;
static serv_confirmed_msg_t Confirmed;

static bool init_device_list();
static bool remove_device(uint32_t dev_addr);
static void remove_device_all();
static bool exits_device(uint32_t dev_addr);
static bool update_device(const device_t* device);
static device_t* get_device(uint32_t dev_addr);
static device_t* get_device(const uint8_t* dev_eui);

static void on_tx_done( void );
static void on_rx_done( uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr );
static void on_tx_timeout( void );
static void on_rx_error( void );
static void on_rx_timeout( void );

bool init_device_list() {
	eeprom_read(DEVICE_LIST_ADDR, (uint8_t*)&list, sizeof(list.nb_device));
	if (list.nb_device > MAX_DEVICE) {
		list.nb_device = MAX_DEVICE;
	}
	eeprom_read(DEVICE_LIST_ADDR, (uint8_t*)&list, sizeof(list.nb_device) + (list.nb_device * sizeof(device_t)));
	APP_DBG("list.nb_device: %d\n", list.nb_device);
	return true;
}

bool update_device(const device_t* device) {
	for (uint8_t i = 0; i < list.nb_device; i++) {
		if (list.device[i].device_address == device->device_address) {
			memcpy(&list.device[i], device, sizeof(device_t));
			eeprom_write(DEVICE_LIST_ADDR, (uint8_t*)&list, sizeof(list.nb_device) + list.nb_device * sizeof(device_t));
			APP_DBG("device exits - update\n");
			return true;
		}
	}
	memcpy(&list.device[list.nb_device++], device, sizeof(device_t));
	APP_DBG("device not found - add new\n");
	return true;
}

bool remove_device(uint32_t dev_addr) {
	for (uint8_t i = 0; i < list.nb_device; i++) {
		if (list.device[i].device_address == dev_addr) {
			memcpy(&list.device[i], &list.device[i+1], sizeof(device_t) * list.nb_device - i -1);
			list.nb_device--;
			eeprom_write(DEVICE_LIST_ADDR, (uint8_t*)&list, sizeof(list.nb_device) + list.nb_device * sizeof(device_t));
			APP_DBG("device exits - rm\n");
			return true;
		}
	}
	APP_DBG("device not found\n");
	return false;
}

void remove_device_all() {
	list.nb_device = 0;
	eeprom_write(DEVICE_LIST_ADDR, (uint8_t*)&list, sizeof(list.nb_device) + list.nb_device * sizeof(device_t));
}

bool exits_device(uint32_t dev_addr) {
	for (uint8_t i = 0; i < list.nb_device; i++) {
		if (list.device[i].device_address == dev_addr) {
			APP_DBG("device exits\n");
			return true;
		}
	}
	APP_DBG("device not found\n");
	return false;
}

device_t* get_device(uint32_t dev_addr) {
	for (uint8_t i = 0; i < list.nb_device; i++) {
		if (list.device[i].device_address == dev_addr) {
			APP_DBG("device exits\n");
			return &list.device[i];
		}
	}
	APP_DBG("device not found\n");
	return NULL;
}

device_t* get_device(const uint8_t *dev_eui) {
	for (uint8_t i = 0; i < list.nb_device; i++) {
		if (memcmp(list.device[i].device_Eui, dev_eui, 8) == 0) {
			APP_DBG("device exits\n");
			return &list.device[i];
		}
	}
	APP_DBG("device not found\n");
	return NULL;
}

static void on_tx_done( void ) {
	APP_DBG("on_tx_done\n");
	Radio.SetChannel(Serv_rx_config.rx_frequency);
	Radio.SetMaxPayloadLength( MODEM_LORA, SERVER_PHY_MAXPAYLOAD );
	Radio.SetRxConfig( MODEM_LORA, GW_BANDWIDTH, SF[Serv_rx_config.rx_datarater],
			GW_CD, 0, GW_PREAMBLE_LENGTH,
			GW_SYMBOL_TIMEOUT, GW_FIX_LENGTH_PAY,
			0, true, 0, 0, GW_IQ_RX, true );
	Radio.Rx( GW_RX_TIMEOUT );
}

static void on_rx_done( uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr ) {
	(void) rssi; (void) snr;

	APP_DBG("on_rx_done\n");
	APP_DBG("radio_payload: %d\n", size);
	APP_DBG_HEX(payload, size);

	LoRaMacHeader_t mac_header;
	uint8_t header_len = 0;

	Radio.Sleep( );

	mac_header.Value = payload[header_len++];

	switch( mac_header.Bits.MType ) {
	case FRAME_TYPE_JOIN_REQ: {
		APP_DBG("FRAME_TYPE_JOIN_REQ\n");

		join_param_t device_param;
		uint16_t device_nonce;
		uint32_t mic, mic_rx= 0;
		device_t* current_device;

		device_param.is_rejoin = false;

		memcpyr(device_param.application_Eui, payload + header_len, 8);
		APP_DBG("app_Eui:");
		APP_DBG_HEX(device_param.application_Eui, 8);

		header_len += 8;
		memcpyr(device_param.device_Eui, payload + header_len, 8);
		APP_DBG("dev_Eui:");
		APP_DBG_HEX(device_param.device_Eui, 8);

		header_len += 8;
		device_nonce = payload[header_len++];
		device_nonce |= ( (uint16_t)payload[header_len++] << 8 );
		APP_DBG("device_nonce: x%08X\n", device_nonce);

		device_param.device_nonce = device_nonce;

		current_device = get_device((const uint8_t*)device_param.device_Eui);
		if( current_device != NULL ) {
			if(current_device->device_nonce == device_param.device_nonce) {
				APP_DBG("request repeated\n");
				break;
			}
			else {
				APP_DBG("rejoin request\n");
				device_param.is_rejoin = true;

				//send for device previous device_addr but nwkskey and appskey change
				device_param.device_address = current_device->device_address;
				APP_DBG("previous device_address:x%08X\n", device_param.device_address);
			}
		}
		else {
			uint32_t device_address, count = 100;
			do {
				device_address = (uint32_t)rand() & 0xFFFFFFFF;
				if (!(count--)) {
					APP_DBG("cannot create device_address\n");
					return;
				}
			} while( exits_device(device_address) );

			device_param.device_address = device_address;
			APP_DBG("new device_address: x%08X\n", device_param.device_address);

		}
		mic_rx |= (   uint32_t )payload[size - LORAMAC_MFR_LEN];
		mic_rx |= ( ( uint32_t )payload[size - LORAMAC_MFR_LEN + 1] << 8 );
		mic_rx |= ( ( uint32_t )payload[size - LORAMAC_MFR_LEN + 2] << 16 );
		mic_rx |= ( ( uint32_t )payload[size - LORAMAC_MFR_LEN + 3] << 24 );
		APP_DBG("mic_rx: x%08X\n", mic_rx);

		LoRaMacJoinComputeMic( payload, size - LORAMAC_MFR_LEN, Serv_init_param.application_key, &mic );
		APP_DBG("mic_compute: x%08X\n", mic);

		if(mic == mic_rx) {
			task_post_common_msg(AC_TASK_SERVER_ID, SERVER_RECV_JOIN_MSG, (uint8_t*)&device_param, sizeof(join_param_t));
		}
		else {
			APP_DBG("mic_compute!=mic_rx\n");
			break;
		}
	}
		break;

	case FRAME_TYPE_DATA_CONFIRMED_UP: {
		APP_DBG("FRAME_TYPE_DATA_CONFIRMED_UP\n");
		goto HANDLE;
	}
		break;

	case FRAME_TYPE_DATA_UNCONFIRMED_UP: {
		APP_DBG("FRAME_TYPE_DATA_UNCONFIRMED_UP\n");
HANDLE:
		LoRaMacFrameCtrl_t frame_ctrl;
		uint8_t frame_len = 0, payload_start_index, port = 0xFF;
		uint16_t rx_counter = 0;
		uint32_t device_address = 0, mic, mic_rx = 0;
		device_t* current_device;

		device_address  =			payload[header_len++];
		device_address |= ((uint32_t)payload[header_len++] << 8 );
		device_address |= ((uint32_t)payload[header_len++] << 16 );
		device_address |= ((uint32_t)payload[header_len++] << 24 );
		APP_DBG("device_address:x%08X\n", device_address);

		if(!exits_device(device_address)) {
			APP_DBG("device not found\n");
			break;
		}
		else {
			current_device = get_device(device_address);
		}

		frame_ctrl.Value = payload[header_len++];
		rx_counter = ( uint16_t )payload[header_len++];
		rx_counter |= ( uint16_t )payload[header_len++] << 8;
		payload_start_index = 8 + frame_ctrl.Bits.FOptsLen;
		mic_rx |= (   uint32_t )payload[size - LORAMAC_MFR_LEN];
		mic_rx |= ( ( uint32_t )payload[size - LORAMAC_MFR_LEN + 1] << 8 );
		mic_rx |= ( ( uint32_t )payload[size - LORAMAC_MFR_LEN + 2] << 16 );
		mic_rx |= ( ( uint32_t )payload[size - LORAMAC_MFR_LEN + 3] << 24 );

		//APP_DBG("FOpts_len:%d\n", fCtrl.Bits.FOptsLen);
		APP_DBG("Bits.Ack: %d\n", frame_ctrl.Bits.Ack);
		APP_DBG("rx_counter:%d\n", rx_counter);
		APP_DBG("mic_rx: x%08X\n", mic_rx);

#if 0 //BINHNT61
		uint16_t uplink_counter_prev = 0;
		uplink_counter_prev = ( uint16_t )(current_device->up_link_cnt);
		//APP_DBG("uplink_counter_prev: %d\n", uplink_counter_prev);
		if( sequence_counter_diff < ( 1 << 15 ) ) {
			up_link_counter += sequence_counter_diff;
			LoRaMacComputeMic( payload, size - LORAMAC_MFR_LEN, nwkSKey, address, UP_LINK, up_link_counter, &mic_compute );
			//APP_DBG("mic_compute:x%08X\n", mic_compute);
			if( mic_rx == mic_compute ) {
				isMicOk = true;
				APP_DBG("mic_rx=mic_compute\n");
			}
		}
		else {
			// check for sequence roll-over
			uint32_t  up_link_counter_tmp = up_link_counter + 0x10000 + ( int16_t )sequence_counter_diff;
			LoRaMacComputeMic( payload, size - LORAMAC_MFR_LEN, \
							   appSKey, address, UP_LINK, \
							   up_link_counter_tmp, &mic_compute );
			//APP_DBG("roll-over, mic_compute:x%08X\n", mic_compute);
			if( mic_rx == mic_compute ) {
				isMicOk = true;
				//APP_DBG("mic_rx=mic_compute\n");
				up_link_counter = up_link_counter_tmp;
			}
		}
#else
		LoRaMacComputeMic( payload, size - LORAMAC_MFR_LEN, (uint8_t*)current_device->nwkskey, device_address, UP_LINK, rx_counter, &mic );
		APP_DBG("mic_compute: x%08X\n", mic);
#endif
		if( mic_rx == mic ) {
			if( ( current_device->up_link_cnt == rx_counter ) &&
					( current_device->up_link_cnt != 0 ) ) {
				APP_DBG("message repeated\n");
				break;
			}

			current_device->up_link_cnt = rx_counter;

			// Process payload and MAC commands
			if( ( ( size - 4 ) - payload_start_index ) > 0 ) {
				port = payload[payload_start_index++];
				frame_len = ( size - 4 ) - payload_start_index;
				APP_DBG("port: %d\n", port);
				APP_DBG("frame_len: %d\n", frame_len);

				if ( port == 0 ) {
#if 0
					// Only allow frames which do not have fOpts
					if( frame_ctrl.Bits.FOptsLen == 0 )	{

						if( mac_header.Bits.MType == FRAME_TYPE_DATA_CONFIRMED_UP ) {
							LoRaMacPayloadDecrypt( payload + payload_start_index, frame_len,
												   (uint8_t*)current_device->nwkskey, device_address,
												   UP_LINK, (current_device->up_link_cnt),
												   Confirmed.data );

							APP_DBG("Confirmed.data: ");
							APP_DBG_HEX(Confirmed.data, frame_len);

							current_device->device_ack_req = true;

							Confirmed.header.port = port;
							Confirmed.header.device_address = device_address;
							Confirmed.header.receive_ack = frame_ctrl.Bits.Ack;

							task_post_common_msg(AC_TASK_SERVER_ID, SERVER_RECV_CONFIRM_MSG, (uint8_t*)&Confirmed, frame_len + sizeof(Confirmed.header));

						}
						else {
							LoRaMacPayloadDecrypt( payload + payload_start_index, frame_len,
												   (uint8_t*)current_device->nwkskey, device_address,
												   UP_LINK, (current_device->up_link_cnt),
												   Unconfirmed.data );

							APP_DBG("Unconfirmed.data: ");
							APP_DBG_HEX(Unconfirmed.data, frame_len);

							current_device->device_ack_req = false;

							Unconfirmed.header.port = port;
							Unconfirmed.header.device_address = device_address;
							Unconfirmed.header.receive_ack = frame_ctrl.Bits.Ack;

							task_post_common_msg(AC_TASK_SERVER_ID, SERVER_RECV_UNCONFIRM_MSG, (uint8_t*)&Unconfirmed, frame_len + sizeof(Unconfirmed.header));

						}
					}
#endif
				}
				else {
					if( frame_ctrl.Bits.FOptsLen > 0 ) {
						// Decode Options field MAC commands. Omit the fPort.
					}

					if( mac_header.Bits.MType == FRAME_TYPE_DATA_CONFIRMED_UP ) {
						LoRaMacPayloadDecrypt( payload + payload_start_index, frame_len,
											   (uint8_t*)current_device->appskey, device_address,
											   UP_LINK, (current_device->up_link_cnt),
											   Confirmed.data );

						APP_DBG("Confirmed.data: ");
						APP_DBG_HEX(Confirmed.data, frame_len);

						current_device->device_ack_req = true;

						Confirmed.header.port = port;
						Confirmed.header.device_address = device_address;
						Confirmed.header.receive_ack = frame_ctrl.Bits.Ack;

						task_post_common_msg(AC_TASK_SERVER_ID, SERVER_RECV_CONFIRM_MSG, (uint8_t*)&Confirmed, frame_len + sizeof(Confirmed.header));

					}
					else {
						LoRaMacPayloadDecrypt( payload + payload_start_index, frame_len,
											   (uint8_t*)current_device->appskey, device_address,
											   UP_LINK, (current_device->up_link_cnt),
											   Unconfirmed.data );

						APP_DBG("Unconfirmed.data: ");
						APP_DBG_HEX(Unconfirmed.data, frame_len);

						current_device->device_ack_req = false;

						Unconfirmed.header.port = port;
						Unconfirmed.header.device_address = device_address;
						Unconfirmed.header.receive_ack = frame_ctrl.Bits.Ack;

						task_post_common_msg(AC_TASK_SERVER_ID, SERVER_RECV_UNCONFIRM_MSG, (uint8_t*)&Unconfirmed, frame_len + sizeof(Unconfirmed.header));
					}
				}
			}
			else {
#if 0
				if( frame_ctrl.Bits.FOptsLen > 0 ) {
					// Decode Options field MAC commands
					//process_mac_cmd( payload, 8, appPayloadStartIndex, snr );
					if( mac_header.Bits.MType == FRAME_TYPE_DATA_CONFIRMED_UP ) {
						current_device->device_ack_req = true;

						Confirmed.header.port = port;
						Confirmed.header.device_address = device_address;
						Confirmed.header.receive_ack = frame_ctrl.Bits.Ack;
						Confirmed.data[0] = 0;

						task_post_common_msg(AC_TASK_SERVER_ID, SERVER_RECV_CONFIRM_MSG, (uint8_t*)&Confirmed, 0 + sizeof(Confirmed.header));
					}
					else {
						current_device->device_ack_req = false;

						Unconfirmed.header.port = port;
						Unconfirmed.header.device_address = device_address;
						Unconfirmed.header.receive_ack = frame_ctrl.Bits.Ack;
						Unconfirmed.data[0] = 0;

						task_post_common_msg(AC_TASK_SERVER_ID, SERVER_RECV_UNCONFIRM_MSG, (uint8_t*)&Unconfirmed, 0 + sizeof(Unconfirmed.header));
					}
				}
#endif
			}
		}
		else {
			APP_DBG("mic_rx!=mic_compute\n");
			break;
		}
	}
		break;

	case FRAME_TYPE_PROPRIETARY: {
		//APP_DBG("FRAME_TYPE_PROPRIETARY\n");
	}
		break;

	default:
		//APP_DBG("FRAME_TYPE_UNKNOW\n");
		break;
	}

	Radio.SetChannel(Serv_rx_config.rx_frequency);
	Radio.SetMaxPayloadLength( MODEM_LORA, SERVER_PHY_MAXPAYLOAD );
	Radio.SetRxConfig( MODEM_LORA, GW_BANDWIDTH, SF[Serv_rx_config.rx_datarater],
			GW_CD, 0, GW_PREAMBLE_LENGTH,
			GW_SYMBOL_TIMEOUT, GW_FIX_LENGTH_PAY,
			0, true, 0, 0, GW_IQ_RX, true );
	Radio.Rx( GW_RX_TIMEOUT );
	//pthread_mutex_unlock(&mac_lock);
}

static void on_tx_timeout( void ) {
	APP_DBG("on_tx_timeout()\n");
	Radio.SetChannel(Serv_rx_config.rx_frequency);
	Radio.SetMaxPayloadLength( MODEM_LORA, SERVER_PHY_MAXPAYLOAD );
	Radio.SetRxConfig( MODEM_LORA, GW_BANDWIDTH, SF[Serv_rx_config.rx_datarater],
			GW_CD, 0, GW_PREAMBLE_LENGTH,
			GW_SYMBOL_TIMEOUT, GW_FIX_LENGTH_PAY,
			0, true, 0, 0, GW_IQ_RX, true );
	Radio.Rx( GW_RX_TIMEOUT );
}

static void on_rx_error( void ) {
	APP_DBG("on_rx_error()\n");
	//pthread_mutex_lock(&status_lock);
	//pthread_mutex_unlock(&status_lock);
	Radio.SetChannel(Serv_rx_config.rx_frequency);
	Radio.SetMaxPayloadLength( MODEM_LORA, SERVER_PHY_MAXPAYLOAD );
	Radio.SetRxConfig( MODEM_LORA, GW_BANDWIDTH, SF[Serv_rx_config.rx_datarater],
			GW_CD, 0, GW_PREAMBLE_LENGTH,
			GW_SYMBOL_TIMEOUT, GW_FIX_LENGTH_PAY,
			0, true, 0, 0, GW_IQ_RX, true );
	Radio.Rx( GW_RX_TIMEOUT );
}

static void on_rx_timeout( void ) {
	APP_DBG("on_rx_timeout()\n");
	FATAL("LORA", 0xDD);

	Radio.SetChannel(Serv_rx_config.rx_frequency);
	Radio.SetMaxPayloadLength( MODEM_LORA, SERVER_PHY_MAXPAYLOAD );
	Radio.SetRxConfig( MODEM_LORA, GW_BANDWIDTH, SF[Serv_rx_config.rx_datarater],
			GW_CD, 0, GW_PREAMBLE_LENGTH,
			GW_SYMBOL_TIMEOUT, GW_FIX_LENGTH_PAY,
			0, true, 0, 0, GW_IQ_RX, true );
	Radio.Rx( GW_RX_TIMEOUT );
}

void serv_set_tx_config(const serv_tx_config_t* tx_config) {
	memcpy(&Serv_tx_config, tx_config, sizeof(serv_tx_config_t));
}

void serv_set_rx_config(const serv_rx_config_t* rx_config) {
	memcpy(&Serv_rx_config, rx_config, sizeof(serv_rx_config_t));
}

serv_tx_config_t* serv_get_tx_config() {
	return &Serv_tx_config;
}

serv_rx_config_t* serv_get_rx_config() {
	return &Serv_rx_config;
}

void serv_init() {
	Radio.Sleep( );
	init_device_list();
}

void serv_set_net_param( const serv_init_param_t* init) {
	APP_DBG("serv_set_init_param\n");

	uint8_t Nwkskey[] = DEFAULT_KEY;
	uint8_t Appskey[] = DEFAULT_KEY;

	init_device_list();

	/*add device static, without join process*/
	dlsettings_t dlsetting;
	dlsetting.value = 0;
	dlsetting.bits.RFU = 0;
	dlsetting.bits.RX1DRoffset = 0;
	dlsetting.bits.RX2DR = GW_DR;

	delay_t delay;
	delay.value = 0;
	delay.bits.RFU = 0;
	delay.bits.Del = 0; //0=>1s, Delay 1s btw Tx & Rx1, Rx2=Rx1+1s

	device_t device;
	memset(&device, 0,sizeof(device_t));
	if( !exits_device(STATIC_ADDR) ) {
		memcpy(device.appskey, Appskey, 16);
		memcpy(device.nwkskey, Nwkskey, 16);
		device.device_address = STATIC_ADDR;
		device.dlsettting = dlsetting;
		device.delay = delay;
		update_device(&device);
	}

	memcpy(&Serv_init_param, init, sizeof(serv_init_param_t));

	// Initialize Radio driver
	srand(Radio.Random());
	Lora_events.TxDone = on_tx_done;
	Lora_events.RxDone = on_rx_done;
	Lora_events.RxError = on_rx_error;
	Lora_events.TxTimeout = on_tx_timeout;
	Lora_events.RxTimeout = on_rx_timeout;
	Radio.Init( &Lora_events );

	Radio.SetChannel( GW_FREQ );
	Radio.SetPublicNetwork(true);
	Radio.SetMaxPayloadLength( MODEM_LORA, SERVER_PHY_MAXPAYLOAD );

	Radio.SetTxConfig( MODEM_LORA, Serv_tx_config.tx_power, 0, \
					   GW_BANDWIDTH, SF[Serv_tx_config.tx_datarater], \
			GW_CD, GW_PREAMBLE_LENGTH, \
			GW_FIX_LENGTH_PAY, true, 0, 0, \
			GW_IQ_TX, GW_TX_TIMEOUT );

	Radio.SetRxConfig( MODEM_LORA, GW_BANDWIDTH, SF[Serv_rx_config.rx_datarater],
			GW_CD, 0, GW_PREAMBLE_LENGTH,
			GW_SYMBOL_TIMEOUT, GW_FIX_LENGTH_PAY,
			0, true, 0, 0, GW_IQ_RX, true );

	Radio.Rx(GW_RX_TIMEOUT);
}

serv_init_param_t* serv_get_net_param() {
	return &Serv_init_param;
}

void serv_remove_device(uint32_t dev_addr) {
	remove_device(dev_addr);
}

void serv_remove_device_all() {
	remove_device_all();
}

void serv_send_join_msg(const join_param_t *join) {
	APP_DBG("serv_send_join\n");
	LoRaMacHeader_t mac_header;
	uint8_t header_len;
	uint16_t pkt_len;
	uint32_t app_nonce, mic;
	device_t current_device;

	pkt_len = 0;
	header_len = 0;
	app_nonce = (uint32_t)rand() & 0x00FFFFFF;//3bytes

	dlsettings_t dlsetting;
	dlsetting.value = 0;
	dlsetting.bits.RFU = 0;
	dlsetting.bits.RX1DRoffset = 0;
	dlsetting.bits.RX2DR = GW_DR;

	delay_t delay;
	delay.value = 0;
	delay.bits.RFU = 0;
	delay.bits.Del = 0; //0=>1s, Delay 1s btw Tx & Rx1, Rx2=Rx1+1s

	//device_addr is before if device_Eui exits
	APP_DBG("app_nonce: x%08X\n", app_nonce);
	APP_DBG("device_address: x%08X\n", (join->device_address));

	mac_header.Value = 0;
	mac_header.Bits.MType = FRAME_TYPE_JOIN_ACCEPT;
	Join_buffer[header_len++] = mac_header.Value;
	Join_buffer[header_len++] = ( app_nonce ) & 0xFF;
	Join_buffer[header_len++] = ( app_nonce >> 8 ) & 0xFF;
	Join_buffer[header_len++] = ( app_nonce >> 16 ) & 0xFF;
	Join_buffer[header_len++] = ( Serv_init_param.network_id ) & 0xFF;
	Join_buffer[header_len++] = ( Serv_init_param.network_id >> 8 ) & 0xFF;
	Join_buffer[header_len++] = ( Serv_init_param.network_id >> 16 ) & 0xFF;
	Join_buffer[header_len++] = ( (join->device_address) ) & 0xFF;
	Join_buffer[header_len++] = ( (join->device_address) >> 8 ) & 0xFF;
	Join_buffer[header_len++] = ( (join->device_address) >> 16 ) & 0xFF;
	Join_buffer[header_len++] = ( (join->device_address) >> 24 ) & 0xFF;
	Join_buffer[header_len++] = dlsetting.value;
	Join_buffer[header_len++] = delay.value;

	pkt_len = header_len;

	LoRaMacJoinComputeMic( Join_buffer, pkt_len, Serv_init_param.application_key, &mic );

	APP_DBG("mic: x%08X\n", mic);

	Join_buffer[pkt_len + 0] =   mic & 0xFF;
	Join_buffer[pkt_len + 1] = ( mic >> 8 ) & 0xFF;
	Join_buffer[pkt_len + 2] = ( mic >> 16 ) & 0xFF;
	Join_buffer[pkt_len + 3] = ( mic >> 24 ) & 0xFF;

	pkt_len += LORAMAC_MFR_LEN;

	LoRaMacJoinEncrypt(Join_buffer + 1, pkt_len - 1, Serv_init_param.application_key, Output_buffer + 1);

#if 0
	//uint8_t key[24] = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16 };
	//uint8_t buffer_encrypt[24] = {0x55 ,0xF1 ,0x11 ,0x60 ,0xA2 ,0x39 ,0x3D ,0xEA ,0xF3 ,0xA2 ,0x92 ,0x1B ,0x99 ,0xAD ,0x4E ,0x08 ,0x10};
	uint8_t buffer_decrypt[20];

	LoRaMacJoinDecrypt(loramac_buffer + 1, loramac_buf_pkt_len - 1, appkey, buffer_decrypt);
	APP_DBG("buffer_decrypt:");
	for (int i=0; i< loramac_buf_pkt_len - 1; i++) APP_DBG("x%02X ", buffer_decrypt[i]);
	APP_DBG("\n");

#endif

	memset(&current_device, 0, sizeof(device_t));
	current_device.device_address = (join->device_address);
	current_device.device_nonce = (join->device_nonce);
	memcpy(current_device.device_Eui, (join->device_Eui), 8);
	memcpy(current_device.application_Eui, (join->application_Eui), 8);
	current_device.dlsettting = dlsetting;
	current_device.delay = delay;
	LoRaMacJoinComputeSKeys( Serv_init_param.application_key, Join_buffer + 1, (join->device_nonce), current_device.nwkskey, current_device.appskey );

	Output_buffer[0] = mac_header.Value;

	APP_DBG("appskey: ");
	APP_DBG_HEX(current_device.appskey, 16);
	APP_DBG("nwkskey: ");
	APP_DBG_HEX(current_device.nwkskey, 16);

	//Radio.SetMaxPayloadLength( MODEM_LORA, loramac_buf_pkt_len );
	Radio.SetTxConfig( MODEM_LORA, Serv_tx_config.tx_power, 0, \
					   GW_BANDWIDTH, SF[Serv_tx_config.tx_datarater], \
			GW_CD, GW_PREAMBLE_LENGTH, \
			GW_FIX_LENGTH_PAY, true, 0, 0, \
			GW_IQ_TX, 3000 );
	Radio.Send( Output_buffer, pkt_len );

	update_device(&current_device);

	APP_DBG("Output_buffer: ");
	APP_DBG_HEX(Output_buffer, pkt_len);
}

void serv_send_unconfirm_msg(const serv_unconfirmed_msg_t* unconfirmed, uint8_t msg_len) {
	APP_DBG("serv_send_unconfirm\n");
	LoRaMacHeader_t		mac_header;
	LoRaMacFrameCtrl_t	frame_ctrl;
	uint8_t				header_len = 0;
	uint16_t			pkt_len = 0;
	uint32_t			mic = 0;
	device_t* current_device;
	uint8_t data_len = msg_len - sizeof(unconfirmed->header);

	APP_DBG("device_address: x%08X\n", (unconfirmed->header.device_address));
	if((current_device = get_device(unconfirmed->header.device_address)) == NULL ) {
		return;
	}

	mac_header.Value			= 0;
	mac_header.Bits.MType		= FRAME_TYPE_DATA_UNCONFIRMED_DOWN;
	frame_ctrl.Value			= 0;
	frame_ctrl.Bits.FOptsLen	= 0;
	frame_ctrl.Bits.FPending	= 1;
	frame_ctrl.Bits.Ack			= current_device->device_ack_req;
	frame_ctrl.Bits.AdrAckReq	= 0;
	frame_ctrl.Bits.Adr			= 0;

	Output_buffer[header_len++] = mac_header.Value;
	Output_buffer[header_len++] = ( (unconfirmed->header.device_address) ) & 0xFF;
	Output_buffer[header_len++] = ( (unconfirmed->header.device_address) >> 8 ) & 0xFF;
	Output_buffer[header_len++] = ( (unconfirmed->header.device_address) >> 16 ) & 0xFF;
	Output_buffer[header_len++] = ( (unconfirmed->header.device_address) >> 24 ) & 0xFF;
	Output_buffer[header_len++] = frame_ctrl.Value;
	Output_buffer[header_len++] = (current_device->down_link_cnt) & 0xFF;
	Output_buffer[header_len++] = ( (current_device->down_link_cnt) >> 8 ) & 0xFF;
	APP_DBG("down_link_cnt: x%08X\n", current_device->down_link_cnt);

	if(data_len > 0 ) {
		Output_buffer[header_len++] = (unconfirmed->header.port);
		if( (unconfirmed->header.port) == 0 ) {
			LoRaMacPayloadEncrypt( (uint8_t* ) unconfirmed->data, (data_len), \
								   (current_device->nwkskey), (unconfirmed->header.device_address), DOWN_LINK, \
								   (current_device->down_link_cnt), &Output_buffer[header_len] );
		}
		else {
			LoRaMacPayloadEncrypt( (uint8_t* ) unconfirmed->data, (data_len), \
								   (current_device->appskey), (unconfirmed->header.device_address), DOWN_LINK, \
								   (current_device->down_link_cnt), &Output_buffer[header_len] );
		}
	}

	pkt_len = header_len + data_len;

	LoRaMacComputeMic( Output_buffer, pkt_len, (current_device->nwkskey), (unconfirmed->header.device_address), \
					   DOWN_LINK, (current_device->down_link_cnt), &mic );
	APP_DBG("mic: x%08X\n", mic);

	Output_buffer[pkt_len + 0] =   mic & 0xFF;
	Output_buffer[pkt_len + 1] = ( mic >> 8 ) & 0xFF;
	Output_buffer[pkt_len + 2] = ( mic >> 16 ) & 0xFF;
	Output_buffer[pkt_len + 3] = ( mic >> 24 ) & 0xFF;

	pkt_len += LORAMAC_MFR_LEN;

	//Radio.SetMaxPayloadLength( MODEM_LORA, loramac_buf_pkt_len );
	Radio.SetTxConfig( MODEM_LORA, Serv_tx_config.tx_power, 0, \
					   GW_BANDWIDTH, SF[Serv_tx_config.tx_datarater], \
			GW_CD, GW_PREAMBLE_LENGTH, \
			GW_FIX_LENGTH_PAY, true, 0, 0, \
			GW_IQ_TX, 3000 );
	Radio.Send( Output_buffer, pkt_len );

	APP_DBG("Output_buffer:\n");
	APP_DBG_HEX(Output_buffer, pkt_len);

	(current_device->device_ack_req) = false;
	(current_device->down_link_cnt)++;
}

void serv_send_confirm_msg(const serv_confirmed_msg_t* confirmed, uint8_t msg_len) {
	APP_DBG("serv_send_confirm\n");
	LoRaMacHeader_t		mac_header;
	LoRaMacFrameCtrl_t	frame_ctrl;
	uint32_t			mic = 0;
	uint8_t				header_len = 0;
	uint16_t			pkt_len = 0;
	uint8_t data_len = msg_len - sizeof(confirmed->header);

	APP_DBG("device_address: x%08X\n", (confirmed->header.device_address));
	device_t* current_device;
	if((current_device = get_device(confirmed->header.device_address)) == NULL ) {
		return;
	}

	mac_header.Value			= 0;
	mac_header.Bits.MType		= FRAME_TYPE_DATA_CONFIRMED_DOWN;
	frame_ctrl.Value			= 0;
	frame_ctrl.Bits.FOptsLen	= 0;
	frame_ctrl.Bits.FPending	= 1;
	frame_ctrl.Bits.Ack			= current_device->device_ack_req;
	frame_ctrl.Bits.AdrAckReq	= 0;
	frame_ctrl.Bits.Adr			= 0;

	Output_buffer[header_len++] = mac_header.Value;
	Output_buffer[header_len++] = ( (confirmed->header.device_address) ) & 0xFF;
	Output_buffer[header_len++] = ( (confirmed->header.device_address) >> 8 ) & 0xFF;
	Output_buffer[header_len++] = ( (confirmed->header.device_address) >> 16 ) & 0xFF;
	Output_buffer[header_len++] = ( (confirmed->header.device_address) >> 24 ) & 0xFF;
	Output_buffer[header_len++] = frame_ctrl.Value;
	Output_buffer[header_len++] = ( current_device->down_link_cnt) & 0xFF;
	Output_buffer[header_len++] = ( (current_device->down_link_cnt) >> 8 ) & 0xFF;
	APP_DBG("down_link_cnt: x%08X\n", current_device->down_link_cnt);

	if(data_len > 0 ) {
		Output_buffer[header_len++] = (confirmed->header.port);
		if( (confirmed->header.port) == 0 ) {
			LoRaMacPayloadEncrypt( (uint8_t* ) confirmed->data, (data_len), \
								   (current_device->nwkskey), (confirmed->header.device_address), DOWN_LINK, \
								   (current_device->down_link_cnt), &Output_buffer[header_len] );
		}
		else {
			LoRaMacPayloadEncrypt( (uint8_t* ) confirmed->data, (data_len), \
								   (current_device->appskey), (confirmed->header.device_address), DOWN_LINK, \
								   (current_device->down_link_cnt), &Output_buffer[header_len] );
		}
	}

	pkt_len = header_len + data_len;

	LoRaMacComputeMic( Output_buffer, pkt_len, (current_device->nwkskey), (confirmed->header.device_address), \
					   DOWN_LINK, (current_device->down_link_cnt), &mic );
	APP_DBG("mic: x%08X\n", mic);

	Output_buffer[pkt_len + 0] =   mic & 0xFF;
	Output_buffer[pkt_len + 1] = ( mic >> 8 ) & 0xFF;
	Output_buffer[pkt_len + 2] = ( mic >> 16 ) & 0xFF;
	Output_buffer[pkt_len + 3] = ( mic >> 24 ) & 0xFF;

	pkt_len += LORAMAC_MFR_LEN;

	//Radio.SetMaxPayloadLength( MODEM_LORA, loramac_buf_pkt_len );
	Radio.SetTxConfig( MODEM_LORA, Serv_tx_config.tx_power, 0, \
					   GW_BANDWIDTH, SF[Serv_tx_config.tx_datarater], \
			GW_CD, GW_PREAMBLE_LENGTH, \
			GW_FIX_LENGTH_PAY, true, 0, 0, \
			GW_IQ_TX, 3000 );
	Radio.Send( Output_buffer, pkt_len );

	APP_DBG("Output_buffer:\n");
	APP_DBG_HEX(Output_buffer, pkt_len);

	(current_device->device_ack_req) = false;
	(current_device->down_link_cnt)++;
}
