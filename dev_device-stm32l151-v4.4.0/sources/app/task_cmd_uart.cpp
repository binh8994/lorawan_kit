#include <stdbool.h>

#include "fsm.h"
#include "port.h"
#include "message.h"
#include "timer.h"

#include "utils.h"
#include "xprintf.h"

#include "sys_dbg.h"
#include "sys_ctrl.h"
#include "sys_irq.h"
#include "sys_svc.h"
#include "sys_io.h"

#include "app.h"
#include "app_dbg.h"
#include "task_list.h"
#include "task_cmd_uart.h"

#include "lorawan_endd.h"
#include "lorawan_fw.h"
#include "lorawan_serv.h"

#define CMD_SOP_CHAR					(0xEF)

#define SOP_STATE						(0x00)
#define LEN_STATE						(0x01)
#define DATA_STATE						(0x02)
#define FCS_STATE						(0x03)

#define CMD_UART_BUFFER_SIZE			(254)

enum {
	MODE_ENDDEVICE,
	MODE_FORWARD,
	MODE_SERVER,
	MODE_IDLE = 255
};

typedef struct {
	uint8_t des_task_id;
	uint8_t des_sig;
} cmd_uart_tbl_t;

static cmd_uart_tbl_t Cmd_uart_tbl[] = \
{
	{AC_TASK_CMD_UART_ID, CMD_UART_SET_MODE			},	//CMD_SET_MODE,
	//
	{AC_TASK_ENDDEVICE_ID, ENDDEVICE_SET_ACTIVE_MODE},	//CMD_ENDD_SET_ACTIVE_MODE,
	{0, 0},												//CMD_ENDD_OTA_STATUS
	{AC_TASK_ENDDEVICE_ID, ENDDEVICE_SET_DEV_EUI	},	//CMD_ENDD_SET_DEV_EUI ,
	{AC_TASK_ENDDEVICE_ID, ENDDEVICE_SET_APP_EUI	},	//CMD_ENDD_SET_APP_EUI,
	{AC_TASK_ENDDEVICE_ID, ENDDEVICE_SET_APP_KEY	},	//CMD_ENDD_SET_APP_KEY,
	{AC_TASK_ENDDEVICE_ID, ENDDEVICE_SET_NET_ID		},	//CMD_ENDD_SET_NET_ID,
	{AC_TASK_ENDDEVICE_ID, ENDDEVICE_SET_DEV_ADDR	},	//CMD_ENDD_SET_DEV_ADDR,
	{AC_TASK_ENDDEVICE_ID, ENDDEVICE_SET_NKW_SKEY	},	//CMD_ENDD_SET_NKW_SKEY,
	{AC_TASK_ENDDEVICE_ID, ENDDEVICE_SET_APP_SKEY	},	//CMD_ENDD_SET_APP_SKEY,
	{AC_TASK_ENDDEVICE_ID, ENDDEVICE_SET_CLASS		},	//CMD_ENDD_SET_CLASS,
	{AC_TASK_ENDDEVICE_ID, ENDDEVICE_SET_CHAN_MASK	},	//CMD_ENDD_SET_CHAN_MASK,
	{AC_TASK_ENDDEVICE_ID, ENDDEVICE_SET_DATARATE	},	//CMD_ENDD_SET_DATARATE,
	{AC_TASK_ENDDEVICE_ID, ENDDEVICE_SET_RX2_CHAN	},	//CMD_ENDD_SET_RX2_CHAN,
	{AC_TASK_ENDDEVICE_ID, ENDDEVICE_SET_RX2_DR		},	//CMD_ENDD_SET_RX2_DR,
	{AC_TASK_ENDDEVICE_ID, ENDDEVICE_SET_TX_PWR		},	//CMD_ENDD_SET_TX_PWR,
	{AC_TASK_ENDDEVICE_ID, ENDDEVICE_SET_ANT_GAIN	},	//CMD_ENDD_SET_ANT_GAIN,
	{AC_TASK_ENDDEVICE_ID, ENDDEVICE_GET_DEV_EUI	},	//CMD_ENDD_GET_DEV_EUI,
	{AC_TASK_ENDDEVICE_ID, ENDDEVICE_GET_APP_EUI	},	//CMD_ENDD_GET_APP_EUI,
	{AC_TASK_ENDDEVICE_ID, ENDDEVICE_GET_APP_KEY	},	//CMD_ENDD_GET_APP_KEY,
	{AC_TASK_ENDDEVICE_ID, ENDDEVICE_GET_NET_ID		},	//CMD_ENDD_GET_NET_ID,
	{AC_TASK_ENDDEVICE_ID, ENDDEVICE_GET_DEV_ADDR	},	//CMD_ENDD_GET_DEV_ADDR,
	{AC_TASK_ENDDEVICE_ID, ENDDEVICE_GET_NKW_SKEY	},	//CMD_ENDD_GET_NKW_SKEY,
	{AC_TASK_ENDDEVICE_ID, ENDDEVICE_GET_APP_SKEY	},	//CMD_ENDD_GET_APP_SKEY,
	{AC_TASK_ENDDEVICE_ID, ENDDEVICE_GET_CLASS		},	//CMD_ENDD_GET_CLASS,
	{AC_TASK_ENDDEVICE_ID, ENDDEVICE_GET_CHAN_MASK	},	//CMD_ENDD_GET_CHAN_MASK,
	{AC_TASK_ENDDEVICE_ID, ENDDEVICE_GET_DATARATE	},	//CMD_ENDD_GET_DATARATE,
	{AC_TASK_ENDDEVICE_ID, ENDDEVICE_GET_RX2_CHAN	},	//CMD_ENDD_GET_RX2_CHAN,
	{AC_TASK_ENDDEVICE_ID, ENDDEVICE_GET_RX2_DR		},	//CMD_ENDD_GET_RX2_DR,
	{AC_TASK_ENDDEVICE_ID, ENDDEVICE_GET_TX_PWR		},	//CMD_ENDD_GET_TX_PWR,
	{AC_TASK_ENDDEVICE_ID, ENDDEVICE_GET_ANT_GAIN	},	//CMD_ENDD_GET_ANT_GAIN,
	{AC_TASK_ENDDEVICE_ID, ENDDEVICE_SEND_UNCONFIRM_MSG	},	//CMD_ENDD_SEND_UNCONFIRM_MSG,
	{AC_TASK_ENDDEVICE_ID, ENDDEVICE_SEND_CONFIRM_MSG	},	//CMD_ENDD_SEND_CONFIRM_MSG,
	{0, 0},												//CMD_ENDD_RECV_CONFIRM_MSG,
	{0, 0},												//CMD_ENDD_RECV_UNCONFIRM_MSG,
	//
	{AC_TASK_FORWARD_ID, FORWARD_SET_TX_CONFIG		},	//CMD_FW_SET_TX_CONFIG,
	{AC_TASK_FORWARD_ID, FORWARD_GET_TX_CONFIG		},	//CMD_FW_GET_TX_CONFIG,
	{AC_TASK_FORWARD_ID, FORWARD_SET_RX_CONFIG		},	//CMD_FW_SET_RX_CONFIG,
	{AC_TASK_FORWARD_ID, FORWARD_GET_RX_CONFIG		},	//CMD_FW_GET_RX_CONFIG,
	{AC_TASK_FORWARD_ID, FORWARD_SEND_MSG			},	//CMD_FW_SEND,
	{0, 0},												//CMD_FW_RECV,
	//
	{AC_TASK_SERVER_ID, SERVER_SET_TX_CONFIG		},	//CMD_SERV_SET_TX_CONFIG,
	{AC_TASK_SERVER_ID, SERVER_GET_TX_CONFIG		},	//CMD_SERV_GET_TX_CONFIG,
	{AC_TASK_SERVER_ID, SERVER_SET_RX_CONFIG		},	//CMD_SERV_SET_RX_CONFIG,
	{AC_TASK_SERVER_ID, SERVER_GET_RX_CONFIG		},	//CMD_SERV_GET_RX_CONFIG,
	{AC_TASK_SERVER_ID, SERVER_SET_NET_PARAM		},	//CMD_SERV_SET_NET_PARAM,
	{AC_TASK_SERVER_ID, SERVER_GET_NET_PARAM,		},	//CMD_SERV_GET_NET_PARAM,
	{AC_TASK_SERVER_ID, SERVER_SET_JOINT_PERMIT		},	//CMD_SERV_SET_JOINT_PERMIT,
	{AC_TASK_SERVER_ID, SERVER_GET_JOINT_PERMIT		},	//CMD_SERV_GET_JOINT_PERMIT,
	{0, 0},												//CMD_SERV_RESP_JOINT_PERMIT,
	{AC_TASK_SERVER_ID, SERVER_REMOVE_DEVICE		},	//CMD_SERV_REMOVE_DEVICE,
	{AC_TASK_SERVER_ID, SERVER_REMOVE_DEVICE_ALL	},	//CMD_SERV_REMOVE_DEVICE_ALL,
	{AC_TASK_SERVER_ID, SERVER_SEND_UNCONFIRM_MSG	},	//CMD_SERV_SEND_UNCONFIRM_MSG,
	{AC_TASK_SERVER_ID, SERVER_SEND_CONFIRM_MSG		},	//CMD_SERV_SEND_CONFIRM_MSG,
	{0, 0},												//CMD_SERV_RECV_UNCONFAIRM,
	{0, 0},												//CMD_SERV_RECV_CONFIRM_MSG,
	{0, 0},												//CMD_SERV_RECV_CONFIRM,
};


static struct cmd_uart_frame_t {
	uint8_t		frame_sop;
	uint32_t	len;
	uint8_t		data_index;
	uint8_t		data[CMD_UART_BUFFER_SIZE];
	uint8_t		frame_fcs;
} cmd_uart_frame;

static uint8_t uart_rx_frame_state = SOP_STATE;
static uint8_t tx_buffer[CMD_UART_BUFFER_SIZE];

static uint8_t Module_mode = MODE_IDLE;

static uint8_t rx_frame_parser(uint8_t data);
static uint8_t cmd_uart_calcfcs(uint8_t, uint8_t*);

static cmd_t Send_cmd;
static uint8_t Return_code;

void sys_irq_cmd_uart() {
	volatile uint8_t c = 0;
	c = cmd_uart_get_char();

	if (rx_frame_parser(c) == APP_FLAG_OFF) {

	}
}

void task_cmd_uart(ak_msg_t* msg) {

	switch (msg->sig) {
	case CMD_UART_FRAME_TO: {
		uart_rx_frame_state = SOP_STATE;
	}
		break;

	case CMD_UART_SET_MODE	: {
		Return_code = CMD_RETURN_ERR;
		Send_cmd.cmd_id = CMD_SET_MODE;
		Send_cmd.cmd_data = &Return_code;

		if ( get_data_len_common_msg(msg) == 1 ) {
			uint8_t* set_mode = (uint8_t*)get_data_common_msg(msg);
			if (set_mode[0] < 3) {
				Module_mode = set_mode[0];
				//APP_DBG("set_mode: %d\n", Module_mode);
				Return_code = CMD_RETURN_OK;

				if (Module_mode == MODE_ENDDEVICE) {
					endd_init();
				}
				else if (Module_mode == MODE_FORWARD) {
					fw_init();
				}
				else {
					serv_init();
				}
			}
		}
		tx_frame_post((cmd_t*)&Send_cmd, sizeof(Return_code));
	}
		break;

	default:
		break;
	}
}

uint8_t rx_frame_parser(uint8_t ch) {
	uint8_t is_parser_frame = APP_FLAG_ON;

	switch (uart_rx_frame_state) {
	case SOP_STATE: {
		if (CMD_SOP_CHAR == ch) {
			uart_rx_frame_state = LEN_STATE;
			timer_set(AC_TASK_CMD_UART_ID, CMD_UART_FRAME_TO, CMD_UART_FRAME_TO_INTERVAL, TIMER_ONE_SHOT);
		}
		else {
			is_parser_frame = APP_FLAG_OFF;
		}
	}
		break;

	case LEN_STATE: {
		if (ch > CMD_UART_BUFFER_SIZE) {
			timer_remove_attr(AC_TASK_CMD_UART_ID, CMD_UART_FRAME_TO);

			uart_rx_frame_state = SOP_STATE;
			is_parser_frame = APP_FLAG_OFF;
		}
		else {
			cmd_uart_frame.len = ch;
			cmd_uart_frame.data_index = 0;
			uart_rx_frame_state = DATA_STATE;

			timer_set(AC_TASK_CMD_UART_ID, CMD_UART_FRAME_TO, CMD_UART_FRAME_TO_INTERVAL, TIMER_ONE_SHOT);
		}
	}
		break;

	case DATA_STATE: {
		cmd_uart_frame.data[cmd_uart_frame.data_index++] = ch;

		if (cmd_uart_frame.data_index == cmd_uart_frame.len) {
			uart_rx_frame_state = FCS_STATE;
		}
	}
		break;

	case FCS_STATE: {
		timer_remove_attr(AC_TASK_CMD_UART_ID, CMD_UART_FRAME_TO);

		uart_rx_frame_state = SOP_STATE;

		cmd_uart_frame.frame_fcs = ch;

		if (cmd_uart_frame.frame_fcs == cmd_uart_calcfcs(cmd_uart_frame.len, cmd_uart_frame.data)) {

			uint8_t* cmd_id = &cmd_uart_frame.data[0];
			uint8_t* dest_task_id = &Cmd_uart_tbl[*cmd_id].des_task_id;
			uint8_t* dest_sig = &Cmd_uart_tbl[*cmd_id].des_sig;

			//APP_DBG("cmd_id:%d\n", *cmd_id);
			//APP_DBG("len:%d\n", cmd_uart_frame.len - sizeof(uint8_t));

			switch (*dest_task_id) {
			case AC_TASK_CMD_UART_ID: {
				if (*dest_sig == CMD_UART_SET_MODE) {
					task_post_common_msg( *dest_task_id, *dest_sig, &cmd_uart_frame.data[1], cmd_uart_frame.len - sizeof(uint8_t));
				}
			}
				break;
			case AC_TASK_ENDDEVICE_ID: {
				if (Module_mode == MODE_ENDDEVICE) {
					task_post_common_msg( *dest_task_id, *dest_sig, &cmd_uart_frame.data[1], cmd_uart_frame.len - sizeof(uint8_t));
					break;
				}
				else {
					Return_code = CMD_RETURN_ERR;
					Send_cmd.cmd_id = *cmd_id;
					Send_cmd.cmd_data = &Return_code;
					tx_frame_post((cmd_t*)&Send_cmd, sizeof(Return_code));
				}
			}
				break;

			case AC_TASK_FORWARD_ID: {
				if (Module_mode == MODE_FORWARD) {
					task_post_common_msg( *dest_task_id, *dest_sig, &cmd_uart_frame.data[1], cmd_uart_frame.len - sizeof(uint8_t));
					break;
				}
				else {
					Return_code = CMD_RETURN_ERR;
					Send_cmd.cmd_id = *cmd_id;
					Send_cmd.cmd_data = &Return_code;
					tx_frame_post((cmd_t*)&Send_cmd, sizeof(Return_code));
				}
			}
				break;

			case AC_TASK_SERVER_ID: {
				if (Module_mode == MODE_SERVER) {
					task_post_common_msg( *dest_task_id, *dest_sig, &cmd_uart_frame.data[1], cmd_uart_frame.len - sizeof(uint8_t));
					break;
				}
				else {
					Return_code = CMD_RETURN_ERR;
					Send_cmd.cmd_id = *cmd_id;
					Send_cmd.cmd_data = &Return_code;
					tx_frame_post((cmd_t*)&Send_cmd, sizeof(Return_code));
				}
			}
				break;

			default: {
				//Return_code = CMD_RETURN_ERR;
				//Send_cmd.cmd_id = *cmd_id;
				//Send_cmd.cmd_data = &Return_code;
				//tx_frame_post((cmd_t*)&Send_cmd, sizeof(Return_code));
			}
				break;
			}
		}
		else {
			//TODO:
		}
	}
		break;

	default:
		break;
	}

	return is_parser_frame;
}

void tx_frame_post(cmd_t* data, uint32_t data_len) {
	uint8_t frame_len = data_len + 1;
	tx_buffer[0] = CMD_SOP_CHAR;
	tx_buffer[1] = frame_len;
	tx_buffer[2] = data->cmd_id;
	memcpy(&tx_buffer[3], data->cmd_data, data_len);
	tx_buffer[2 + frame_len] = cmd_uart_calcfcs(frame_len, &tx_buffer[2]);

	for (uint8_t i = 0; i < (frame_len + 3); i++) {
		cmd_uart_put_char(tx_buffer[i]);
	}
}

uint8_t cmd_uart_calcfcs(uint8_t len, uint8_t *data_ptr) {
	uint8_t xor_result = len;
	for (int i = 0; i < len; i++, data_ptr++) {
		xor_result = xor_result ^ *(data_ptr);
	}

	//xprintf("FCS:%X\n", xor_result);
	return xor_result;
}
