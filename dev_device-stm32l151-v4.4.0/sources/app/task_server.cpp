#include <string.h>

#include "fsm.h"
#include "port.h"
#include "message.h"
#include "timer.h"

#include "sys_ctrl.h"
#include "sys_dbg.h"

#include "app.h"
#include "app_dbg.h"
#include "eeprom.h"
#include "radio.h"
#include "LoRaMac.h"
#include "LoRaMacCrypto.h"

#include "lorawan_serv.h"
#include "task_list.h"
#include "task_server.h"
#include "task_cmd_uart.h"

//#define SERVER_SEND_JOIN_DELAY			4250//[4200-4300]
#define SERVER_SEND_JOIN_DELAY			5000
#define SERVER_MSG_LIST_SIZE			5

typedef struct serv_msg_t{
	serv_confirmed_msg_t msg;
	uint8_t msg_len;
	uint32_t last_time;
	uint32_t counter;
} serv_msg_t;

static bool Enable_join = false;
static serv_join_permit_t Serv_join_permit = {{0}, {0}, 0};
static serv_join_acept_t Serv_join_acept;

static join_param_t Join_param;

static serv_msg_t Serv_msg_pool[SERVER_MSG_LIST_SIZE];
static uint8_t Serv_msg_top = 0;

static void serv_msg_remove(uint32_t device_address);
static bool serv_msg_check_last_finish(uint32_t device_address);

static void serv_set_join_permit(const serv_join_permit_t* join);
static serv_join_permit_t* serv_get_join_permit();

static bool serv_msg_put(serv_confirmed_msg_t* msg, uint8_t msg_len);

void task_server(ak_msg_t* msg) {

	static cmd_t Send_cmd;
	static uint8_t Return_code = CMD_RETURN_ERR;
	Send_cmd.cmd_data = &Return_code;

	switch (msg->sig) {

	case SERVER_SET_TX_CONFIG: {
		Send_cmd.cmd_id = CMD_SERV_SET_TX_CONFIG;

		if ( get_data_len_common_msg(msg) == sizeof(serv_tx_config_t) ) {
			serv_tx_config_t* cfg = (serv_tx_config_t*)get_data_common_msg(msg);
			if (cfg->tx_frequency <= 525000000 && cfg->tx_frequency >= 410000000 \
					&& cfg->tx_datarater <= 5 && cfg->tx_power <= 20) {
				serv_set_tx_config(cfg);
				APP_DBG("tx_frequency: %d\n", cfg->tx_frequency);
				APP_DBG("tx_datarater: %d\n", cfg->tx_datarater);
				APP_DBG("tx_power: %d\n", cfg->tx_power);
				Return_code = CMD_RETURN_OK;
			}
		}
		tx_frame_post((cmd_t*)&Send_cmd, sizeof(Return_code));
	}
		break;

	case SERVER_GET_TX_CONFIG: {
		Send_cmd.cmd_id = CMD_SERV_GET_TX_CONFIG;

		if ( get_data_len_common_msg(msg) == 0 ) {
			serv_tx_config_t* cfg = serv_get_tx_config();
			Send_cmd.cmd_data = (uint8_t*)cfg;
		}
		tx_frame_post((cmd_t*)&Send_cmd, sizeof(serv_tx_config_t));
	}
		break;

	case SERVER_SET_RX_CONFIG: {
		Send_cmd.cmd_id = CMD_SERV_SET_RX_CONFIG;

		if ( get_data_len_common_msg(msg) == sizeof(serv_rx_config_t) ) {
			serv_rx_config_t* cfg = (serv_rx_config_t*)get_data_common_msg(msg);
			if (cfg->rx_frequency <= 525000000 && cfg->rx_frequency >= 410000000 \
					&& cfg->rx_datarater <= 5) {
				serv_set_rx_config(cfg);
				APP_DBG("rx_frequency: %d\n", cfg->rx_frequency);
				APP_DBG("rx_datarater: %d\n", cfg->rx_datarater);
				Return_code = CMD_RETURN_OK;
			}
		}
		tx_frame_post((cmd_t*)&Send_cmd, sizeof(Return_code));
	}
		break;

	case SERVER_GET_RX_CONFIG: {
		Send_cmd.cmd_id = CMD_SERV_GET_RX_CONFIG;

		if ( get_data_len_common_msg(msg) == 0 ) {
			serv_rx_config_t* cfg = serv_get_rx_config();
			Send_cmd.cmd_data = (uint8_t*)cfg;
		}
		tx_frame_post((cmd_t*)&Send_cmd, sizeof(serv_rx_config_t));
	}
		break;

	case SERVER_SET_NET_PARAM: {
		Send_cmd.cmd_id = CMD_SERV_SET_NET_PARAM;

		if ( get_data_len_common_msg(msg) == sizeof(serv_init_param_t) ) {
			serv_init_param_t* init = (serv_init_param_t*)get_data_common_msg(msg);
			serv_set_net_param(init);
			APP_DBG("nwk_id: %d\n", init->network_id);
			APP_DBG_HEX(init->application_key, 16);
			Return_code = CMD_RETURN_OK;
		}
		tx_frame_post((cmd_t*)&Send_cmd, sizeof(Return_code));
	}
		break;

	case SERVER_GET_NET_PARAM: {
		Send_cmd.cmd_id = CMD_SERV_GET_NET_PARAM;

		if ( get_data_len_common_msg(msg) == 0 ) {
			serv_init_param_t* init = serv_get_net_param();
			Send_cmd.cmd_data = (uint8_t*)init;
		}
		tx_frame_post((cmd_t*)&Send_cmd, sizeof(serv_init_param_t));
	}
		break;

	case SERVER_SET_JOINT_PERMIT: {
		Send_cmd.cmd_id = CMD_SERV_SET_JOINT_PERMIT;

		if ( get_data_len_common_msg(msg) == sizeof(serv_join_permit_t) ) {
			serv_join_permit_t* join_permit = (serv_join_permit_t*)get_data_common_msg(msg);
			serv_set_join_permit(join_permit);
			APP_DBG_HEX(join_permit->application_Eui, 8);\
			APP_DBG_HEX(join_permit->devive_Eui, 8);
			APP_DBG("timeout: %d\n", join_permit->timeout);
			Return_code = CMD_RETURN_OK;
		}
		tx_frame_post((cmd_t*)&Send_cmd, sizeof(Return_code));
	}
		break;

	case SERVER_GET_JOINT_PERMIT: {
		Send_cmd.cmd_id = CMD_SERV_GET_JOINT_PERMIT;

		if ( get_data_len_common_msg(msg) == 0 ) {
			serv_join_permit_t* join_permit = serv_get_join_permit();
			Send_cmd.cmd_data = (uint8_t*)join_permit;
		}
		tx_frame_post((cmd_t*)&Send_cmd, sizeof(serv_join_permit_t));
	}
		break;

	case SERVER_REMOVE_DEVICE: {
		Send_cmd.cmd_id = CMD_SERV_REMOVE_DEVICE;

		if ( get_data_len_common_msg(msg) == sizeof(uint32_t) ) {
			uint32_t* addr = (uint32_t*)get_data_common_msg(msg);
			serv_remove_device(*addr);
			APP_DBG("Address: %d\n", addr);
			Return_code = CMD_RETURN_OK;
		}
		tx_frame_post((cmd_t*)&Send_cmd, sizeof(Return_code));
	}
		break;

	case SERVER_REMOVE_DEVICE_ALL: {
		Send_cmd.cmd_id = CMD_SERV_REMOVE_DEVICE_ALL;

		if ( get_data_len_common_msg(msg) == 0 ) {
			serv_remove_device_all();
			APP_DBG("Remove device all\n");
			Return_code = CMD_RETURN_OK;
		}
		tx_frame_post((cmd_t*)&Send_cmd, sizeof(Return_code));
	}
		break;

	case SERVER_SEND_UNCONFIRM_MSG: {
		Send_cmd.cmd_id = CMD_SERV_SEND_UNCONFIRM_MSG;
		uint8_t msg_len = get_data_len_common_msg(msg);
		serv_unconfirmed_msg_t* unconfirm_msg = (serv_unconfirmed_msg_t*)get_data_common_msg(msg);

		if (msg_len >= sizeof(unconfirm_msg->header)) {
			serv_send_unconfirm_msg(unconfirm_msg, msg_len);
			APP_DBG("Address: %d\n", unconfirm_msg->header.device_address);
			Return_code = CMD_RETURN_OK;
		}

		tx_frame_post((cmd_t*)&Send_cmd, sizeof(Return_code));
	}
		break;

	case SERVER_SEND_CONFIRM_MSG: {
		Send_cmd.cmd_id = CMD_SERV_SEND_CONFIRM_MSG;
		uint8_t msg_len = get_data_len_common_msg(msg);
		serv_confirmed_msg_t* confirm_msg = (serv_confirmed_msg_t*)get_data_common_msg(msg);

		if (msg_len >= sizeof(confirm_msg->header)) {
			if (serv_msg_put(confirm_msg, msg_len)) {
				serv_send_confirm_msg(confirm_msg, msg_len);
				APP_DBG("Address: %d\n", confirm_msg->header.device_address);
				Return_code = CMD_RETURN_OK;
			}
		}

		tx_frame_post((cmd_t*)&Send_cmd, sizeof(Return_code));
	}
		break;

		/******************************************************************************************/

	case SERVER_RECV_JOIN_MSG: {
		APP_DBG_SIG("SERVER_RECV_JOIN_MSG\n");
		join_param_t* param = (join_param_t*)get_data_common_msg(msg);
		if (param->is_rejoin) {
			memcpy(&Join_param, param, sizeof(join_param_t));
			timer_set(AC_TASK_SERVER_ID, SERVER_SEND_JOIN, SERVER_SEND_JOIN_DELAY, TIMER_ONE_SHOT);

			//serv_send_join_msg(&Join_param);
		}
		else if (Enable_join) {
			if ( (memcmp(param->application_Eui, Serv_join_permit.application_Eui, 8) == 0) && \
				 (memcmp(param->device_Eui, Serv_join_permit.devive_Eui, 8) == 0)) {

				Enable_join = false;
				timer_remove_attr(AC_TASK_SERVER_ID, SERVER_JOIN_TIMEOUT);

				memcpy(&Join_param, param, sizeof(join_param_t));
				timer_set(AC_TASK_SERVER_ID, SERVER_SEND_JOIN, SERVER_SEND_JOIN_DELAY, TIMER_ONE_SHOT);

				serv_send_join_msg(&Join_param);

				//TODO
			}
			else {
				APP_DBG("Eui invalid\n");
			}
		}

	}
		break;

	case SERVER_SEND_JOIN: {
		APP_DBG_SIG("SERVER_SEND_JOIN\n");

		serv_send_join_msg(&Join_param);

		if (!Join_param.is_rejoin) {
			memcpy(Serv_join_acept.application_Eui, Join_param.application_Eui, 8);
			memcpy(Serv_join_acept.devive_Eui, Join_param.device_Eui, 8);
			Serv_join_acept.device_address = Join_param.device_address;

			Send_cmd.cmd_id = CMD_SERV_RESP_JOINT_PERMIT;
			Send_cmd.cmd_data = (uint8_t*)&Serv_join_acept;
			tx_frame_post((cmd_t*)&Send_cmd, sizeof(serv_join_acept_t));
		}

	}
		break;

	case SERVER_JOIN_TIMEOUT: {
		APP_DBG_SIG("SERVER_JOIN_TIMEOUT\n");

		Enable_join = false;
		timer_remove_attr(AC_TASK_SERVER_ID, SERVER_JOIN_TIMEOUT);
	}
		break;

	case SERVER_RECV_UNCONFIRM_MSG: {
		APP_DBG_SIG("SERVER_RECV_UNCONFIRM_MSG\n");

		serv_unconfirmed_msg_t* unconfirmed = (serv_unconfirmed_msg_t*)get_data_common_msg(msg);
		uint8_t msg_len = get_data_len_common_msg(msg);

		if (unconfirmed->header.receive_ack) {
			if (Serv_msg_top) {
				for (int index=0; index<Serv_msg_top; index++) {
					if (Serv_msg_pool[index].msg.header.device_address == unconfirmed->header.device_address) {
						uint32_t device_address = unconfirmed->header.device_address;
						serv_msg_remove(device_address);
						APP_DBG("receive_ack: x%08X", device_address);

						if (msg_len == sizeof(unconfirmed->header)) {
							Send_cmd.cmd_id = CMD_SERV_RECV_CONFIRM;
							Send_cmd.cmd_data = (uint8_t*)&device_address;
							tx_frame_post((cmd_t*)&Send_cmd, sizeof(device_address));
						}

						break;
					}
				}
			}

		}

		Send_cmd.cmd_id = CMD_SERV_RECV_UNCONFIRM_MSG;
		Send_cmd.cmd_data = (uint8_t*)unconfirmed;
		tx_frame_post((cmd_t*)&Send_cmd, msg_len);

	}
		break;

	case SERVER_RECV_CONFIRM_MSG: {
		APP_DBG_SIG("SERVER_RECV_CONFIRM_MSG\n");

		serv_confirmed_msg_t* confirmed = (serv_confirmed_msg_t*)get_data_common_msg(msg);
		uint8_t msg_len = get_data_len_common_msg(msg);

		if (confirmed->header.receive_ack) {
			if (Serv_msg_top) {
				for (int index=0; index<Serv_msg_top; index++) {
					if (Serv_msg_pool[index].msg.header.device_address == confirmed->header.device_address) {
						uint32_t device_address = confirmed->header.device_address;
						serv_msg_remove(device_address);
						APP_DBG("receive_ack: x%08X", device_address);

						if (msg_len == sizeof(confirmed->header)) {
							Send_cmd.cmd_id = CMD_SERV_RECV_CONFIRM;
							Send_cmd.cmd_data = (uint8_t*)&device_address;
							tx_frame_post((cmd_t*)&Send_cmd, sizeof(device_address));
						}

						break;
					}
				}
			}
		}

		Send_cmd.cmd_id = CMD_SERV_RECV_CONFIRM_MSG;
		Send_cmd.cmd_data = (uint8_t*)confirmed;
		tx_frame_post((cmd_t*)&Send_cmd, msg_len);
	}
		break;

	case SERVER_SEND_MSG_RETRY: {
		//APP_DBG_SIG("SERVER_SEND_MSG_RETRY\n");
		serv_msg_t* node;

		if (Serv_msg_top) {
			for (int index = 0; index<Serv_msg_top; index++) {
				node = &Serv_msg_pool[Serv_msg_top];
				if (node->counter < node->msg.header.retries) {
					uint32_t now = sys_ctrl_millis();
					if ((now - node->last_time) >= 10000) {
						node->last_time = now;
						node->counter++;
						serv_send_confirm_msg(&node->msg, node->msg_len);
					}
				}
				else {
					serv_msg_remove(node->msg.header.device_address);
				}
			}
		}
	}
		break;

	default:
		break;
	}
}

void serv_set_join_permit(const serv_join_permit_t* join) {
	xprintf("serv_set_join_permit\n");
	memcpy(&Serv_join_permit, join, sizeof(serv_join_permit_t));
	Enable_join = true;
	timer_set(AC_TASK_SERVER_ID, SERVER_JOIN_TIMEOUT, join->timeout * 1000, TIMER_ONE_SHOT);
}

serv_join_permit_t* serv_get_join_permit() {
	return &Serv_join_permit;
}

bool serv_msg_put(serv_confirmed_msg_t* msg, uint8_t msg_len) {
	if (serv_msg_check_last_finish(msg->header.device_address)) {
		if (Serv_msg_top < SERVER_MSG_LIST_SIZE) {
			serv_msg_t* serv_msg = &Serv_msg_pool[Serv_msg_top];
			serv_msg->msg_len = msg_len;
			serv_msg->last_time = sys_ctrl_millis();
			serv_msg->counter = 0;
			memcpy(&serv_msg->msg, msg, msg_len);

			Serv_msg_top++;
			return true;
		}
	}
	return false;
}

void serv_msg_remove(uint32_t device_address) {
	if (!Serv_msg_top) return;
	for (int index=0; index<Serv_msg_top; index++) {
		if (Serv_msg_pool[index].msg.header.device_address == device_address) {
			if (index != (Serv_msg_top -1)) {
				memcpy(&Serv_msg_pool[index], &Serv_msg_pool[index+1], (Serv_msg_top-index-1)*sizeof(serv_msg_t));
			}
			Serv_msg_top--;
			return;
		}
	}
}

bool serv_msg_check_last_finish(uint32_t device_address) {
	if (!Serv_msg_top) return true;
	for (int index=0; index<Serv_msg_top; index++) {
		if (Serv_msg_pool[index].msg.header.device_address == device_address) {
			return false;
		}
	}
	return true;
}
