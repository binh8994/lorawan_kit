#include <string.h>

#include "fsm.h"
#include "port.h"
#include "message.h"
#include "timer.h"

#include "sys_ctrl.h"
#include "sys_dbg.h"

#include "app.h"
#include "app_dbg.h"
#include "eeprom.h"
#include "radio.h"
#include "LoRaMac.h"
#include "LoRaMacCrypto.h"

#include "lorawan_fw.h"
#include "task_list.h"
#include "task_cmd_uart.h"

#include "task_forward.h"

void task_forward(ak_msg_t* msg) {

	/*Set default return code*/
	static cmd_t Send_cmd;
	static uint8_t Return_code = CMD_RETURN_ERR;
	Send_cmd.cmd_data = &Return_code;

	switch (msg->sig) {

	case FORWARD_SET_TX_CONFIG: {
		Send_cmd.cmd_id = CMD_FW_SET_TX_CONFIG;
		if ( get_data_len_common_msg(msg) == sizeof(fw_tx_config_t) ) {
			fw_tx_config_t* cfg = (fw_tx_config_t*)get_data_common_msg(msg);
			if (cfg->tx_frequency <= 525000000 && cfg->tx_frequency >= 410000000 \
					&& cfg->tx_datarater <= 5 && cfg->tx_power <= 20) {
				fw_set_tx_config(cfg);
				//APP_DBG("tx_frequency: %d\n", cfg->tx_frequency);
				//APP_DBG("tx_datarater: %d\n", cfg->tx_datarater);
				//APP_DBG("tx_power: %d\n", cfg->tx_power);
				Return_code = CMD_RETURN_OK;
			}
		}
		tx_frame_post((cmd_t*)&Send_cmd, sizeof(Return_code));
	}
		break;

	case FORWARD_GET_TX_CONFIG: {
		Send_cmd.cmd_id = CMD_FW_GET_TX_CONFIG;
		if ( get_data_len_common_msg(msg) == 0 ) {
			fw_tx_config_t* cfg = fw_get_tx_config();
			Send_cmd.cmd_data = (uint8_t*)cfg;
		}
		tx_frame_post((cmd_t*)&Send_cmd, sizeof(fw_tx_config_t));
	}
		break;

	case FORWARD_SET_RX_CONFIG: {
		Send_cmd.cmd_id = CMD_FW_SET_RX_CONFIG;
		if ( get_data_len_common_msg(msg) == sizeof(fw_rx_config_t) ) {
			fw_rx_config_t* cfg = (fw_rx_config_t*)get_data_common_msg(msg);
			if (cfg->rx_frequency <= 525000000 && cfg->rx_frequency >= 410000000 && cfg->rx_datarater <= 5) {
				fw_set_rx_config(cfg);
				//APP_DBG("rx_frequency: %d\n", cfg->rx_frequency);
				//APP_DBG("rx_datarater: %d\n", cfg->rx_datarater);
				Return_code = CMD_RETURN_OK;
			}
		}
		tx_frame_post((cmd_t*)&Send_cmd, sizeof(Return_code));
	}
		break;

	case FORWARD_GET_RX_CONFIG: {
		Send_cmd.cmd_id = CMD_FW_GET_RX_CONFIG;
		if ( get_data_len_common_msg(msg) == 0 ) {
			fw_rx_config_t* cfg = fw_get_rx_config();
			Send_cmd.cmd_data = (uint8_t*)cfg;
		}
		tx_frame_post((cmd_t*)&Send_cmd, sizeof(fw_rx_config_t));
	}
		break;

	case FORWARD_SEND_MSG: {
		Send_cmd.cmd_id = CMD_FW_SEND;
		if ( uint8_t msg_len = get_data_len_common_msg(msg) ) {
			uint8_t* ref_data = (uint8_t*)get_data_common_msg(msg);
			fw_send(ref_data, msg_len);
			//APP_DBG("send\n");
			Return_code = CMD_RETURN_OK;
		}
		tx_frame_post((cmd_t*)&Send_cmd, sizeof(Return_code));
	}
		break;

	default:
		break;
	}
}
