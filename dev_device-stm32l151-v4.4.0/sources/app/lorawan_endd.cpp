#include <string.h>

#include "fsm.h"
#include "port.h"
#include "timer.h"
#include "message.h"

#include "sys_ctrl.h"
#include "sys_dbg.h"
#include "sys_io.h"
#include "xprintf.h"

#include "app.h"
#include "app_dbg.h"
#include "task_list.h"
#include "app_bsp.h"

#include "sx1276_cfg.h"
#include "sx1276.h"

#include "LoRaMac-definitions.h"
#include "LoRaMac.h"
#include "LoRaMacCrypto.h"
#include "Region.h"

#include "aes.h"
#include "cmac.h"

#include "task_cmd_uart.h"
#include "lorawan_endd.h"
#include "app_eeprom.h"
#include "eeprom.h"

enum {
	ACTIVE_OTA,
	ACTIVE_ABP,
};

static endd_recv_common_t RecvCommon;
static cmd_t SendCmd;

static uint16_t ChanMask = 0x0001;
static Rx2ChannelParams_t Rx2Chan = {433175000, DR_0};

static LoRaMacPrimitives_t LoRaMacPrimitives;
static LoRaMacCallback_t LoRaMacCallbacks;
MibRequestConfirm_t MibReq;
endd_setting_t Endd_setting;

static void McpsConfirm( McpsConfirm_t *mcpsConfirm );
static void McpsIndication( McpsIndication_t *mcpsIndication );
static void MlmeConfirm( MlmeConfirm_t *mlmeConfirm );

void endd_init() {

	eeprom_read(ENDD_SETTING_ADDR, (uint8_t*)&Endd_setting, sizeof(endd_setting_t));

	LoRaMacPrimitives.MacMcpsConfirm = McpsConfirm;
	LoRaMacPrimitives.MacMcpsIndication = McpsIndication;
	LoRaMacPrimitives.MacMlmeConfirm = MlmeConfirm;
	LoRaMacCallbacks.GetBatteryLevel = 0;
	LoRaMacInitialization( &LoRaMacPrimitives, &LoRaMacCallbacks, LORAMAC_REGION_EU433 );

	MibReq.Type = MIB_ADR;
	MibReq.Param.AdrEnable = false;
	LoRaMacMibSetRequestConfirm( &MibReq );

	MibReq.Type = MIB_REPEATER_SUPPORT;
	MibReq.Param.EnableRepeaterSupport = false;
	LoRaMacMibSetRequestConfirm( &MibReq );

	MibReq.Type = MIB_PUBLIC_NETWORK;
	MibReq.Param.EnablePublicNetwork = true;
	LoRaMacMibSetRequestConfirm( &MibReq );

	MibReq.Type = MIB_DEVICE_CLASS;
	MibReq.Param.Class = CLASS_C;
	LoRaMacMibSetRequestConfirm( &MibReq );

	MibReq.Type = MIB_CHANNELS_DEFAULT_MASK;
	MibReq.Param.ChannelsDefaultMask = &ChanMask;
	LoRaMacMibSetRequestConfirm( &MibReq );

	MibReq.Type = MIB_CHANNELS_MASK;
	MibReq.Param.ChannelsMask = &ChanMask;
	LoRaMacMibSetRequestConfirm( &MibReq );

	MibReq.Type = MIB_CHANNELS_DATARATE;
	MibReq.Param.ChannelsDatarate = DR_0;
	LoRaMacMibSetRequestConfirm( &MibReq );

	MibReq.Type = MIB_CHANNELS_DEFAULT_DATARATE;
	MibReq.Param.ChannelsDefaultDatarate = DR_0;
	LoRaMacMibSetRequestConfirm( &MibReq );

	MibReq.Type = MIB_RX2_DEFAULT_CHANNEL;
	MibReq.Param.Rx2DefaultChannel = Rx2Chan;
	LoRaMacMibSetRequestConfirm( &MibReq );

	MibReq.Type = MIB_RX2_CHANNEL;
	MibReq.Param.Rx2Channel = Rx2Chan;
	LoRaMacMibSetRequestConfirm( &MibReq );

	MibReq.Type = MIB_CHANNELS_DEFAULT_TX_POWER;
	MibReq.Param.ChannelsDefaultTxPower = TX_POWER_0;
	LoRaMacMibSetRequestConfirm( &MibReq );

	MibReq.Type = MIB_CHANNELS_TX_POWER;
	MibReq.Param.ChannelsTxPower = TX_POWER_0;
	LoRaMacMibSetRequestConfirm( &MibReq );

	MibReq.Type = MIB_ANTENNA_GAIN;
	MibReq.Param.AntennaGain = 0.0;
	LoRaMacMibSetRequestConfirm( &MibReq );

}

bool endd_set_active_mode(uint8_t mode) {
	eeprom_read(ENDD_SETTING_ADDR, (uint8_t*)&Endd_setting, sizeof(endd_setting_t));

	MibReq.Type = MIB_NET_ID;
	MibReq.Param.NetID = Endd_setting.network_id;
	LoRaMacMibSetRequestConfirm( &MibReq );

	MibReq.Type = MIB_DEV_ADDR;
	MibReq.Param.DevAddr = Endd_setting.device_address;
	LoRaMacMibSetRequestConfirm( &MibReq );

	MibReq.Type = MIB_NWK_SKEY;
	MibReq.Param.NwkSKey = (uint8_t*)Endd_setting.network_skey;
	LoRaMacMibSetRequestConfirm( &MibReq );

	MibReq.Type = MIB_APP_SKEY;
	MibReq.Param.AppSKey = (uint8_t*)Endd_setting.application_skey;
	LoRaMacMibSetRequestConfirm( &MibReq );

	if (mode == ACTIVE_ABP) {
		MibReq.Type = MIB_NETWORK_JOINED;
		MibReq.Param.IsNetworkJoined = true;
		LoRaMacMibSetRequestConfirm( &MibReq );
		//APP_DBG("ABP\n");

		return true;
	}
	else if (mode == ACTIVE_OTA){
		MlmeReq_t mlmeReq;
		mlmeReq.Type = MLME_JOIN;
		mlmeReq.Req.Join.DevEui = Endd_setting.device_Eui;
		mlmeReq.Req.Join.AppEui = Endd_setting.application_Eui;
		mlmeReq.Req.Join.AppKey = Endd_setting.application_key;
		mlmeReq.Req.Join.NbTrials = 3;

		if (LORAMAC_STATUS_OK == LoRaMacMlmeRequest( &mlmeReq )) {
			//APP_DBG("OTA\n");
			return true;
		}
	}
	return false;
}

bool endd_set_chanmask(const uint16_t* chanmask) {
	ChanMask = *chanmask;
	MibReq.Type = MIB_CHANNELS_DEFAULT_MASK;
	MibReq.Param.ChannelsDefaultMask = &ChanMask;
	if (LORAMAC_STATUS_OK == LoRaMacMibSetRequestConfirm( &MibReq ) ) {
		MibReq.Type = MIB_CHANNELS_MASK;
		MibReq.Param.ChannelsMask = &ChanMask;
		if (LORAMAC_STATUS_OK == LoRaMacMibSetRequestConfirm( &MibReq ) ) {
			return true;
		}
	}
	return false;
}

uint16_t* endd_get_chanmask() {
	return &ChanMask;
}

bool endd_set_rx2_freq(const uint32_t* freq) {
	Rx2Chan.Frequency = *freq;
	MibReq.Type = MIB_RX2_DEFAULT_CHANNEL;
	MibReq.Param.Rx2DefaultChannel = Rx2Chan;
	if (LORAMAC_STATUS_OK == LoRaMacMibSetRequestConfirm( &MibReq ) ) {
		MibReq.Type = MIB_RX2_CHANNEL;
		MibReq.Param.Rx2Channel = Rx2Chan;
		if (LORAMAC_STATUS_OK == LoRaMacMibSetRequestConfirm( &MibReq ) ) {
			return true;
		}
	}
	return false;
}

uint32_t* endd_get_rx2_freq() {
	return &Rx2Chan.Frequency;
}

bool endd_set_rx2_dr(const uint8_t* dr) {
	Rx2Chan.Datarate = *dr;
	MibReq.Type = MIB_RX2_DEFAULT_CHANNEL;
	MibReq.Param.Rx2DefaultChannel = Rx2Chan;
	if (LORAMAC_STATUS_OK == LoRaMacMibSetRequestConfirm( &MibReq ) ) {
		MibReq.Type = MIB_RX2_CHANNEL;
		MibReq.Param.Rx2Channel = Rx2Chan;
		if (LORAMAC_STATUS_OK == LoRaMacMibSetRequestConfirm( &MibReq ) ) {
			return true;
		}
	}
	return false;
}

uint8_t* endd_get_rx2_dr() {
	return &Rx2Chan.Datarate;
}

bool endd_send_confirm_msg(uint8_t port, uint8_t retries, uint8_t* payload, uint8_t payload_len) {
	//APP_DBG("send_confirm_msg()\n");
	McpsReq_t mcpsReq;
	LoRaMacTxInfo_t txInfo;

	MibReq.Type = MIB_NETWORK_JOINED;

	if( LORAMAC_STATUS_OK == LoRaMacMibGetRequestConfirm( &MibReq )) {
		if( MibReq.Param.IsNetworkJoined == true ) {
			if( LoRaMacQueryTxPossible( payload_len, &txInfo ) != LORAMAC_STATUS_OK ) {
				mcpsReq.Type = MCPS_CONFIRMED;
				mcpsReq.Req.Confirmed.fPort = 0;
				mcpsReq.Req.Confirmed.fBuffer = NULL;
				mcpsReq.Req.Confirmed.fBufferSize = 0;
				mcpsReq.Req.Confirmed.Datarate = 0;

				LoRaMacMcpsRequest( &mcpsReq );
			}
			else {
				MibReq.Type = MIB_CHANNELS_DATARATE;
				LoRaMacMibGetRequestConfirm( &MibReq );

				mcpsReq.Type = MCPS_CONFIRMED;
				mcpsReq.Req.Confirmed.fPort = port;
				mcpsReq.Req.Confirmed.fBuffer = (uint8_t*)payload;
				mcpsReq.Req.Confirmed.fBufferSize = payload_len;
				mcpsReq.Req.Confirmed.NbTrials = retries;
				mcpsReq.Req.Confirmed.Datarate = MibReq.Param.ChannelsDatarate;

				if(LoRaMacMcpsRequest( &mcpsReq ) == LORAMAC_STATUS_OK) {
					return true;
				}
			}
		}
	}

	return false;
}

bool endd_send_unconfirm_msg(uint8_t port, uint8_t* payload, uint8_t payload_len) {
	//APP_DBG("send_unconfirm_msg()\n");
	McpsReq_t mcpsReq;
	LoRaMacTxInfo_t txInfo;

	MibReq.Type = MIB_NETWORK_JOINED;

	if( LORAMAC_STATUS_OK == LoRaMacMibGetRequestConfirm( &MibReq )) {
		if( MibReq.Param.IsNetworkJoined == true ) {
			if( LoRaMacQueryTxPossible( payload_len, &txInfo ) != LORAMAC_STATUS_OK ) {
				mcpsReq.Type = MCPS_UNCONFIRMED;
				mcpsReq.Req.Unconfirmed.fPort = 0;
				mcpsReq.Req.Unconfirmed.fBuffer = NULL;
				mcpsReq.Req.Unconfirmed.fBufferSize = 0;
				mcpsReq.Req.Unconfirmed.Datarate = 0;

				LoRaMacMcpsRequest( &mcpsReq );
			}
			else {
				MibReq.Type = MIB_CHANNELS_DATARATE;
				LoRaMacMibGetRequestConfirm( &MibReq );

				mcpsReq.Type = MCPS_UNCONFIRMED;
				mcpsReq.Req.Unconfirmed.fPort = port;
				mcpsReq.Req.Unconfirmed.fBuffer = (uint8_t*)payload;
				mcpsReq.Req.Unconfirmed.fBufferSize = payload_len;
				mcpsReq.Req.Unconfirmed.Datarate = MibReq.Param.ChannelsDatarate;

				if(LoRaMacMcpsRequest( &mcpsReq ) == LORAMAC_STATUS_OK) {
					return true;
				}
			}
		}
	}

	return false;
}

static void McpsConfirm( McpsConfirm_t *mcpsConfirm ) {
	if( mcpsConfirm->Status == LORAMAC_EVENT_INFO_STATUS_OK ) {
		switch( mcpsConfirm->McpsRequest ) {
		case MCPS_UNCONFIRMED: {
			//APP_PRINT("McpsConf-UNCONF\n");
			break;
		}
		case MCPS_CONFIRMED: {
			//APP_PRINT("McpsConf-CONF\n");
			break;
		}
		case MCPS_PROPRIETARY: {
			//APP_PRINT("McpsConf-PROP\n");
			break;
		}
		default:
			break;
		}

	}
}

static void McpsIndication( McpsIndication_t *mcpsIndication ) {
	if( mcpsIndication->Status != LORAMAC_EVENT_INFO_STATUS_OK ) {
		return;
	}

	switch( mcpsIndication->McpsIndication ) {
	case MCPS_UNCONFIRMED: {
		//APP_PRINT("McpsInd-UNCONF\n");
		SendCmd.cmd_id = CMD_ENDD_RECV_UNCONFIRM_MSG;
		break;
	}
	case MCPS_CONFIRMED: {
		//APP_PRINT("McpsInd-CONF\n");
		SendCmd.cmd_id = CMD_ENDD_RECV_CONFIRM_MSG;
		break;
	}
	case MCPS_PROPRIETARY: {
		//APP_PRINT("McpsInd-PRO\n");
		SendCmd.cmd_id = CMD_ENDD_RECV_UNCONFIRM_MSG;
		break;
	}
	case MCPS_MULTICAST: {
		//APP_PRINT("McpsIndn:MULT\n");
		SendCmd.cmd_id = CMD_ENDD_RECV_UNCONFIRM_MSG;
		break;
	}
	default:
		break;
	}

	if( mcpsIndication->RxData == true ) {
		//APP_PRINT("RxData:true\n");
		RecvCommon.header.port = mcpsIndication->Port;
		RecvCommon.header.rx_slot = mcpsIndication->RxSlot;
		RecvCommon.header.rssi = mcpsIndication->Rssi;
		RecvCommon.header.snr = mcpsIndication->Snr;
		memcpy(RecvCommon.buffer, mcpsIndication->Buffer, mcpsIndication->BufferSize);

		SendCmd.cmd_data = (uint8_t*)&RecvCommon;
		tx_frame_post((cmd_t*)&SendCmd, sizeof(RecvCommon.header) + mcpsIndication->BufferSize);

	}
}

static void MlmeConfirm( MlmeConfirm_t *mlmeConfirm ) {
	uint8_t status;
	switch( mlmeConfirm->MlmeRequest ) {
	case MLME_JOIN: {
		if( mlmeConfirm->Status == LORAMAC_EVENT_INFO_STATUS_OK ) {
			APP_PRINT("JOIN_SUCC\n");

			MibRequestConfirm_t mibReq;
			mibReq.Type = MIB_NET_ID;
			if( LoRaMacMibGetRequestConfirm( &mibReq ) == LORAMAC_STATUS_OK) {
				Endd_setting.network_id = mibReq.Param.NetID;
				APP_PRINT("NetID:0x%08X\n", Endd_setting.network_id);
			}
			mibReq.Type = MIB_DEV_ADDR;
			if( LoRaMacMibGetRequestConfirm( &mibReq ) == LORAMAC_STATUS_OK) {
				Endd_setting.device_address = mibReq.Param.DevAddr;
				APP_PRINT("DevAddr:0x%08X\n", Endd_setting.device_address);
			}
			mibReq.Type = MIB_NWK_SKEY;
			if( LoRaMacMibGetRequestConfirm( &mibReq ) == LORAMAC_STATUS_OK) {
				memcpy(Endd_setting.network_skey, mibReq.Param.NwkSKey, 16);
				APP_PRINT("NwkSKey: ");
				APP_PRINT_HEX(Endd_setting.network_skey, 16);

			}
			mibReq.Type = MIB_APP_SKEY;
			if( LoRaMacMibGetRequestConfirm( &mibReq ) == LORAMAC_STATUS_OK) {
				memcpy(Endd_setting.application_skey, mibReq.Param.AppSKey, 16);
				APP_PRINT("AppSKey: ");
				APP_PRINT_HEX(Endd_setting.application_skey, 16);
			}
			Endd_setting.network_joined = LORAWAN_NWK_JOINED;
			eeprom_write(ENDD_SETTING_ADDR, (uint8_t*)&Endd_setting, sizeof(endd_setting_t));

			status = 1;
			SendCmd.cmd_id = CMD_ENDD_OTA_STATUS;
			SendCmd.cmd_data = (uint8_t*)&status;
			tx_frame_post((cmd_t*)&SendCmd, sizeof(status));
		}
		else {
			APP_PRINT("JOIN_FAIL\n");

			status = 0;
			SendCmd.cmd_id = CMD_ENDD_OTA_STATUS;
			SendCmd.cmd_data = (uint8_t*)&status;
			tx_frame_post((cmd_t*)&SendCmd, sizeof(status));
		}
		break;
	}
	case MLME_LINK_CHECK: {
		if( mlmeConfirm->Status == LORAMAC_EVENT_INFO_STATUS_OK ) {
			//APP_PRINT("LINK\n");
		}
		break;
	}
	default:
		break;
	}

}
