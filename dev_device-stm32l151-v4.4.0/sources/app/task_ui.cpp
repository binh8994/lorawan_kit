#include "../ak/fsm.h"
#include "../ak/port.h"
#include "../ak/message.h"

#include "../sys/sys_ctrl.h"
#include "../driver/button/button.h"
#include "app.h"
#include "app_dbg.h"

#include "task_list.h"
#include "task_ui.h"
#include "lorawan_endd.h"
#include "lora_ping_pong.h"
#include "app_bsp.h"


void task_ui(ak_msg_t* msg) {
	switch (msg->sig) {
	case AC_BUTTON_ONE_CLICK:

		break;

	case AC_BUTTON_LONG_PRESS:


		break;

	case AC_SENT_LEAVE_NETWORK:


		break;

	case AC_PINGPONG_SENT_TEST:

		lora_fw_send();

		break;

	case AC_LORAWAN_SENT_TEST:

		break;

	default:
		break;
	}
}
