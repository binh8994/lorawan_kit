#ifndef __LORAWAN_SERV_H__
#define __LORAWAN_SERV_H__

#include "port.h"

#define SERVER_PHY_MAXPAYLOAD		255

typedef struct {
	uint32_t network_id;
	uint8_t application_key[16];
} __AK_PACKETED serv_init_param_t;

typedef struct {
	uint32_t tx_frequency;
	uint8_t tx_datarater;
	uint8_t tx_power;
} __AK_PACKETED serv_tx_config_t;

typedef struct {
	uint32_t rx_frequency;
	uint8_t rx_datarater;
} __AK_PACKETED serv_rx_config_t;

typedef struct {
	uint8_t devive_Eui[8];
	uint8_t application_Eui[8];
	uint32_t timeout;
} __AK_PACKETED serv_join_permit_t;

typedef struct {
	uint8_t devive_Eui[8];
	uint8_t application_Eui[8];
	uint32_t device_address;
} __AK_PACKETED serv_join_acept_t;

typedef struct {
	struct {
		uint32_t device_address;
		uint8_t receive_ack;
		uint8_t port;
	} __AK_PACKETED header;
	uint8_t data[SERVER_PHY_MAXPAYLOAD];
} __AK_PACKETED serv_unconfirmed_msg_t;

typedef struct {
	struct {
		uint32_t device_address;
		uint8_t receive_ack;
		uint8_t retries;
		uint8_t port;
	} __AK_PACKETED header;
	uint8_t data[SERVER_PHY_MAXPAYLOAD];
} __AK_PACKETED serv_confirmed_msg_t;

typedef union {
	uint8_t value;
	struct {
		uint8_t RX2DR			:4;
		uint8_t RX1DRoffset		:3;
		uint8_t RFU				:1;
	} bits;
} dlsettings_t;

typedef union {
	uint8_t value;
	struct {
		uint8_t Del		:4;
		uint8_t RFU		:4;
	} bits;
} delay_t;

typedef struct {
	uint8_t device_Eui[8];
	uint8_t application_Eui[8];
	uint16_t device_nonce;
	uint32_t device_address;
	uint8_t is_rejoin;
} __AK_PACKETED join_param_t;

typedef struct {
	uint16_t data_len;
	uint8_t* data;
} __AK_PACKETED proprietary_param_t;

extern void serv_set_tx_config(const serv_tx_config_t* tx_config);
extern void serv_set_rx_config(const serv_rx_config_t* rx_config);
extern serv_tx_config_t* serv_get_tx_config();
extern serv_rx_config_t* serv_get_rx_config();

#if 0
extern void serv_set_tx_frequency(uint32_t freq);
extern void serv_set_rx_frequency(uint32_t freq);
extern void serv_set_tx_datarate(uint8_t dr);
extern void serv_set_rx_datarate(uint8_t dr);
extern void serv_set_tx_power(uint8_t power);

extern uint32_t serv_get_tx_frequency();
extern uint32_t serv_get_rx_frequency();
extern uint8_t serv_get_tx_datarate();
extern uint8_t serv_get_rx_datarate();
extern uint8_t serv_get_tx_power();
#endif

extern void serv_init();
extern void serv_set_net_param(const serv_init_param_t* init);
extern serv_init_param_t* serv_get_net_param();

extern void serv_remove_device(uint32_t dev_addr);
extern void serv_remove_device_all();

extern void serv_send_join_msg(const join_param_t* join);
extern void serv_send_unconfirm_msg(const serv_unconfirmed_msg_t* msg, uint8_t msg_len);
extern void serv_send_confirm_msg(const serv_confirmed_msg_t* msg, uint8_t msg_len);

#endif // __LORAWAN_SERV_H__
