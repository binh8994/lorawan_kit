#include <string.h>

#include "fsm.h"
#include "port.h"
#include "timer.h"
#include "message.h"

#include "sys_ctrl.h"
#include "sys_dbg.h"
#include "sys_io.h"
#include "xprintf.h"

#include "app.h"
#include "app_dbg.h"
#include "task_list.h"
#include "app_bsp.h"
#include "task_cmd_uart.h"

#include "sx1276/sx1276_cfg.h"
#include "sx1276/sx1276.h"

#include "LoRaMac-definitions.h"
#include "LoRaMac.h"
#include "LoRaMacCrypto.h"

#include "aes.h"
#include "cmac.h"

#include "lorawan_fw.h"

#define FW_PHY_MAXPAYLOAD			255
#define GW_FREQ						433175000
#define GW_BANDWIDTH				0
// 0: 125 kHz
// 1: 250 kHz
// 2: 500 kHz
// 3: Reserved
#define GW_SF						12
// [SF7..SF12]
#define GW_DR						0	//DR_0
#define GW_CD						1
// 1: 4/5
// 2: 4/6
// 3: 4/7
// 4: 4/8
#define GW_TX_POWER					20	//dBm
#define GW_PREAMBLE_LENGTH			8
#define GW_SYMBOL_TIMEOUT			5
#define GW_FIX_LENGTH_PAY			false
#define GW_IQ_TX					true
#define GW_IQ_RX					false
#define GW_TX_TIMEOUT				3000
#define GW_RX_TIMEOUT				0

static fw_tx_config_t Fw_tx_config = {GW_FREQ, GW_DR, GW_TX_POWER};
static fw_rx_config_t Fw_rx_config = {GW_FREQ, GW_DR};
static uint8_t SF[] = {12, 11, 10, 9, 8, 7};

static cmd_t Send_cmd;

static RadioEvents_t Lora_events;
static void on_tx_done( void );
static void on_rx_done( uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr );
static void on_tx_timeout( void );
static void on_rx_timeout( void );
static void on_rx_error( void );

void change_to_rx() {
	Radio.SetChannel( Fw_rx_config.rx_frequency );
	Radio.SetMaxPayloadLength( MODEM_LORA, FW_PHY_MAXPAYLOAD );
	Radio.SetRxConfig( MODEM_LORA, GW_BANDWIDTH, SF[Fw_rx_config.rx_datarater],
			GW_CD, 0, GW_PREAMBLE_LENGTH,
			GW_SYMBOL_TIMEOUT, GW_FIX_LENGTH_PAY,
			0, true, 0, 0, GW_IQ_RX, true );
	Radio.Rx( GW_RX_TIMEOUT );
}

void fw_init() {
	//APP_DBG("lora_fw_init\n");

	Lora_events.TxDone =	on_tx_done;
	Lora_events.RxDone =	on_rx_done;
	Lora_events.TxTimeout = on_tx_timeout;
	Lora_events.RxTimeout = on_rx_timeout;
	Lora_events.RxError =	on_rx_error;

	Radio.Init( &Lora_events );
	Radio.SetPublicNetwork(true);
	Radio.SetMaxPayloadLength( MODEM_LORA, FW_PHY_MAXPAYLOAD );
	Radio.Sleep();
}

void fw_set_tx_config(const fw_tx_config_t* tx_config) {
	memcpy(&Fw_tx_config, tx_config, sizeof(fw_tx_config_t));
	Radio.SetTxConfig( MODEM_LORA, Fw_tx_config.tx_power, 0, GW_BANDWIDTH,
					   SF[Fw_tx_config.tx_datarater], GW_CD,
			GW_PREAMBLE_LENGTH, GW_FIX_LENGTH_PAY,
			true, 0, 0, GW_IQ_TX, GW_TX_TIMEOUT );
	Radio.Sleep();
}

void fw_set_rx_config(const fw_rx_config_t* rx_config) {
	memcpy(&Fw_rx_config, rx_config, sizeof(fw_rx_config_t));
	change_to_rx();
}

fw_tx_config_t* fw_get_tx_config() {
	return &Fw_tx_config;
}

fw_rx_config_t* fw_get_rx_config() {
	return &Fw_rx_config;
}

bool fw_send(uint8_t* buffer, uint8_t length) {
	//APP_DBG("lora_fw_send\n");

	if(Radio.GetStatus() != RF_TX_RUNNING) {
		Radio.Send(buffer, length);
		return true;
	}
	return false;
}

void on_tx_done( void ) {
	//APP_DBG("on_tx_done\n");
	change_to_rx();
}

void on_tx_timeout( void ) {
	//APP_DBG("on_tx_timeout\n");
	change_to_rx();
}

void on_rx_done( uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr )
{
	(void) payload;	(void) size;	(void) rssi;	(void) snr;
	//APP_DBG("on_rx_done, size:%d, rssi:%d, snr:%d\n\n", size, rssi, snr);
	//APP_DBG_HEX(payload, size);

	Send_cmd.cmd_id = CMD_FW_RECV;
	Send_cmd.cmd_data = payload;
	tx_frame_post(&Send_cmd, size);

	change_to_rx();
}

void on_rx_timeout( void ) {
	//APP_DBG("on_rx_timeout\n");
	change_to_rx();
}

void on_rx_error( void ) {
	//APP_DBG("on_rx_error\n");
	change_to_rx();
}

