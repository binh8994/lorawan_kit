#ifndef __TASK_LIST_H__
#define __TASK_LIST_H__

#include "ak.h"
#include "task.h"

extern task_t app_task_table[];

/*****************************************************************************/
/*  DECLARE: Internal Task ID
 *  Note: Task id MUST be increasing order.
 */
/*****************************************************************************/
enum {
	/* SYSTEM TASKS */
	TASK_TIMER_TICK_ID,

	/* APP TASKS */
	AC_TASK_SHELL_ID,
	AC_TASK_LIFE_ID,
	AC_TASK_LORAMAC_ID,
	AC_TASK_CMD_UART_ID,
	AC_TASK_ENDDEVICE_ID,
	AC_TASK_FORWARD_ID,
	AC_TASK_SERVER_ID,

	/* NRF24 NETWORKS */
	//RF24_PHY_ID,
	//RF24_MAC_ID,
	//RF24_NWK_ID,

	/* LINK */
	//LINK_PHY_ID,
	//LINK_MAC_ID,

	/* EOT task ID */
	AK_TASK_EOT_ID,
};

/*****************************************************************************/
/*  DECLARE: Task entry point
 */
/*****************************************************************************/
/* APP TASKS */
extern void task_shell(ak_msg_t*);
extern void task_life(ak_msg_t*);
extern void task_loramac(ak_msg_t*);
extern void task_cmd_uart(ak_msg_t*);
extern void task_enddevice(ak_msg_t*);
extern void task_forward(ak_msg_t*);
extern void task_server(ak_msg_t*);

///* RF24 NETWORK TASK */
//extern void task_rf24_phy(ak_msg_t*);
//extern void task_rf24_mac(ak_msg_t*);
//extern void task_rf24_nwk(ak_msg_t*);

///* LINK TASK */
//extern void task_link_phy(ak_msg_t*);
//extern void task_link_mac(ak_msg_t*);

#endif //__TASK_LIST_H__
