#include <string.h>

#include "fsm.h"
#include "port.h"
#include "timer.h"
#include "message.h"

#include "sys_ctrl.h"
#include "sys_dbg.h"
#include "sys_io.h"
#include "xprintf.h"

#include "app.h"
#include "app_dbg.h"
#include "task_list.h"
#include "app_bsp.h"
#include "eeprom.h"
#include "loramac/mac/LoRaMac.h"

#include "task_cmd_uart.h"
#include "lorawan_endd.h"
#include "task_enddevice.h"

void task_enddevice(ak_msg_t* msg) {

	/*Set default return code*/
	static cmd_t Send_cmd;
	static uint8_t Return_code = CMD_RETURN_ERR;
	Send_cmd.cmd_data = &Return_code;

	switch (msg->sig) {

	case ENDDEVICE_SET_ACTIVE_MODE: {
		Send_cmd.cmd_id = CMD_ENDD_SET_ACTIVE_MODE;
		if ( get_data_len_common_msg(msg) == sizeof(uint8_t) ) {

			uint8_t* mode = (uint8_t*)get_data_common_msg(msg);
			if (endd_set_active_mode(*mode)) {
				//APP_DBG("Act: %d\n", *mode);

				Return_code = CMD_RETURN_OK;
			}
		}
		tx_frame_post((cmd_t*)&Send_cmd, sizeof(Return_code));
	}
		break;

	case ENDDEVICE_SET_DEV_EUI: {
		Send_cmd.cmd_id = CMD_ENDD_SET_DEV_EUI;
		uint8_t msg_len = get_data_len_common_msg(msg);
		uint8_t* payload = get_data_common_msg(msg);
		if ( msg_len == 8 ) {
			eeprom_read(ENDD_SETTING_ADDR, (uint8_t*)&Endd_setting, sizeof(endd_setting_t));
			memcpy(Endd_setting.device_Eui, payload, msg_len);
			eeprom_write(ENDD_SETTING_ADDR, (uint8_t*)&Endd_setting, sizeof(endd_setting_t));
			//APP_DBG("device_Eui:");
			//APP_DBG_HEX(Endd_setting.device_Eui, msg_len);

			Return_code = CMD_RETURN_OK;

		}
		tx_frame_post((cmd_t*)&Send_cmd, sizeof(Return_code));
	}
		break;

	case ENDDEVICE_SET_APP_EUI: {
		Send_cmd.cmd_id = CMD_ENDD_SET_APP_EUI;
		uint8_t msg_len = get_data_len_common_msg(msg);
		uint8_t* payload = get_data_common_msg(msg);		
		if ( msg_len == 8 ) {
			eeprom_read(ENDD_SETTING_ADDR, (uint8_t*)&Endd_setting, sizeof(endd_setting_t));
			memcpy(Endd_setting.application_Eui, payload, msg_len);
			eeprom_write(ENDD_SETTING_ADDR, (uint8_t*)&Endd_setting, sizeof(endd_setting_t));
			//APP_DBG("app_Eui:");
			//APP_DBG_HEX(Endd_setting.application_Eui, msg_len);

			Return_code = CMD_RETURN_OK;

		}
		tx_frame_post((cmd_t*)&Send_cmd, sizeof(Return_code));
	}
		break;

	case ENDDEVICE_SET_APP_KEY: {
		Send_cmd.cmd_id = CMD_ENDD_SET_APP_KEY;
		uint8_t msg_len = get_data_len_common_msg(msg);
		uint8_t* payload = get_data_common_msg(msg);
		if ( msg_len == 16 ) {
			eeprom_read(ENDD_SETTING_ADDR, (uint8_t*)&Endd_setting, sizeof(endd_setting_t));
			memcpy(Endd_setting.application_key, payload, msg_len);
			eeprom_write(ENDD_SETTING_ADDR, (uint8_t*)&Endd_setting, sizeof(endd_setting_t));
			//APP_DBG("app_key:");
			//APP_DBG_HEX(Endd_setting.application_key, msg_len);

			Return_code = CMD_RETURN_OK;

		}
		tx_frame_post((cmd_t*)&Send_cmd, sizeof(Return_code));
	}
		break;

	case ENDDEVICE_SET_NET_ID: {
		Send_cmd.cmd_id = CMD_ENDD_SET_NET_ID;
		if ( get_data_len_common_msg(msg) == 3 ) {
			eeprom_read(ENDD_SETTING_ADDR, (uint8_t*)&Endd_setting, sizeof(endd_setting_t));
			Endd_setting.network_id = *((uint32_t*)get_data_common_msg(msg));
			eeprom_write(ENDD_SETTING_ADDR, (uint8_t*)&Endd_setting, sizeof(endd_setting_t));
			//APP_DBG("nwk_id: x%08x\n", Endd_setting.network_id);

			Return_code = CMD_RETURN_OK;

		}
		tx_frame_post((cmd_t*)&Send_cmd, sizeof(Return_code));
	}
		break;

	case ENDDEVICE_SET_DEV_ADDR: {
		Send_cmd.cmd_id = CMD_ENDD_SET_DEV_ADDR;
		if ( get_data_len_common_msg(msg) == sizeof(uint32_t) ) {
			eeprom_read(ENDD_SETTING_ADDR, (uint8_t*)&Endd_setting, sizeof(endd_setting_t));
			Endd_setting.device_address = *((uint32_t*)get_data_common_msg(msg));
			eeprom_write(ENDD_SETTING_ADDR, (uint8_t*)&Endd_setting, sizeof(endd_setting_t));
			//APP_DBG("dev_addr: x%08x\n", Endd_setting.device_address);

			Return_code = CMD_RETURN_OK;

		}
		tx_frame_post((cmd_t*)&Send_cmd, sizeof(Return_code));
	}
		break;

	case ENDDEVICE_SET_NKW_SKEY: {
		Send_cmd.cmd_id = CMD_ENDD_SET_NKW_SKEY;
		uint8_t msg_len = get_data_len_common_msg(msg);
		uint8_t* payload = get_data_common_msg(msg);
		if ( msg_len == 16 ) {
			eeprom_read(ENDD_SETTING_ADDR, (uint8_t*)&Endd_setting, sizeof(endd_setting_t));
			memcpy(Endd_setting.network_skey, payload, msg_len);
			eeprom_write(ENDD_SETTING_ADDR, (uint8_t*)&Endd_setting, sizeof(endd_setting_t));
			//APP_DBG("app_skey:");
			//APP_DBG_HEX(Endd_setting.application_skey, msg_len);

			Return_code = CMD_RETURN_OK;
		}
		tx_frame_post((cmd_t*)&Send_cmd, sizeof(Return_code));
	}
		break;

	case ENDDEVICE_SET_APP_SKEY: {
		Send_cmd.cmd_id = CMD_ENDD_SET_APP_SKEY;
		uint8_t msg_len = get_data_len_common_msg(msg);
		uint8_t* payload = get_data_common_msg(msg);
		if ( msg_len == 16 ) {
			eeprom_read(ENDD_SETTING_ADDR, (uint8_t*)&Endd_setting, sizeof(endd_setting_t));
			memcpy(Endd_setting.application_skey, payload, msg_len);
			eeprom_write(ENDD_SETTING_ADDR, (uint8_t*)&Endd_setting, sizeof(endd_setting_t));
			//APP_DBG("nwk_skey:");
			//APP_DBG_HEX(Endd_setting.network_skey, msg_len);

			Return_code = CMD_RETURN_OK;
		}
		tx_frame_post((cmd_t*)&Send_cmd, sizeof(Return_code));
	}
		break;

	case ENDDEVICE_SET_CLASS: {
		Send_cmd.cmd_id = CMD_ENDD_SET_CLASS;
		if ( get_data_len_common_msg(msg) == sizeof(uint8_t) ) {
			uint8_t* ref_data = get_data_common_msg(msg);
			MibReq.Type = MIB_DEVICE_CLASS;
			MibReq.Param.Class = (DeviceClass_t)ref_data[0];
			if (LORAMAC_STATUS_OK == LoRaMacMibSetRequestConfirm( &MibReq ) ) {
				//APP_DBG("Class: %d\n", ref_data[0]);

				Return_code = CMD_RETURN_OK;
			}

		}
		tx_frame_post((cmd_t*)&Send_cmd, sizeof(Return_code));
	}
		break;

	case ENDDEVICE_SET_CHAN_MASK: {
		Send_cmd.cmd_id = CMD_ENDD_SET_CHAN_MASK;
		if ( get_data_len_common_msg(msg) == sizeof(uint16_t) ) {
			uint16_t* chan_mask = (uint16_t*)get_data_common_msg(msg);
			if (endd_set_chanmask(chan_mask)) {
				//APP_DBG("Mask: x%04x\n", *chan_mask);

				Return_code = CMD_RETURN_OK;
			}
		}
		tx_frame_post((cmd_t*)&Send_cmd, sizeof(Return_code));
	}
		break;

	case ENDDEVICE_SET_DATARATE: {
		Send_cmd.cmd_id = CMD_ENDD_SET_DATARATE;
		if ( get_data_len_common_msg(msg) == sizeof(uint8_t) ) {
			uint8_t* ref_data = (uint8_t*)get_data_common_msg(msg);
			MibReq.Type = MIB_CHANNELS_DEFAULT_DATARATE;
			MibReq.Param.ChannelsDefaultDatarate = *(ref_data);
			if (LORAMAC_STATUS_OK == LoRaMacMibSetRequestConfirm( &MibReq ) ) {
				MibReq.Type = MIB_CHANNELS_DATARATE;
				MibReq.Param.ChannelsDatarate = *(ref_data);
				if (LORAMAC_STATUS_OK == LoRaMacMibSetRequestConfirm( &MibReq ) ) {
					//APP_DBG("Datarate: %d\n", *(ref_data));

					Return_code = CMD_RETURN_OK;
				}
			}
		}
		tx_frame_post((cmd_t*)&Send_cmd, sizeof(Return_code));
	}
		break;

	case ENDDEVICE_SET_RX2_CHAN: {
		Send_cmd.cmd_id = CMD_ENDD_SET_RX2_CHAN;
		if ( get_data_len_common_msg(msg) == sizeof(uint32_t) ) {
			uint32_t* freq = (uint32_t*)get_data_common_msg(msg);
			if (endd_set_rx2_freq(freq)) {
				//APP_DBG("Rx2_Chan: %d\n", *freq);

				Return_code = CMD_RETURN_OK;
			}
		}
		tx_frame_post((cmd_t*)&Send_cmd, sizeof(Return_code));
	}
		break;

	case ENDDEVICE_SET_RX2_DR: {
		Send_cmd.cmd_id = CMD_ENDD_SET_RX2_DR;
		if ( get_data_len_common_msg(msg) == sizeof(uint8_t) ) {
			uint8_t* dr = (uint8_t*)get_data_common_msg(msg);
			if (endd_set_rx2_dr(dr)) {
				//APP_DBG("Rx2_Dr: %d\n", *dr);

				Return_code = CMD_RETURN_OK;
			}
		}
		tx_frame_post((cmd_t*)&Send_cmd, sizeof(Return_code));
	}
		break;

	case ENDDEVICE_SET_TX_PWR: {
		Send_cmd.cmd_id = CMD_ENDD_SET_TX_PWR;
		if ( get_data_len_common_msg(msg) == sizeof(uint8_t) ) {
			uint8_t* ref_data = (uint8_t*)get_data_common_msg(msg);
			MibReq.Type = MIB_CHANNELS_DEFAULT_TX_POWER;
			MibReq.Param.ChannelsDefaultTxPower = *ref_data;
			if (LORAMAC_STATUS_OK == LoRaMacMibSetRequestConfirm( &MibReq ) ) {
				MibReq.Type = MIB_CHANNELS_TX_POWER;
				MibReq.Param.ChannelsTxPower = *ref_data;
				if (LORAMAC_STATUS_OK == LoRaMacMibSetRequestConfirm( &MibReq ) ) {
					//APP_DBG("TxPower: %d\n", *(ref_data));

					Return_code = CMD_RETURN_OK;
				}
			}
		}
		tx_frame_post((cmd_t*)&Send_cmd, sizeof(Return_code));
	}
		break;

	case ENDDEVICE_SET_ANT_GAIN: {
		Send_cmd.cmd_id = CMD_ENDD_SET_ANT_GAIN;
		if ( get_data_len_common_msg(msg) == sizeof(uint8_t) ) {
			uint8_t* ref_data = (uint8_t*)get_data_common_msg(msg);
			MibReq.Type = MIB_ANTENNA_GAIN;
			MibReq.Param.AntennaGain = (float)*ref_data;
			if (LORAMAC_STATUS_OK == LoRaMacMibSetRequestConfirm( &MibReq ) ) {
				//APP_DBG("AntennaGain: %d\n", *(ref_data));

				Return_code = CMD_RETURN_OK;
			}
		}
		tx_frame_post((cmd_t*)&Send_cmd, sizeof(Return_code));
	}
		break;

		/*******************************************************/

	case ENDDEVICE_GET_DEV_EUI: {
		Send_cmd.cmd_id = CMD_ENDD_GET_DEV_EUI;
		if ( get_data_len_common_msg(msg) == 0 ) {
			eeprom_read(ENDD_SETTING_ADDR, (uint8_t*)&Endd_setting, sizeof(endd_setting_t));
			Send_cmd.cmd_data = (uint8_t*)Endd_setting.device_Eui;
			tx_frame_post((cmd_t*)&Send_cmd, sizeof(Endd_setting.device_Eui));
		}
		else {
			tx_frame_post((cmd_t*)&Send_cmd, sizeof(Return_code));
		}
	}
		break;

	case ENDDEVICE_GET_APP_EUI: {
		Send_cmd.cmd_id = CMD_ENDD_GET_APP_EUI;
		if ( get_data_len_common_msg(msg) == 0 ) {
			eeprom_read(ENDD_SETTING_ADDR, (uint8_t*)&Endd_setting, sizeof(endd_setting_t));
			Send_cmd.cmd_data = (uint8_t*)Endd_setting.application_Eui;
			tx_frame_post((cmd_t*)&Send_cmd, sizeof(Endd_setting.application_Eui));
		}
		else {
			tx_frame_post((cmd_t*)&Send_cmd, sizeof(Return_code));
		}
	}
		break;

	case ENDDEVICE_GET_APP_KEY: {
		Send_cmd.cmd_id = CMD_ENDD_GET_APP_KEY;
		if ( get_data_len_common_msg(msg) == 0 ) {
			eeprom_read(ENDD_SETTING_ADDR, (uint8_t*)&Endd_setting, sizeof(endd_setting_t));
			Send_cmd.cmd_data = (uint8_t*)Endd_setting.application_key;
			tx_frame_post((cmd_t*)&Send_cmd, sizeof(Endd_setting.application_key));
		}
		else {
			tx_frame_post((cmd_t*)&Send_cmd, sizeof(Return_code));
		}
	}
		break;

	case ENDDEVICE_GET_NET_ID: {
		Send_cmd.cmd_id = CMD_ENDD_GET_NET_ID;
		if ( get_data_len_common_msg(msg) == 0 ) {
			eeprom_read(ENDD_SETTING_ADDR, (uint8_t*)&Endd_setting, sizeof(endd_setting_t));
			Send_cmd.cmd_data = (uint8_t*)&Endd_setting.network_id;
			tx_frame_post((cmd_t*)&Send_cmd, 3);
		}
		else {
			tx_frame_post((cmd_t*)&Send_cmd, sizeof(Return_code));
		}
	}
		break;

	case ENDDEVICE_GET_DEV_ADDR: {
		Send_cmd.cmd_id = CMD_ENDD_GET_DEV_ADDR;
		if ( get_data_len_common_msg(msg) == 0 ) {
			eeprom_read(ENDD_SETTING_ADDR, (uint8_t*)&Endd_setting, sizeof(endd_setting_t));
			Send_cmd.cmd_data = (uint8_t*)&Endd_setting.device_address;
			tx_frame_post((cmd_t*)&Send_cmd, sizeof(Endd_setting.device_address));
		}
		else {
			tx_frame_post((cmd_t*)&Send_cmd, sizeof(Return_code));
		}
	}
		break;

	case ENDDEVICE_GET_NKW_SKEY: {
		Send_cmd.cmd_id = CMD_ENDD_GET_NKW_SKEY;
		if ( get_data_len_common_msg(msg) == 0 ) {
			eeprom_read(ENDD_SETTING_ADDR, (uint8_t*)&Endd_setting, sizeof(endd_setting_t));
			Send_cmd.cmd_data = (uint8_t*)Endd_setting.network_skey;
			tx_frame_post((cmd_t*)&Send_cmd, sizeof(Endd_setting.network_skey));
		}
		else {
			tx_frame_post((cmd_t*)&Send_cmd, sizeof(Return_code));
		}
	}
		break;

	case ENDDEVICE_GET_APP_SKEY: {
		Send_cmd.cmd_id = CMD_ENDD_GET_APP_SKEY;
		if ( get_data_len_common_msg(msg) == 0 ) {
			eeprom_read(ENDD_SETTING_ADDR, (uint8_t*)&Endd_setting, sizeof(endd_setting_t));
			Send_cmd.cmd_data = (uint8_t*)Endd_setting.application_skey;
			tx_frame_post((cmd_t*)&Send_cmd, sizeof(Endd_setting.application_skey));
		}
		else {
			tx_frame_post((cmd_t*)&Send_cmd, sizeof(Return_code));
		}
	}
		break;

	case ENDDEVICE_GET_CLASS: {
		Send_cmd.cmd_id = CMD_ENDD_GET_CLASS;
		if ( get_data_len_common_msg(msg) == 0 ) {
			MibReq.Type = MIB_DEVICE_CLASS;
			LoRaMacMibGetRequestConfirm( &MibReq );
			Send_cmd.cmd_data = (uint8_t*)&MibReq.Param.Class;
			tx_frame_post((cmd_t*)&Send_cmd, sizeof(uint8_t));
		}
		else {
			tx_frame_post((cmd_t*)&Send_cmd, sizeof(Return_code));
		}
	}
		break;

	case ENDDEVICE_GET_CHAN_MASK: {
		Send_cmd.cmd_id = CMD_ENDD_GET_CHAN_MASK;
		if ( get_data_len_common_msg(msg) == 0 ) {
			uint16_t* chanmask = endd_get_chanmask();
			Send_cmd.cmd_data = (uint8_t*)chanmask;
			tx_frame_post((cmd_t*)&Send_cmd, sizeof(uint16_t));
		}
		else {
			tx_frame_post((cmd_t*)&Send_cmd, sizeof(Return_code));
		}
	}
		break;

	case ENDDEVICE_GET_DATARATE: {
		Send_cmd.cmd_id = CMD_ENDD_GET_DATARATE;
		if ( get_data_len_common_msg(msg) == 0 ) {
			MibReq.Type = MIB_CHANNELS_DATARATE;
			LoRaMacMibGetRequestConfirm( &MibReq );
			Send_cmd.cmd_data = (uint8_t*)&MibReq.Param.ChannelsDatarate;
			tx_frame_post((cmd_t*)&Send_cmd, sizeof(uint8_t));
		}
		else {
			tx_frame_post((cmd_t*)&Send_cmd, sizeof(Return_code));
		}
	}
		break;

	case ENDDEVICE_GET_RX2_CHAN: {
		Send_cmd.cmd_id = CMD_ENDD_GET_RX2_CHAN;
		if ( get_data_len_common_msg(msg) == 0 ) {
			uint32_t* freq = endd_get_rx2_freq();
			Send_cmd.cmd_data = (uint8_t*)freq;
			tx_frame_post((cmd_t*)&Send_cmd, sizeof(uint32_t));
		}
		else {
			tx_frame_post((cmd_t*)&Send_cmd, sizeof(Return_code));
		}
	}
		break;

	case ENDDEVICE_GET_RX2_DR: {
		Send_cmd.cmd_id = CMD_ENDD_GET_RX2_DR;
		if ( get_data_len_common_msg(msg) == 0 ) {
			uint8_t* dr = endd_get_rx2_dr();
			Send_cmd.cmd_data = (uint8_t*)dr;
			tx_frame_post((cmd_t*)&Send_cmd, sizeof(uint8_t));
		}
		else {
			tx_frame_post((cmd_t*)&Send_cmd, sizeof(Return_code));
		}
	}
		break;

	case ENDDEVICE_GET_TX_PWR: {
		Send_cmd.cmd_id = CMD_ENDD_GET_TX_PWR;
		if ( get_data_len_common_msg(msg) == 0 ) {
			MibReq.Type = MIB_CHANNELS_TX_POWER;
			LoRaMacMibGetRequestConfirm( &MibReq );
			Send_cmd.cmd_data = (uint8_t*)&MibReq.Param.ChannelsTxPower;
			tx_frame_post((cmd_t*)&Send_cmd, sizeof(uint8_t));
		}
		else {
			tx_frame_post((cmd_t*)&Send_cmd, sizeof(Return_code));
		}
	}
		break;

	case ENDDEVICE_GET_ANT_GAIN: {
		Send_cmd.cmd_id = CMD_ENDD_GET_ANT_GAIN;
		if ( get_data_len_common_msg(msg) == 0 ) {
			MibReq.Type = MIB_ANTENNA_GAIN;
			LoRaMacMibGetRequestConfirm( &MibReq );
			Send_cmd.cmd_data[0] = (uint8_t)MibReq.Param.AntennaGain;
			tx_frame_post((cmd_t*)&Send_cmd, sizeof(uint8_t));
		}
		else {
			tx_frame_post((cmd_t*)&Send_cmd, sizeof(Return_code));
		}
	}
		break;

	case ENDDEVICE_SEND_UNCONFIRM_MSG: {
		Send_cmd.cmd_id = CMD_ENDD_SEND_UNCONFIRM_MSG;
		uint8_t msg_len = get_data_len_common_msg(msg);
		endd_send_unconfirm_t* smsg = (endd_send_unconfirm_t*)get_data_common_msg(msg);
		if (msg_len >= sizeof(smsg->header)) {
			if (endd_send_unconfirm_msg(smsg->header.port, smsg->buffer, msg_len - sizeof(smsg->header))) {
				Return_code = CMD_RETURN_OK;
			}
		}
		tx_frame_post((cmd_t*)&Send_cmd, sizeof(Return_code));
	}
		break;

	case ENDDEVICE_SEND_CONFIRM_MSG: {
		Send_cmd.cmd_id = CMD_ENDD_SEND_CONFIRM_MSG;
		uint8_t msg_len = get_data_len_common_msg(msg);
		endd_send_confirm_t* smsg = (endd_send_confirm_t*)get_data_common_msg(msg);
		if (msg_len >= sizeof(smsg->header)) {
			if (endd_send_confirm_msg(smsg->header.port, smsg->header.num_retries, smsg->buffer, msg_len - sizeof(smsg->header))) {
				Return_code = CMD_RETURN_OK;
			}
		}
		tx_frame_post((cmd_t*)&Send_cmd, sizeof(Return_code));

	}
		break;

	default:
		break;
	}
}
