#ifndef __LORAWAN_FW_H__
#define __LORAWAN_FW_H__

#include "port.h"

typedef struct {
	uint32_t tx_frequency;
	uint8_t tx_datarater;
	uint8_t tx_power;
} __AK_PACKETED fw_tx_config_t;

typedef struct {
	uint32_t rx_frequency;
	uint8_t rx_datarater;
} __AK_PACKETED fw_rx_config_t;

extern void fw_init();
extern void fw_set_tx_config(const fw_tx_config_t* tx_config);
extern void fw_set_rx_config(const fw_rx_config_t* rx_config);
extern fw_tx_config_t* fw_get_tx_config();
extern fw_rx_config_t* fw_get_rx_config();
extern bool fw_send(uint8_t* buffer, uint8_t length);

#endif // __LORAWAN_FW_H__
