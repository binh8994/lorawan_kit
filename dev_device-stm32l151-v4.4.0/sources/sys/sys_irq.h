#ifndef __SYS_IRQ_H__
#define __SYS_IRQ_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>
#include "port.h"

#define SYS_IRQ_EXCEPTION_NUMBER_IRQ0_NUMBER_RESPECTIVE		16	/* exception number 16 ~~ IRQ0 */

extern void sys_irq_nrf24l01() __AK_WEAK;
extern void sys_irq_shell();
extern void sys_irq_ir_io_rev();
extern void sys_irq_timer_50us();
extern void sys_irq_timer_10ms();
extern void sys_irq_timer_hs1101();
extern void sys_irq_usb_recv(uint8_t* data, uint32_t len);
extern void sys_irq_sx1276_dio_0();
extern void sys_irq_sx1276_dio_1();
extern void sys_irq_sx1276_dio_2();
extern void sys_irq_sx1276_dio_3();
extern void sys_irq_sx1276_dio_4();
extern void sys_irq_sx1276_dio_5();
extern void sys_irq_cmd_uart();

#ifdef __cplusplus
}
#endif

#endif // __SYS_IRQ_H__
