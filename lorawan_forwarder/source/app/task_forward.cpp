#include <stdint.h>         /* C99 types */
#include <stdbool.h>        /* bool type */
#include <stdio.h>          /* printf, fprintf, snprintf, fopen, fputs */
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/signal.h>
#include <fcntl.h>
#include <termios.h>
#include <errno.h>
#include <signal.h>         /* sigaction */
#include <time.h> 
#include <unistd.h>         /* getopt, access */
#include <errno.h>          /* error messages */
#include <math.h>           /* modf */
#include <assert.h>
#include <termios.h>
#include <netdb.h>          /* gai_strerror */

#include <sys/socket.h>     /* socket specific definitions */
#include <netinet/in.h>     /* INET constants and stuff */
#include <arpa/inet.h>      /* IP address conversion stuff */
#include <netdb.h>          /* gai_strerror */
#include <cstring>
#include <sys/ioctl.h>
#include <net/if.h>

#include "ak.h"
#include "timer.h"

#include "app.h"
#include "app_if.h"
#include "app_dbg.h"
#include "app_data.h"
#include "base64.h"
#include "json.hpp"

#include "task_list.h"
#include "task_forward.h"

using json = nlohmann::json;

#define CMD_UART_FRAME_TO_INTERVAL		500
#define CMD_UART_SOP_CHAR				0xEF
#define CMD_UART_DEVPATH				"/dev/ttyUSB2"
#define CMD_UART_DATA_SIZE				254

static struct cmd_uart_frame_t {
	uint8_t		frame_sop;
	uint32_t	len;
	uint8_t		data_index;
	uint8_t		data[CMD_UART_DATA_SIZE];
	uint8_t		frame_fcs;
} cmd_uart_frame;

#define CMD_RETURN_OK					(1)
#define CMD_RETURN_ERR					(0)

enum {
	MODE_ENDDEVICE,
	MODE_FORWARD,
	MODE_SERVER
};

enum {
	CMD_SET_MODE,

	CMD_ENDD_SET_ACTIVE_MODE,
	CMD_ENDD_OTA_STATUS,
	CMD_ENDD_SET_DEV_EUI ,
	CMD_ENDD_SET_APP_EUI,
	CMD_ENDD_SET_APP_KEY,
	CMD_ENDD_SET_NET_ID,
	CMD_ENDD_SET_DEV_ADDR,
	CMD_ENDD_SET_NKW_SKEY,
	CMD_ENDD_SET_APP_SKEY,
	CMD_ENDD_SET_CLASS,
	CMD_ENDD_SET_CHAN_MASK,
	CMD_ENDD_SET_DATARATE,
	CMD_ENDD_SET_RX2_CHAN,
	CMD_ENDD_SET_RX2_DR,
	CMD_ENDD_SET_TX_PWR,
	CMD_ENDD_SET_ANT_GAIN,
	CMD_ENDD_GET_DEV_EUI,
	CMD_ENDD_GET_APP_EUI,
	CMD_ENDD_GET_APP_KEY,
	CMD_ENDD_GET_NET_ID,
	CMD_ENDD_GET_DEV_ADDR,
	CMD_ENDD_GET_NKW_SKEY,
	CMD_ENDD_GET_APP_SKEY,
	CMD_ENDD_GET_CLASS,
	CMD_ENDD_GET_CHAN_MASK,
	CMD_ENDD_GET_DATARATE,
	CMD_ENDD_GET_RX2_CHAN,
	CMD_ENDD_GET_RX2_DR,
	CMD_ENDD_GET_TX_PWR,
	CMD_ENDD_GET_ANT_GAIN,
	CMD_ENDD_SEND_UNCONFIRM_MSG,
	CMD_ENDD_SEND_CONFIRM_MSG,
	CMD_ENDD_RECV_CONFIRM_MSG,
	CMD_ENDD_RECV_UNCONFIRM_MSG,

	CMD_FW_SET_TX_CONFIG,
	CMD_FW_GET_TX_CONFIG,
	CMD_FW_SET_RX_CONFIG,
	CMD_FW_GET_RX_CONFIG,
	CMD_FW_SEND,
	CMD_FW_RECV,

	CMD_SERV_SET_TX_CONFIG,
	CMD_SERV_GET_TX_CONFIG,
	CMD_SERV_SET_RX_CONFIG,
	CMD_SERV_GET_RX_CONFIG,
	CMD_SERV_SET_NET_PARAM,
	CMD_SERV_GET_NET_PARAM,
	CMD_SERV_SET_JOINT_PERMIT,
	CMD_SERV_GET_JOINT_PERMIT,
	CMD_SERV_RESP_JOINT_PERMIT,
	CMD_SERV_REMOVE_DEVICE,
	CMD_SERV_REMOVE_DEVICE_ALL,
	CMD_SERV_SEND_UNCONFIRM_MSG,
	CMD_SERV_SEND_CONFIRM_MSG,
	CMD_SERV_RECV_UNCONFIRM_MSG,
	CMD_SERV_RECV_CONFIRM_MSG,
	CMD_SERV_RECV_CONFIRM,
};

typedef struct {
	uint8_t des_task_id;
	uint8_t des_sig;
} cmd_uart_tbl_t;

static cmd_uart_tbl_t Cmd_uart_tbl[] = \
{
	{GW_TASK_FORWARD_ID, FORWARD_SET_TX_CONFIG		},	//CMD_SET_MODE,
	//
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	//
	{GW_TASK_FORWARD_ID, FORWARD_SET_RX_CONFIG	},	//CMD_FW_SET_TX_CONFIG
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{GW_TASK_FORWARD_ID, FORWARD_RECV_MSG		},	//CMD_FW_RECV
	//
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
};

typedef struct {
	uint8_t cmd_id;
	uint8_t* cmd_data;
} cmd_t;

#define UART_TX_BUFFER				256
#define GW_FREQ						433175000
#define GW_BANDWIDTH				0
// 0: 125 kHz
// 1: 250 kHz
// 2: 500 kHz
#define GW_DR						0
#define GW_TX_POWER					20

static fw_tx_config_t Fw_tx_config  = { GW_FREQ, GW_DR, GW_TX_POWER};
static fw_rx_config_t Fw_rx_config = { GW_FREQ, GW_DR};

#define SOP_STATE						0x00
#define LEN_STATE						0x01
#define DATA_STATE						0x02
#define FCS_STATE						0x03

static uint8_t rx_frame_state = SOP_STATE;
static uint8_t tx_buffer[UART_TX_BUFFER];

static int cmd_uart_fd;

#define INTERFACE "enp3s0"			//interface to get MAC
#define SERVER1 "router.as2.thethings.network"
//#define SERVER1 "118.69.167.36"
#define PORT "1700"                   // The port on which to send data
#define DEFAULT_KEEPALIVE	5	/* default time interval for downstream keep-alive packet */

struct sockaddr_in si_other;
int sock_up, sock_down;
struct ifreq ifr;
struct addrinfo hints;
struct addrinfo *result; /* store result of getaddrinfo */
struct addrinfo *q; /* pointer to move into *result data */
char serv_addr[] = SERVER1;
char serv_port_up[] = PORT;
char serv_port_down[] = PORT;
static int keepalive_time = DEFAULT_KEEPALIVE; /* send a PULL_DATA request every X seconds, negative = disabled */

char host_name[64];
char port_name[64];

#define TIMER_STAT			30000
#define PUSH_TIMEOUT_MS		200
#define PULL_TIMEOUT_MS		400
static struct timeval push_timeout_half = {0, (PUSH_TIMEOUT_MS * 500)}; /* cut in half, critical for throughput */
static struct timeval pull_timeout = {0, (PULL_TIMEOUT_MS * 1000)}; /* non critical for throughput */

static uint32_t cp_rxok = 0;
static uint32_t cp_rxfw = 0;
static uint32_t cp_dwnb = 0;
static uint32_t cp_txnb = 0;

// Set location
float lat=0.0;
float lon=0.0;
int   alt=0;

/* Informal status fields */
static char platform[]    = "Gateway";  /* platform definition */
static char email[]       = "";                        /* used for contact email */
static char description[] = "";                        /* used for free form description */

#define PROTOCOL_VERSION		1
#define PKT_PUSH_DATA			0
#define PKT_PUSH_ACK			1
#define PKT_PULL_DATA			2
#define PKT_PULL_RESP			3
#define PKT_PULL_ACK			4
#define BUFFUP_SIZE				2048
#define BUFFSTT_SIZE			1024

static uint8_t buff_ack[32];
static char bufup[BUFFUP_SIZE];
static char bufstt[BUFFSTT_SIZE];

static int cmd_uart_opentty(const char* devpath);
static uint8_t cmd_uart_calcfcs(uint8_t len, uint8_t *data_ptr);
static void rx_frame_parser(uint8_t* data, uint8_t len);
static int cmd_uart_tx_frame_post(cmd_t* data, uint8_t data_len);

static double difftimespec(struct timespec end, struct timespec beginning);

static pthread_t cmd_uart_rx_thread;
static pthread_t thrid_down;

static void* cmd_uart_rx_thread_handler(void*);
static void* thread_down(void*);

q_msg_t gw_task_forward_mailbox;

void* gw_task_forward_entry(void*) {
	wait_all_tasks_started();

	APP_DBG("[STARTED] gw_task_forward_entry\n");

	ak_msg_t* msg = AK_MSG_NULL;

	if (cmd_uart_opentty(CMD_UART_DEVPATH) < 0) {
		APP_DBG("Cannot open %s !\n", CMD_UART_DEVPATH);
	}
	else {
		APP_DBG("Opened %s success !\n", CMD_UART_DEVPATH);\
		pthread_create(&cmd_uart_rx_thread, NULL, cmd_uart_rx_thread_handler, NULL);

		/* prepare hints to open network sockets */
		memset(&hints, 0, sizeof hints);
		hints.ai_family = AF_INET; /* WA: Forcing IPv4 as AF_UNSPEC makes connection on localhost to fail */
		hints.ai_socktype = SOCK_DGRAM;

		/* look for server address w/ upstream port */
		int i = getaddrinfo(serv_addr, serv_port_up, &hints, &result);
		if (i != 0) {
			APP_DBG("[up] getaddrinfo on address %s (PORT %s)\n", serv_addr, serv_port_up);
			FATAL("NET", 0x02);
		}

		/* try to open socket for upstream traffic */
		for (q=result; q!=NULL; q=q->ai_next) {
			sock_up = socket(q->ai_family, q->ai_socktype,q->ai_protocol);
			if (sock_up == -1) continue; /* try next field */
			else break; /* success, get out of loop */
		}
		if (q == NULL) {
			APP_DBG("[up] failed to open socket to any of server %s addresses (port %s)\n", serv_addr, serv_port_up);
			i = 1;
			for (q=result; q!=NULL; q=q->ai_next) {
				getnameinfo(q->ai_addr, q->ai_addrlen, host_name, sizeof host_name, port_name, sizeof port_name, NI_NUMERICHOST);
				APP_DBG("[up] result %i host:%s service:%s\n", i, host_name, port_name);
				++i;
			}
			FATAL("NET", 0x03);
		}

		/* connect so we can send/receive packet with the server only */
		i = connect(sock_up, q->ai_addr, q->ai_addrlen);
		if (i != 0) {
			APP_DBG("[up] connect returned %s\n", strerror(errno));
			FATAL("NET", 0x04);
		}
		freeaddrinfo(result);

		/* look for server address w/ downstream port */
		i = getaddrinfo(serv_addr, serv_port_down, &hints, &result);
		if (i != 0) {
			APP_DBG("[down] getaddrinfo on address %s (port %s) returned %s\n", serv_addr, serv_port_down, gai_strerror(i));
			FATAL("NET", 0x05);
		}

		/* try to open socket for downstream traffic */
		for (q=result; q!=NULL; q=q->ai_next) {
			sock_down = socket(q->ai_family, q->ai_socktype,q->ai_protocol);
			if (sock_down == -1) continue; /* try next field */
			else break; /* success, get out of loop */
		}
		if (q == NULL) {
			APP_DBG("[down] failed to open socket to any of server %s addresses (port %s)\n", serv_addr, serv_port_down);
			i = 1;
			for (q=result; q!=NULL; q=q->ai_next) {
				getnameinfo(q->ai_addr, q->ai_addrlen, host_name, sizeof host_name, port_name, sizeof port_name, NI_NUMERICHOST);
				APP_DBG("[down] result %i host:%s service:%s\n", i, host_name, port_name);
				++i;
			}
			FATAL("NET", 0x06);
		}

		/* connect so we can send/receive packet with the server only */
		i = connect(sock_down, q->ai_addr, q->ai_addrlen);
		if (i != 0) {
			APP_DBG("[down] connect returned %s\n", strerror(errno));
			FATAL("NET", 0x07);
		}
		freeaddrinfo(result);

		i = setsockopt(sock_up, SOL_SOCKET, SO_RCVTIMEO, (void *)&push_timeout_half, sizeof push_timeout_half);
		if (i != 0) {
			APP_DBG("[up] setsockopt returned %s\n", strerror(errno));
			FATAL("NET", 0x08);
		}

		ifr.ifr_addr.sa_family = AF_INET;
		strncpy(ifr.ifr_name, INTERFACE, IFNAMSIZ-1);
		ioctl(sock_up, SIOCGIFHWADDR, &ifr);

		APP_DBG("Gateway ID: %.2x:%.2x:%.2x:ff:ff:%.2x:%.2x:%.2x\n",
				(unsigned char)ifr.ifr_hwaddr.sa_data[0],	(unsigned char)ifr.ifr_hwaddr.sa_data[1],
				(unsigned char)ifr.ifr_hwaddr.sa_data[2],	(unsigned char)ifr.ifr_hwaddr.sa_data[3],
				(unsigned char)ifr.ifr_hwaddr.sa_data[4],	(unsigned char)ifr.ifr_hwaddr.sa_data[5]);

		i = pthread_create( &thrid_down, NULL, thread_down, NULL);
		if (i != 0) {
			APP_DBG("[down] impossible to create downstream thread\n");
			FATAL("NET", 0x09);
		}

		task_post_pure_msg(GW_TASK_FORWARD_ID, FORWARD_SEND_STAT);
	}

	while (1) {

		/* get messge */
		msg = ak_msg_rev(GW_TASK_FORWARD_ID);

		switch (msg->header->sig) {
		case FORWRAD_UART_TO: {
			APP_DBG_SIG("SERVER_UART_TO\n");
			rx_frame_state = SOP_STATE;
		}
			break;
		case FORWARD_INIT: {
			APP_DBG_SIG("FORWARD_INIT\n");

			uint8_t mode = MODE_FORWARD;
			cmd_t send_fw_init;
			send_fw_init.cmd_id =  CMD_SET_MODE;
			send_fw_init.cmd_data = &mode;
			cmd_uart_tx_frame_post(&send_fw_init, sizeof(mode));
		}
			break;

		case FORWARD_SET_TX_CONFIG: {
			APP_DBG_SIG("FORWARD_SET_TX_CONFIG\n");

			cmd_t send_tx_cfg;
			send_tx_cfg.cmd_id =  CMD_FW_SET_TX_CONFIG;
			send_tx_cfg.cmd_data = (uint8_t*)&Fw_tx_config;
			cmd_uart_tx_frame_post(&send_tx_cfg, sizeof(fw_tx_config_t));
		}
			break;

		case FORWARD_SET_RX_CONFIG: {
			APP_DBG_SIG("FORWARD_SET_RX_CONFIG\n");

			cmd_t send_rx_cfg;
			send_rx_cfg.cmd_id =  CMD_FW_SET_RX_CONFIG;
			send_rx_cfg.cmd_data = (uint8_t*)&Fw_rx_config;
			cmd_uart_tx_frame_post(&send_rx_cfg, sizeof(fw_rx_config_t));

		}
			break;

		case FORWARD_RECV_MSG: {
			APP_DBG_SIG("FORWARD_RECV_MSG\n");
			uint8_t* raw_buff = (uint8_t*)msg->header->payload;
			int raw_len = get_data_len_dynamic_msg(msg);
			int buff_index=0, j;

			cp_rxok++;
			cp_rxfw++;

			/* pre-fill the data buffer with fixed fields */
			bufup[0] = PROTOCOL_VERSION;
			bufup[3] = PKT_PUSH_DATA;

			bufup[4] = (unsigned char)ifr.ifr_hwaddr.sa_data[0];
			bufup[5] = (unsigned char)ifr.ifr_hwaddr.sa_data[1];
			bufup[6] = (unsigned char)ifr.ifr_hwaddr.sa_data[2];
			bufup[7] = 0xFF;
			bufup[8] = 0xFF;
			bufup[9] = (unsigned char)ifr.ifr_hwaddr.sa_data[3];
			bufup[10] = (unsigned char)ifr.ifr_hwaddr.sa_data[4];
			bufup[11] = (unsigned char)ifr.ifr_hwaddr.sa_data[5];

			/* start composing datagram with the header */
			uint8_t token_h = (uint8_t)rand(); /* random token */
			uint8_t token_l = (uint8_t)rand(); /* random token */
			bufup[1] = token_h;
			bufup[2] = token_l;
			buff_index = 12; /* 12-byte header */

			// TODO: tmst can jump is time is (re)set, not good.
			struct timeval now;
			gettimeofday(&now, NULL);
			uint32_t tmst = (uint32_t)(now.tv_sec*1000000 + now.tv_usec);

			/* start of JSON structure */
			memcpy((void *)(bufup + buff_index), (void *)"{\"rxpk\":[", 9);
			buff_index += 9;
			bufup[buff_index] = '{';
			++buff_index;
			j = snprintf((char *)(bufup + buff_index), BUFFUP_SIZE-buff_index, "\"tmst\":%u", tmst);
			buff_index += j;
			j = snprintf((char *)(bufup + buff_index), BUFFUP_SIZE-buff_index, ",\"chan\":%1u,\"rfch\":%1u,\"freq\":%.6lf", 0, 0, (double)GW_FREQ/1000000.0f);
			buff_index += j;
			memcpy((void *)(bufup + buff_index), (void *)",\"stat\":1", 9);
			buff_index += 9;
			memcpy((void *)(bufup + buff_index), (void *)",\"modu\":\"LORA\"", 14);
			buff_index += 14;
			/* Lora datarate & bandwidth, 16-19 useful chars */
			switch (GW_DR) {
			case 5:
				memcpy((void *)(bufup + buff_index), (void *)",\"datr\":\"SF7", 12);
				buff_index += 12;
				break;
			case 4:
				memcpy((void *)(bufup + buff_index), (void *)",\"datr\":\"SF8", 12);
				buff_index += 12;
				break;
			case 3:
				memcpy((void *)(bufup + buff_index), (void *)",\"datr\":\"SF9", 12);
				buff_index += 12;
				break;
			case 2:
				memcpy((void *)(bufup + buff_index), (void *)",\"datr\":\"SF10", 13);
				buff_index += 13;
				break;
			case 1:
				memcpy((void *)(bufup + buff_index), (void *)",\"datr\":\"SF11", 13);
				buff_index += 13;
				break;
			case 0:
				memcpy((void *)(bufup + buff_index), (void *)",\"datr\":\"SF12", 13);
				buff_index += 13;
				break;
			default:
				memcpy((void *)(bufup + buff_index), (void *)",\"datr\":\"SF?", 12);
				buff_index += 12;
			}
			memcpy((void *)(bufup + buff_index), (void *)"BW125\"", 6);
			buff_index += 6;
			memcpy((void *)(bufup + buff_index), (void *)",\"codr\":\"4/5\"", 13);
			buff_index += 13;
			j = snprintf((char *)(bufup + buff_index), BUFFUP_SIZE-buff_index, ",\"lsnr\":%li", 9ul);
			buff_index += j;
			j = snprintf((char *)(bufup + buff_index), BUFFUP_SIZE-buff_index, ",\"rssi\":%d,\"size\":%u", -50, raw_len);
			buff_index += j;
			memcpy((void *)(bufup + buff_index), (void *)",\"data\":\"", 9);
			buff_index += 9;
			j = bin_to_b64((uint8_t *)raw_buff, raw_len, (char *)(bufup + buff_index), 341);
			buff_index += j;
			bufup[buff_index] = '"';
			++buff_index;

			/* End of packet serialization */
			bufup[buff_index] = '}';
			++buff_index;
			bufup[buff_index] = ']';
			++buff_index;
			/* end of JSON datagram payload */
			bufup[buff_index] = '}';
			++buff_index;
			bufup[buff_index] = 0; /* add string terminator, for safety */

			APP_DBG("%s\n", (char *)(bufup + 12));
			//APP_DBG_MSG(bufup + 12, buff_index);
			//APP_DBG("buff_index:%d\n", buff_index);

			//send the messages
			//inet_aton(SERVER1 , &si_other.sin_addr);
			//if (sendto(sock_up, (char *)msg, buff_index, 0 , (struct sockaddr *) &si_other, sizeof(si_other))==-1) {
			//	APP_DBG("[Err] sendto()");
			//}
			if (send(sock_up, (char *)bufup, buff_index, 0) == -1) {
				APP_DBG("[Err] send()");
			}
			/* wait for acknowledge (in 2 times, to catch extra packets) */
			for (int i=0; i<2; ++i) {
				j = recv(sock_up, (void *)buff_ack, sizeof buff_ack, 0);
				if (j == -1) {
					if (errno == EAGAIN) { /* timeout */
						APP_DBG("timeout: %d\n", i);
						continue;
					} else { /* server connection error */
						break;
					}
				} else if ((j < 4) || (buff_ack[0] != PROTOCOL_VERSION) || (buff_ack[3] != PKT_PUSH_ACK)) {
					APP_DBG("ERR: [up] ignored invalid non-ACL packet\n");
					continue;
				} else if ((buff_ack[1] != token_h) || (buff_ack[2] != token_l)) {
					APP_DBG("ERR: [up] ignored out-of sync ACK packet\n");
					continue;
				} else {
					APP_DBG("INFO: [up] PUSH_ACK received\n");
					break;
				}
			}

		}
			break;

		case FORWARD_SEND_STAT: {
			APP_DBG_SIG("FORWARD_SEND_STAT\n");

			char stat_timestamp[24];
			time_t t;
			int stat_index=0;

			/* pre-fill the data buffer with fixed fields */
			bufstt[0] = PROTOCOL_VERSION;
			bufstt[1] = (uint8_t)rand();
			bufstt[2] = (uint8_t)rand();
			bufstt[3] = PKT_PUSH_DATA;

			bufstt[4] = (unsigned char)ifr.ifr_hwaddr.sa_data[0];
			bufstt[5] = (unsigned char)ifr.ifr_hwaddr.sa_data[1];
			bufstt[6] = (unsigned char)ifr.ifr_hwaddr.sa_data[2];
			bufstt[7] = 0xFF;
			bufstt[8] = 0xFF;
			bufstt[9] = (unsigned char)ifr.ifr_hwaddr.sa_data[3];
			bufstt[10] = (unsigned char)ifr.ifr_hwaddr.sa_data[4];
			bufstt[11] = (unsigned char)ifr.ifr_hwaddr.sa_data[5];

			/* start composing datagram with the header */
			uint8_t token_h = (uint8_t)rand(); /* random token */
			uint8_t token_l = (uint8_t)rand(); /* random token */
			bufstt[1] = token_h;
			bufstt[2] = token_l;

			stat_index += 12; /* 12-byte header */

			/* get timestamp for statistics */
			t = time(NULL);
			strftime(stat_timestamp, sizeof stat_timestamp, "%F %T %Z", gmtime(&t));

			int j = snprintf((char *)(bufstt + stat_index), BUFFSTT_SIZE-stat_index, \
							 "{\"stat\":{\"time\":\"%s\",\"lati\":%.5f,\"long\":%.5f,\"alti\":%i,\"rxok\":%u,\"rxfw\":%u,\"dwnb\":%u,\"txnb\":%u,\"pfrm\":\"%s\",\"mail\":\"%s\",\"desc\":\"%s\"}}", \
							 stat_timestamp, lat, lon, (int)alt, cp_rxok, cp_rxfw, cp_dwnb, cp_txnb, platform,email,description);
			stat_index += j;
			bufstt[stat_index] = 0; /* add string terminator, for safety */

			//printf("stat update: %s\n", (char *)(bufstt+12)); /* DEBUG: display JSON stat */
			APP_DBG_MSG(bufstt+12, stat_index);

			//send the update
			//inet_aton(SERVER1 , &si_other.sin_addr);
			//if (sendto(sock_up, (char *)bufstt, stat_index, 0 , (struct sockaddr *) &si_other, sizeof(si_other))==-1) {
			//	APP_DBG("[Err] sendto()");
			//}
			if (send(sock_up, (char *)bufstt, stat_index, 0) == -1) {
				APP_DBG("[Err] send()");
			}
			/* wait for acknowledge (in 2 times, to catch extra packets) */
			for (int i=0; i<2; ++i) {
				j = recv(sock_up, (void *)buff_ack, sizeof buff_ack, 0);
				if (j == -1) {
					if (errno == EAGAIN) { /* timeout */
						APP_DBG("INFO: [stt] timeout: %d\n", i);
						continue;
					} else { /* server connection error */
						break;
					}
				} else if ((j < 4) || (buff_ack[0] != PROTOCOL_VERSION) || (buff_ack[3] != PKT_PUSH_ACK)) {
					APP_DBG("ERR: [stt] ignored invalid non-ACL packet\n");
					continue;
				} else if ((buff_ack[1] != token_h) || (buff_ack[2] != token_l)) {
					APP_DBG("ERR: [stt] ignored out-of sync ACK packet\n");
					continue;
				} else {
					APP_DBG("INFO: [stt] PUSH_ACK received\n");
					break;
				}
			}
			cp_rxok=0;
			cp_rxfw=0;
			cp_dwnb=0;
			cp_txnb=0;

			timer_set(GW_TASK_FORWARD_ID, FORWARD_SEND_STAT, TIMER_STAT, TIMER_ONE_SHOT);
		}
			break;

		default:
			break;
		}

		/* free message */
		ak_msg_free(msg);
	}

	return (void*)0;
}


void* cmd_uart_rx_thread_handler(void*) {
	APP_DBG("cmd_uart_rx_thread_handler entry successed!\n");
	uint8_t rx_buffer[CMD_UART_DATA_SIZE];
	uint32_t rx_read_len;

	task_post_pure_msg(GW_TASK_FORWARD_ID, FORWARD_INIT);

	while(1) {
		rx_read_len = read(cmd_uart_fd, rx_buffer, CMD_UART_DATA_SIZE);
		if (rx_read_len > 0) {
			rx_frame_parser(rx_buffer, rx_read_len);
		}
		usleep(1);
	}

	return (void*)0;
}

int cmd_uart_opentty(const char* devpath) {
	struct termios options;
	APP_DBG("[cmd_uart_opentty] devpath: %s\n", devpath);

	cmd_uart_fd = open(devpath, O_RDWR | O_NOCTTY | O_NDELAY);
	if (cmd_uart_fd < 0) {
		return cmd_uart_fd;
	}
	else {
		fcntl(cmd_uart_fd, F_SETFL, 0);

		/* get current status */
		tcgetattr(cmd_uart_fd, &options);

		cfsetispeed(&options, B9600);
		cfsetospeed(&options, B9600);

		/* No parity (8N1) */
		options.c_cflag &= ~PARENB;
		options.c_cflag &= ~CSTOPB;
		options.c_cflag &= ~CSIZE;
		options.c_cflag |= CS8;

		options.c_cflag |= (CLOCAL | CREAD);
		options.c_cflag     &=  ~CRTSCTS;

		cfmakeraw(&options);

		tcflush(cmd_uart_fd, TCIFLUSH);
		if (tcsetattr (cmd_uart_fd, TCSANOW, &options) != 0) {
			SYS_DBG("error in tcsetattr()\n");
		}
	}
	return 0;
}

void rx_frame_parser(uint8_t* data, uint8_t len) {
	uint8_t ch;
	int rx_remain;

	while(len) {

		ch = *data++;
		len--;

		switch (rx_frame_state) {
		case SOP_STATE: {
			if (CMD_UART_SOP_CHAR == ch) {
				rx_frame_state = LEN_STATE;
				timer_set(GW_TASK_FORWARD_ID, FORWRAD_UART_TO, CMD_UART_FRAME_TO_INTERVAL, TIMER_ONE_SHOT);
			}
		}
			break;

		case LEN_STATE: {
			if (ch > CMD_UART_DATA_SIZE) {
				timer_remove_attr(GW_TASK_FORWARD_ID, FORWRAD_UART_TO);

				rx_frame_state = SOP_STATE;
				return;
			}
			else {
				cmd_uart_frame.len = ch;
				cmd_uart_frame.data_index = 0;
				rx_frame_state = DATA_STATE;

				timer_set(GW_TASK_FORWARD_ID, FORWRAD_UART_TO, CMD_UART_FRAME_TO_INTERVAL, TIMER_ONE_SHOT);
			}
		}
			break;

		case DATA_STATE: {
			cmd_uart_frame.data[cmd_uart_frame.data_index++] = ch;

			rx_remain = cmd_uart_frame.len - cmd_uart_frame.data_index;

			if (len >= rx_remain) {
				memcpy((uint8_t*)(cmd_uart_frame.data + cmd_uart_frame.data_index), data, rx_remain);
				cmd_uart_frame.data_index += rx_remain;
				len -= rx_remain;
				data += rx_remain;
			}
			else {
				memcpy((uint8_t*)(cmd_uart_frame.data + cmd_uart_frame.data_index), data, len);
				cmd_uart_frame.data_index += len;
				len = 0;
			}

			if (cmd_uart_frame.data_index == cmd_uart_frame.len) {
				rx_frame_state = FCS_STATE;
			}
		}
			break;

		case FCS_STATE: {
			timer_remove_attr(GW_TASK_FORWARD_ID, FORWRAD_UART_TO);

			rx_frame_state = SOP_STATE;

			cmd_uart_frame.frame_fcs = ch;

			if (cmd_uart_frame.frame_fcs \
					== cmd_uart_calcfcs(cmd_uart_frame.len, cmd_uart_frame.data)) {

				uint8_t* cmd_id = &cmd_uart_frame.data[0];
				uint8_t* dest_task_id = &Cmd_uart_tbl[*cmd_id].des_task_id;
				uint8_t* dest_sig = &Cmd_uart_tbl[*cmd_id].des_sig;
				//APP_DBG("cmd_id:%d\n", *cmd_id);
				//APP_DBG("dest_task_id:%d\n", *dest_task_id);
				//APP_DBG("dest_sig:%d\n", *dest_sig);
				switch (*dest_task_id) {
				case GW_TASK_FORWARD_ID: {
					if (*dest_sig) {
						task_post_dynamic_msg(*dest_task_id, *dest_sig, &cmd_uart_frame.data[1], cmd_uart_frame.len - sizeof(uint8_t));
					}
				}
					break;

				default: {
					//cmd_t Send_cmd;
					//uint8_t Return_code = CMD_RETURN_ERR;
					//Send_cmd.cmd_id = *cmd_id;
					//Send_cmd.cmd_data = &Return_code;
					//cmd_uart_tx_frame_post((cmd_t*)&Send_cmd, sizeof(Return_code));
				}
					break;
				}
			}
			else {
				/* TODO: handle checksum incorrect */
			}
		}
			break;

		default:
			break;
		}
	}
}

void* thread_down(void*) {
	int i; /* loop variables */

	/* local timekeeping variables */
	struct timespec send_time; /* time of the pull request */
	struct timespec recv_time; /* time of return from recv socket call */

	/* data buffers */
	uint8_t buff_down[1000]; /* buffer to receive downstream packets */
	uint8_t buff_req[12]; /* buffer to compose pull requests */
	int msg_len;

	/* protocol variables */
	uint8_t token_h; /* random token for acknowledgement matching */
	uint8_t token_l; /* random token for acknowledgement matching */
	bool req_ack = false; /* keep track of whether PULL_DATA was acknowledged or not */

	json down_js;
	json txpkt_js;
	fw_tx_config_t tx_cfg = Fw_tx_config;
	int raw_len;
	uint8_t raw_buff[255];
	short x0, x1;
	cmd_t send_down;

	/* auto-quit variable */
	uint32_t autoquit_cnt = 0; /* count the number of PULL_DATA sent since the latest PULL_ACK */

	/* set downstream socket RX timeout */
	i = setsockopt(sock_down, SOL_SOCKET, SO_RCVTIMEO, (void *)&pull_timeout, sizeof pull_timeout);
	if (i != 0) {
		APP_DBG("ERROR: [down] setsockopt returned %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}

	/* pre-fill the pull request buffer with fixed fields */
	buff_req[0] = PROTOCOL_VERSION;
	buff_req[3] = PKT_PULL_DATA;
	//*(uint32_t *)(buff_req + 4) = net_mac_h;
	//*(uint32_t *)(buff_req + 8) = net_mac_l;

	buff_req[4] = (unsigned char)ifr.ifr_hwaddr.sa_data[0];
	buff_req[5] = (unsigned char)ifr.ifr_hwaddr.sa_data[1];
	buff_req[6] = (unsigned char)ifr.ifr_hwaddr.sa_data[2];
	buff_req[7] = 0xFF;
	buff_req[8] = 0xFF;
	buff_req[9] = (unsigned char)ifr.ifr_hwaddr.sa_data[3];
	buff_req[10] = (unsigned char)ifr.ifr_hwaddr.sa_data[4];
	buff_req[11] = (unsigned char)ifr.ifr_hwaddr.sa_data[5];

	while (1) {
		/* generate random token for request */
		token_h = (uint8_t)rand(); /* random token */
		token_l = (uint8_t)rand(); /* random token */
		buff_req[1] = token_h;
		buff_req[2] = token_l;

		/* send PULL request and record time */
		send(sock_down, (void *)buff_req, sizeof buff_req, 0);
		clock_gettime(CLOCK_MONOTONIC, &send_time);
		req_ack = false;
		autoquit_cnt++;

		/* listen to packets and process them until a new PULL request must be sent */
		recv_time = send_time;
		while ((int)difftimespec(recv_time, send_time) < keepalive_time) {

			/* try to receive a datagram */
			msg_len = recv(sock_down, (void *)buff_down, (sizeof buff_down)-1, 0);
			clock_gettime(CLOCK_MONOTONIC, &recv_time);

			/* if no network message was received, got back to listening sock_down socket */
			if (msg_len == -1) {
				//APP_DBG("WARNING: [down] recv returned %s\n", strerror(errno)); /* too verbose */
				continue;
			}

			/* if the datagram does not respect protocol, just ignore it */
			if ((msg_len < 4) || (buff_down[0] != PROTOCOL_VERSION) || ((buff_down[3] != PKT_PULL_RESP) && (buff_down[3] != PKT_PULL_ACK))) {
				APP_DBG("WARNING: [down] ignoring invalid packet\n");
				continue;
			}

			/* if the datagram is an ACK, check token */
			if (buff_down[3] == PKT_PULL_ACK) {
				if ((buff_down[1] == token_h) && (buff_down[2] == token_l)) {
					if (req_ack) {
						APP_DBG("INFO: [down] duplicate ACK received :)\n");
					} else { /* if that packet was not already acknowledged */
						req_ack = true;
						autoquit_cnt = 0;						
						APP_DBG("INFO: [down] PULL_ACK received in %i ms\n", (int)(1000 * difftimespec(recv_time, send_time)));
					}
				} else { /* out-of-sync token */
					APP_DBG("INFO: [down] received out-of-sync ACK\n");
				}
				continue;
			}

			/* the datagram is a PULL_RESP */
			buff_down[msg_len] = 0; /* add string terminator, just to be safe */
			APP_DBG("INFO: [down] PULL_RESP received :)\n"); /* very verbose */
			APP_DBG("\nJSON down: %s\n", (char *)(buff_down + 4)); /* DEBUG: display JSON payload */

			cp_dwnb++;
			cp_txnb++;

			/* initialize TX struct and try to parse JSON */			
			try {
				down_js = json::parse((const char *)(buff_down + 4));
			} 
			catch (exception e) {
				(void)e;
				APP_DBG("WARNING: [down] invalid JSON, TX aborted\n");
				continue;
			}

			if (txpkt_js.find("txpk") == txpkt_js.end()) {
				APP_DBG("WARNING: [down] no \"txpk\" object in JSON, TX aborted\n");
				continue;
			}
			else {
				txpkt_js = down_js["txpk"];
			}

			if (txpkt_js.find("imme") != txpkt_js.end()) {
				if (txpkt_js["imme"].get<bool>()) {
					APP_DBG("INFO: [down] a packet will be sent in \"immediate\" mode\n");
				}
				else if (txpkt_js.find("tmst") != txpkt_js.end()) {
					uint32_t ts = txpkt_js["tmst"].get<unsigned long>();
					APP_DBG("INFO: [down] a packet will be sent on timestamp value %u\n", ts);					
				}
				else {
					APP_DBG("WARNING: [down] only \"immediate\" and \"timestamp\" modes supported, TX aborted\n");
					continue;
				}
			}

			if (txpkt_js.find("freq") == txpkt_js.end()) {
				APP_DBG("WARNING: [down] no mandatory \"txpk.freq\" object in JSON, TX aborted\n");
				continue;
			}
			else {
				tx_cfg.tx_frequency = (uint32_t)txpkt_js["freq"].get<double>() * 1000000.0f;
			}

			if (txpkt_js.find("powe") == txpkt_js.end()) {
				APP_DBG("WARNING: [down] no mandatory \"txpk.powe\" object in JSON, TX aborted\n");
				continue;
			}
			else {
				tx_cfg.tx_power = (uint8_t)txpkt_js["powe"].get<unsigned int>();
			}

			if (txpkt_js.find("modu") == txpkt_js.end()) {
				APP_DBG("WARNING: [down] no mandatory \"txpk.modu\" object in JSON, TX aborted\n");
				continue;
			}
			else {
				string modu = txpkt_js["modu"].get<string>();
				if (!modu.compare("FSK")) {
					APP_DBG("WARNING: [down] no handle for \"txpk.modu\"=\"FSK\", TX aborted\n");
					continue;
				}
				else if (!modu.compare("LORA")) {

					if (txpkt_js.find("datr") == txpkt_js.end()) {
						APP_DBG("WARNING: [down] no mandatory \"txpk.datr\" object in JSON, TX aborted\n");
						continue;
					}
					else {
						string datr = txpkt_js["datr"].get<string>();
						i = sscanf(datr.data(), "SF%2hdBW%3hd", &x0, &x1);
						(void)x1;
						if (i != 2) {
							APP_DBG("WARNING: [down] format error in \"txpk.datr\", TX aborted\n");
							continue;
						}
						switch (x0) {
						case  7: tx_cfg.tx_datarater = 5;  break;
						case  8: tx_cfg.tx_datarater = 4;  break;
						case  9: tx_cfg.tx_datarater = 3;  break;
						case 10: tx_cfg.tx_datarater = 2; break;
						case 11: tx_cfg.tx_datarater = 1; break;
						case 12: tx_cfg.tx_datarater = 0; break;
						default:
							APP_DBG("WARNING: [down] format error in \"txpk.datr\", invalid SF, TX aborted\n");
							continue;
						}

						send_down.cmd_id =  CMD_FW_SET_TX_CONFIG;
						send_down.cmd_data = (uint8_t*)&tx_cfg;
						cmd_uart_tx_frame_post(&send_down, sizeof (fw_tx_config_t));
					}

				}
				else {
					APP_DBG("WARNING: [down] invalid modulation in \"txpk.modu\", TX aborted\n");
					continue;
				}
			}

			if (txpkt_js.find("size") == txpkt_js.end()) {
				APP_DBG("WARNING: [down] no mandatory \"txpk.size\" object in JSON, TX aborted\n");
				continue;
			}
			else {
				raw_len = (uint8_t)txpkt_js["size"].get<unsigned int>();
			}

			if (txpkt_js.find("data") == txpkt_js.end()) {
				APP_DBG("WARNING: [down] no mandatory \"txpk.data\" object in JSON, TX aborted\n");
				continue;
			}
			else {
				string data = txpkt_js["data"].get<string>();
				i = b64_to_bin(data.data(), data.length(), raw_buff, 255);
				if (i != raw_len) {
					APP_DBG("WARNING: [down] mismatch between .size and .data size once converter to binary\n");
				}

			}

			send_down.cmd_id =  CMD_FW_SEND;
			send_down.cmd_data = (uint8_t*)raw_buff;
			cmd_uart_tx_frame_post(&send_down, raw_len);
		}
	}
	APP_DBG("\nINFO: End of downstream thread\n");
}

static double difftimespec(struct timespec end, struct timespec beginning) {
	double x;

	x = 1E-9 * (double)(end.tv_nsec - beginning.tv_nsec);
	x += (double)(end.tv_sec - beginning.tv_sec);

	return x;
}

int cmd_uart_tx_frame_post(cmd_t* data, uint8_t data_len) {
	//printf("cmd_uart_tx_frame_post\n");
	uint8_t frame_len = data_len + sizeof(data->cmd_id);
	tx_buffer[0] = CMD_UART_SOP_CHAR;
	tx_buffer[1] = frame_len;
	tx_buffer[2] = data->cmd_id;
	memcpy(&tx_buffer[3], data->cmd_data, data_len);
	tx_buffer[2 + frame_len] = cmd_uart_calcfcs(frame_len, &tx_buffer[2]);

	return write(cmd_uart_fd, tx_buffer, (frame_len + 3));
}

uint8_t cmd_uart_calcfcs(uint8_t len, uint8_t *data_ptr) {
	uint8_t xor_result = len;
	for (int i = 0; i < len; i++, data_ptr++) {
		xor_result = xor_result ^ *data_ptr;
	}
	return xor_result;
}
