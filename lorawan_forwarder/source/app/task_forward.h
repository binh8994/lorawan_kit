#ifndef __TASK_FORWARD_H__
#define __TASK_FORWARD_H__

#include "message.h"
#include "port.h"

typedef struct {
	uint32_t tx_frequency;
	uint8_t tx_datarater;
	uint8_t tx_power;
} __AK_PACKETED fw_tx_config_t;

typedef struct {
	uint32_t rx_frequency;
	uint8_t rx_datarater;
} __AK_PACKETED fw_rx_config_t;

extern q_msg_t gw_task_forward_mailbox;
extern void* gw_task_forward_entry(void*);

#endif // __TASK_FORWARD_H__
